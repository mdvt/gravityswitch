﻿//#define DEEP_VALID_CHECK
//#define PLAYER_PATH_DEBUG

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

using Path;
using Patterns = Path.Patterns;
using MyLibraries;
using MyLibraries.Figures;

using Random = UnityEngine.Random;

public class RandomArea : AreaGenerator
{
    public override IntRangeSet PossibleStartHeights(float difficulty)
    {
        return new IntRangeSet(new IntRange(MinGateHeight(difficulty),
                                            MaxGateHeight(difficulty)));
    }

    public int MaxGateHeight(float difficulty) { return LerpParamByDifficulty(difficulty, 7, 15); }
    public int MinGateHeight(float difficulty) { return LerpParamByDifficulty(difficulty, 7, 5); }
    public int MaxPatternLenght { get { return 10; } }
    public int MinPatternLenght { get { return 2; } }
    public int MinAngle90PatternsShift { get { return 2; } }
    public int MaxAngle90PatternsShift { get { return 5; } }
    public int TopBorder { get { return 15; } }
    public int BottomBorder { get { return -5; } }

    // path generation params
    private IntRange KeepGravityPatternLengthRange { get { return new IntRange(3, 10); } }
    private IntRange SwitchGravityPatternLengthRange { get { return new IntRange(2, 6); } }
    private int PlayerPathPatternsCount { get { return Random.Range(5, 10); } }
    // path generation params

    enum ValidCheckErrCode
    {
        none,
        passOutOfBorders,
        invalidGateHeight,
        invalidPatternSize
    }

    static string[] ValidCheckErrText = new string[4]
    {
            "None",
            "Passed out of borders",
            "Invalid gate height",
            "Invalid pattern size"
    };

    class ValidCheckError
    {
        public static ValidCheckError none { get { return new ValidCheckError(ValidCheckErrCode.none, PathPoint.zero, PathPoint.zero); } }

        public ValidCheckErrCode code;
        public PathPoint generated;
        public PathPoint last;
        public ValidCheckError(ValidCheckErrCode code, PathPoint generated, PathPoint last)
        {
            this.code = code;
            this.generated = generated;
            this.last = last;
        }
    }

    protected override IEnumerator<LinkedList<PathPoint>> Generate(GenerateInfo info)
    {
        Dictionary<Patterns.BasePattern, IntRangeSet>
            possibleGeneration = new Dictionary<Patterns.BasePattern, IntRangeSet>();
        Dictionary<Patterns.BasePattern, float>
            patternsProbability = new Dictionary<Patterns.BasePattern, float>();

        PlayerPath playerPath = new PlayerPath();
        playerPath.BasicBuild(DiscreteNormalDistributionByDifficulty(1 - info.difficulty, KeepGravityPatternLengthRange),
                              DiscreteNormalDistributionByDifficulty(1 - info.difficulty, SwitchGravityPatternLengthRange),
                              PlayerPathPatternsCount);
        ByPlayerPathPatternsLoader patternsLoader = new ByPlayerPathPatternsLoader(playerPath,
                                                                                   info.lastPoint,
                                                                                   GlobalSpace.game.player.FallMovementTan()
#if PLAYER_PATH_DEBUG
                                                                                   , info.area
#endif
                                                                                   );

        while (!patternsLoader.Filled)
        {
            possibleGeneration.Clear();
            patternsProbability.Clear();
            foreach (Patterns.BasePattern p in GlobalSpace.game.map.PathPatterns)
            {
                IntRangeSet possibleArgs = patternsLoader.GetPossibleArgs(p);
                possibleArgs = possibleArgs.GetIntersection(PatternPossibleArgs(p));
                possibleArgs = possibleArgs.GetIntersection(MaxMinGateHeightPossibleArgs(MinGateHeight(info.difficulty),
                                                                                            MaxGateHeight(info.difficulty),
                                                                                            p, patternsLoader.points.Last.Value));
                possibleArgs = possibleArgs.GetIntersection(PatternSizeСonstraints(MinPatternLenght,
                                                                                                MaxPatternLenght,
                                                                                                MinAngle90PatternsShift,
                                                                                                MaxAngle90PatternsShift,
                                                                                                p));

                if (!possibleArgs.IsEmpty)
                {
                    possibleGeneration.Add(p, possibleArgs);
                    patternsProbability.Add(p, PatternProbability(p, patternsLoader.points.Last.Value.generatingPattern, info.difficulty));
                }

                yield return null;
            }

            if (possibleGeneration.Count == 0)
                throw new Exception("Сonflicting generation conditions!");

            var pattern = MyRandom.RandomOptionByProbability(patternsProbability);

            if (pattern == null)
            {
                /*pattern = patternsProbability.Keys.First();
                PathPoint last = patternsLoader.points.Last.Value;
                info.area.localDebugLines.AddLast(new DebugLine((Vector2)last.pos, last.pos + last.height * Vector2.up, Color.blue));
                Debug.Log("epsilon case");*/
                throw new Exception("Сonflicting generation conditions!");
            }

#if DEEP_VALID_CHECK
            foreach (var pair in possibleGeneration)
            {
                ValidCheckError error = IsValidGeneratorArgs(pair.Key, pair.Value, patternsLoader.points.Last.Value, startPos, info.difficulty);
                if (error.code != ValidCheckErrCode.none && error.code != ValidCheckErrCode.passOutOfBorders)
                    throw new Exception("Not valid args in pattern. ERRCODE: " + ValidCheckErrText[(int)error.code]);
                yield return null;
            }
#endif

            var distributionDensityFunc = NormalDistributionByDifficulty(PatternDifficultyByArgIncrease(pattern, info.difficulty),
                                                                            PatternSizeСonstraintsPositiveRange(MinPatternLenght,
                                                                                                                    MaxPatternLenght,
                                                                                                                    MinAngle90PatternsShift,
                                                                                                                    MaxAngle90PatternsShift,
                                                                                                                    pattern)).ByArgAbs();
            int arg = MyRandom.RandomOptionByProbability(ArgumentsProbability(possibleGeneration[pattern], distributionDensityFunc));

            patternsLoader.AddNewPattern(pattern, arg);

            yield return null;
        }

        patternsLoader.points.RemoveFirst();
        yield return patternsLoader.points;
    }

    private IEnumerable<ModifiableKeyValuePair<int, float>> ArgumentsProbability(IntRangeSet args, Func<float, float> probabilityFunc)
    {
        ModifiableKeyValuePair<int, float> current;
        foreach (var range in args.elements)
            foreach (int arg in range)
            {
                current.Key = arg;
                current.Value = probabilityFunc(arg);
                yield return current;
            }
    }

    float PatternDifficultyByArgIncrease(Patterns.BasePattern pattern, float difficulty)
    {
        if (pattern.ChangingHeight == Patterns.HeightChangeTypes.doubleDecrease)
            return difficulty;

        if (pattern.ChangingHeight == Patterns.HeightChangeTypes.decrease && pattern.Angle == Patterns.AngleType._90)
            return difficulty;

        return 1 - difficulty;
    }

    float PatternProbability(Patterns.BasePattern pattern, Patterns.BasePattern lastPattern, float difficulty)
    {
        if (pattern.GetHashCode() == lastPattern.GetHashCode())
            return float.Epsilon;

        if (lastPattern.Angle != Patterns.AngleType._0 && pattern.Angle != Patterns.AngleType._0)
            return float.Epsilon;

        if (pattern.ChangingHeight == Patterns.HeightChangeTypes.doubleDecrease)
        {
            return difficulty;
        }

        if (pattern.Angle == Patterns.AngleType._45)
            return 1 - difficulty + float.Epsilon;

        return 1;
    }

    IntRangeSet PatternPossibleArgs(Patterns.BasePattern pattern)
    {
        if (!pattern.MayTakeNegativeArg)
            return new IntRangeSet(new IntRange(1, int.MaxValue));

        return new IntRangeSet(new IntRange(int.MinValue, -1), new IntRange(1, int.MaxValue));//exclude zero
    }

    IntRangeSet BordersPossibleArgs(int minHeight, int maxHeight, Patterns.BasePattern pattern, PathPoint lastPoint, int startHeight)
    {
        int absoluteYPos = lastPoint.pos.y + startHeight;
        switch (pattern.ChangingHeight)
        {
            case Patterns.HeightChangeTypes.none:
                {
                    if (pattern.Angle == Patterns.AngleType._0)
                        return IntRangeSet.Infinite;
                    else //Angle: 45
                        return new IntRangeSet(new IntRange(minHeight - absoluteYPos, maxHeight - (absoluteYPos + lastPoint.height)));
                }

            case Patterns.HeightChangeTypes.increase:
                {
                    return new IntRangeSet(new IntRange(minHeight - absoluteYPos, maxHeight - (absoluteYPos + lastPoint.height)));
                }

            case Patterns.HeightChangeTypes.doubleIncrease:
                {
                    int distanceToNearestLimit = Mathf.Min(absoluteYPos - minHeight,
                                                            maxHeight - (absoluteYPos + lastPoint.height));
                    return new IntRangeSet(new IntRange(int.MinValue, distanceToNearestLimit));
                }

            default://all of ChangingHeight: decrease
                {
                    return IntRangeSet.Infinite;
                }
        }
    }

    IntRangeSet MaxMinGateHeightPossibleArgs(int minGateHeight, int maxGateHeight, Patterns.BasePattern pattern, PathPoint lastPoint)
    {
        switch (pattern.ChangingHeight)
        {
            case Patterns.HeightChangeTypes.decrease:
                {
                    int arg = lastPoint.height - minGateHeight;
                    return new IntRangeSet(new IntRange(-arg, arg));
                }

            case Patterns.HeightChangeTypes.increase:
                {
                    int arg = maxGateHeight - lastPoint.height;
                    return new IntRangeSet(new IntRange(-arg, arg));
                }

            case Patterns.HeightChangeTypes.doubleIncrease:
                {
                    int maxArg = (maxGateHeight - lastPoint.height) / 2;
                    return new IntRangeSet(new IntRange(int.MinValue, maxArg));
                }

            case Patterns.HeightChangeTypes.doubleDecrease:
                {
                    int maxArg = (lastPoint.height - minGateHeight) / 2;
                    return new IntRangeSet(new IntRange(int.MinValue, maxArg));
                }

            default: //ChangingHeight: none
                {
                    return IntRangeSet.Infinite;
                }
        }
    }

    IntRangeSet PatternSizeСonstraints(int minLength, int maxLength, int minAngle90Size, int maxAngle90Size, Patterns.BasePattern pattern)
    {
        var resultRange = PatternSizeСonstraintsPositiveRange(minLength, maxLength, minAngle90Size, maxAngle90Size, pattern);
        return new IntRangeSet(new IntRange(-resultRange.end, -resultRange.start), resultRange);
    }

    IntRange PatternSizeСonstraintsPositiveRange(int minLength, int maxLength, int minAngle90Size, int maxAngle90Size, Patterns.BasePattern pattern)
    {
        if (pattern.Angle == Patterns.AngleType._90)
            return new IntRange(minAngle90Size, maxAngle90Size);

        return new IntRange(minLength, maxLength);
    }

    ValidCheckErrCode IsValidPoint(PathPoint p, PathPoint lastPoint, int handlerPosY, float difficulty)
    {
        if (!(p.pos.y + handlerPosY >= BottomBorder && p.pos.y + p.height + handlerPosY <= TopBorder))
            return ValidCheckErrCode.passOutOfBorders;
        if (!(p.height <= MaxGateHeight(difficulty) && p.height >= MinGateHeight(difficulty)))
            return ValidCheckErrCode.invalidGateHeight;

        if (p.generatingPattern.Angle == Patterns.AngleType._90)
        {
            bool doubleHeightChanging = p.generatingPattern.ChangingHeight == Patterns.HeightChangeTypes.doubleDecrease ||
                                        p.generatingPattern.ChangingHeight == Patterns.HeightChangeTypes.doubleIncrease;
            int shift = Mathf.Abs(p.height - lastPoint.height);
            int maxShift = MaxAngle90PatternsShift * (doubleHeightChanging ? 2 : 1);
            if (!(shift <= maxShift))
                return ValidCheckErrCode.invalidPatternSize;
        }
        else if (!(p.pos.x - lastPoint.pos.x >= MinPatternLenght && p.pos.x - lastPoint.pos.x <= MaxPatternLenght))
            return ValidCheckErrCode.invalidPatternSize;
        return ValidCheckErrCode.none;
    }

    ValidCheckError IsValidGeneratorArgs(Patterns.BasePattern pattern, IntRangeSet args, PathPoint last, int handlerPosY, float difficulty)
    {
        foreach (IntRange r in args.elements)
        {
            for (int i = r.start; i <= r.end; i++)
            {
                PathPoint p = pattern.Generate(last, i);
                ValidCheckErrCode errCode = IsValidPoint(p, last, handlerPosY, difficulty);
                if (errCode != ValidCheckErrCode.none)
                    return new ValidCheckError(errCode, p, last);
            }
        }
        return ValidCheckError.none;
    }

    private class ByPlayerPathPatternsLoader
    {
        private enum ActionSequienceStates
        {
            Keep,
            Switch
        }

        public LinkedList<PathPoint> points;

        private readonly PlayerPath playerPath;
        private readonly float playerFallTan;

        private ActionSequienceStates actionSequienceState;
        private LinkedListNode<KeepSwitchActionsSequience> currActionSequienceNode;

        private Vector2 keepStartPos;
        private MyMath.Direction playerGravity;

        private Ray2 switchStartBorder;
        private Ray2 switchEndBorder;
        private bool currDangerPatternLoaded;

#if PLAYER_PATH_DEBUG
        private AreaHandler area;
#endif

        public ByPlayerPathPatternsLoader(PlayerPath playerPath, PathPoint firstPoint, float playerFallTan
#if PLAYER_PATH_DEBUG
            , AreaHandler area
#endif
            )
        {
#if PLAYER_PATH_DEBUG
            this.area = area;
#endif

            this.playerPath = playerPath;
            this.playerFallTan = playerFallTan;

            points = new LinkedList<PathPoint>();
            points.AddFirst(firstPoint);

            currActionSequienceNode = this.playerPath.First;
            actionSequienceState = ActionSequienceStates.Keep;

            playerGravity = (MyMath.Direction)MyRandom.RandomSign();
            currDangerPatternLoaded = false;
            keepStartPos = firstPoint.pos + (playerGravity == MyMath.Direction.Up ? (firstPoint.height * Vector2.up).ToVector2Int() : Vector2Int.zero);
        }

        public bool Filled
        {
            get
            {
                return currActionSequienceNode == playerPath.Last &&
                       actionSequienceState != ActionSequienceStates.Keep;
            }
        }

        public void AddNewPattern(Patterns.BasePattern pattern, int arg)
        {
#if PLAYER_PATH_DEBUG
            PathPoint debugLastPoint = points.Last.Value;
            Line2 bottomLine = pattern.GetBottomLine(debugLastPoint, arg);
            area.localDebugLines.AddLast(new DebugLine(new Line2Segment(bottomLine.point, bottomLine.point + bottomLine.direction * Mathf.Abs(arg)), Color.green));

            Line2 topLine = pattern.GetTopLine(debugLastPoint, arg);
            area.localDebugLines.AddLast(new DebugLine(new Line2Segment(topLine.point, topLine.point + topLine.direction * Mathf.Abs(arg)), Color.green));
#endif
            points.AddLast(pattern.Generate(points.Last.Value, arg));

            PathPoint prevLastPoint = points.Last.Previous.Value;
            PathPoint lastPoint = points.Last.Value;

            bool stateSwitched;
            do
            {
                stateSwitched = false;
                if (actionSequienceState == ActionSequienceStates.Keep)
                {
                    if (playerGravity == MyMath.Direction.Up && !pattern.GetBottomLineType(arg).isSafe ||
                        playerGravity == MyMath.Direction.Down && !pattern.GetTopLineType(arg).isSafe)
                        currDangerPatternLoaded = true;

                    if (points.Last.Value.pos.x - keepStartPos.x >= currActionSequienceNode.Value.KeepLength)
                    {
                        SetupSwitchStates();

                        actionSequienceState = ActionSequienceStates.Switch;

                        stateSwitched = true;

#if PLAYER_PATH_DEBUG
                        area.localDebugLines.AddLast(new DebugLine(keepStartPos, switchStartBorder.point, Color.yellow));
#endif
                    }
                }
                else if (actionSequienceState == ActionSequienceStates.Switch)
                {
                    if (playerGravity == MyMath.Direction.Up && !pattern.GetTopLineType(arg).isSafe ||
                        playerGravity == MyMath.Direction.Down && !pattern.GetBottomLineType(arg).isSafe)
                        currDangerPatternLoaded = true;

                    Vector2 intersectionPoint;

                    if (playerGravity == MyMath.Direction.Up && pattern.GetBottomLineType(arg).angle != Patterns.AngleType._90 ||
                        playerGravity == MyMath.Direction.Down && pattern.GetTopLineType(arg).angle != Patterns.AngleType._90)
                    {
                        if (playerGravity == MyMath.Direction.Up)
                        {
                            intersectionPoint = MyMath.GetIntersections((Line2)switchEndBorder, pattern.GetBottomLine(prevLastPoint, arg))[0];

                            if (pattern.GetBottomLineType(arg).angle == Patterns.AngleType._45 && intersectionPoint.x < prevLastPoint.pos.x + 1)
                                intersectionPoint = MyMath.GetIntersections((Line2)switchEndBorder, new Line2(prevLastPoint.pos, Vector2.right))[0];
                        }
                        else
                        {
                            intersectionPoint = MyMath.GetIntersections((Line2)switchEndBorder, pattern.GetTopLine(prevLastPoint, arg))[0];

                            if (pattern.GetTopLineType(arg).angle == Patterns.AngleType._45 && intersectionPoint.x < prevLastPoint.pos.x + 1)
                                intersectionPoint = MyMath.GetIntersections((Line2)switchEndBorder,
                                                                            new Line2(prevLastPoint.pos + prevLastPoint.height * Vector2.up, Vector2.right))[0];
                        }

                        if (intersectionPoint.x <= lastPoint.pos.x)
                        {
                            playerGravity = MyMath.Switch(playerGravity);
                            keepStartPos = intersectionPoint;

                            actionSequienceState = ActionSequienceStates.Keep;

                            currActionSequienceNode = currActionSequienceNode.Next;

                            stateSwitched = true;

#if PLAYER_PATH_DEBUG
                            area.localDebugLines.AddLast(new DebugLine(switchStartBorder.point, keepStartPos + Vector2.left * currActionSequienceNode.Previous.Value.DistanceToSwitch, Color.yellow));
                            area.localDebugLines.AddLast(new DebugLine(switchEndBorder.point, keepStartPos, Color.yellow));
#endif
                        }
                    }
                }
            }
            while (stateSwitched && !Filled);
        }

        private void SetupSwitchStates()
        {
            PathPoint lastPoint = points.Last.Value;

            if (playerGravity == MyMath.Direction.Up)
                switchStartBorder = new Ray2(new Vector2(keepStartPos.x + currActionSequienceNode.Value.KeepLength, lastPoint.pos.y + lastPoint.height),
                                             new Vector2(1, -playerFallTan));
            else
                switchStartBorder = new Ray2(new Vector2(keepStartPos.x + currActionSequienceNode.Value.KeepLength, lastPoint.pos.y),
                                             new Vector2(1, playerFallTan));

            switchEndBorder = new Ray2(switchStartBorder.point + Vector2.right * currActionSequienceNode.Value.DistanceToSwitch,
                                       new Vector2(1, playerFallTan * -(int)playerGravity));
            currDangerPatternLoaded = false;
        }

        public IntRangeSet GetPossibleArgs(Patterns.BasePattern pattern)
        {
            if (actionSequienceState == ActionSequienceStates.Keep)
                return GetKeepRangePossibleArgs(pattern);
            else if (actionSequienceState == ActionSequienceStates.Switch)
                return GetSwitchRangePossibleArgs(pattern);

            return new IntRangeSet();
        }

        private IntRangeSet GetKeepRangePossibleArgs(Patterns.BasePattern pattern)
        {
            int maxArg;
            int minArg = 0;

            if (playerGravity == MyMath.Direction.Up)
            {
                maxArg = GetKeepMaxArg(pattern, pattern.GetTopLineType(1), pattern.GetBottomLineType(1));
                if (pattern.MayTakeNegativeArg)
                    minArg = -GetKeepMaxArg(pattern, pattern.GetTopLineType(-1), pattern.GetBottomLineType(-1));
            }
            else
            {
                maxArg = GetKeepMaxArg(pattern, pattern.GetBottomLineType(1), pattern.GetTopLineType(1));
                if (pattern.MayTakeNegativeArg)
                    minArg = -GetKeepMaxArg(pattern, pattern.GetBottomLineType(-1), pattern.GetTopLineType(-1));
            }

            if (minArg != maxArg)
                return new IntRangeSet(new IntRange(minArg, maxArg));
            else
                return new IntRangeSet();
        }

        private int GetKeepMaxArg(Patterns.BasePattern pattern, Patterns.LineType saveSideLine, Patterns.LineType dangerSideLine)
        {
            return Mathf.Min(GetKeepSaveSideMaxArg(pattern, saveSideLine, dangerSideLine),
                             GetKeepDangerSideMaxArg(pattern, saveSideLine, dangerSideLine));
        }

        private int GetKeepSaveSideMaxArg(Patterns.BasePattern pattern, Patterns.LineType saveSideLine, Patterns.LineType dangerSideLine)
        {
            KeepSwitchActionsSequience currSequience = currActionSequienceNode.Value;
            KeepSwitchActionsSequience nextSequience = currActionSequienceNode.Next?.Value;
            PathPoint lastPoint = points.Last.Value;

            if (!saveSideLine.isSafe)
                return 0;

            if (pattern.Angle != Patterns.AngleType._90)
            {
                if (currSequience.DistanceToSwitch == -1)
                    return MyMath.Round(keepStartPos.x + currSequience.KeepLength * 2 - lastPoint.pos.x, MyMath.Direction.Down);

                if (saveSideLine.angle == Patterns.AngleType._45)
                    return MyMath.Round(keepStartPos.x + currSequience.KeepLength - lastPoint.pos.x, MyMath.Direction.Down);

                if (saveSideLine.angle == Patterns.AngleType._0)
                {
                    if (dangerSideLine.isSafe)
                    {
                        Line2 rightSwitchBorder = new Line2(new Vector2(keepStartPos.x + currSequience.KeepLength + currSequience.DistanceToSwitch,
                                                                        lastPoint.pos.y + lastPoint.height),
                                                            new Vector2(1, -playerFallTan));

                        Vector2 endNextKeepPos = default;

                        if (dangerSideLine.angle == Patterns.AngleType._0)
                        {
                            Vector2 endSwitchPos = MyMath.GetIntersections(new Line2(lastPoint.pos, Vector2.right), rightSwitchBorder)[0];
                            endNextKeepPos = endSwitchPos + Vector2.right * nextSequience.KeepLength;
                        }

                        if (dangerSideLine.angle == Patterns.AngleType._45)
                        {
                            Vector2 endSwitchPos = MyMath.GetIntersections(new Line2(lastPoint.pos + Vector2.right, new Vector2(1, -1)),
                                                                           rightSwitchBorder)[0];
                            endNextKeepPos = endSwitchPos + new Vector2(1, -1) * nextSequience.KeepLength;
                        }

                        if (dangerSideLine.angle == Patterns.AngleType._0 || dangerSideLine.angle == Patterns.AngleType._45)
                            return MyMath.Round(endNextKeepPos.x - 2 - lastPoint.pos.x, MyMath.Direction.Down);
                    }
                }
            }
            else
            {
                if (saveSideLine.angle == Patterns.AngleType._90)
                    return Mathf.Clamp((int)((currSequience.KeepLength - (lastPoint.pos.x - keepStartPos.x) - 1) * playerFallTan), 0, int.MaxValue);
            }

            return int.MaxValue;
        }

        private int GetKeepDangerSideMaxArg(Patterns.BasePattern pattern, Patterns.LineType saveSideLine, Patterns.LineType dangerSideLine)
        {
            KeepSwitchActionsSequience currSequience = currActionSequienceNode.Value;
            KeepSwitchActionsSequience nextSequience = currActionSequienceNode.Next?.Value;
            PathPoint lastPoint = points.Last.Value;

            if (!currDangerPatternLoaded && dangerSideLine.isSafe)
                return 0;

            if (dangerSideLine.isSafe)
            {
                if (dangerSideLine.angle == Patterns.AngleType._45 && saveSideLine.isSafe)
                {
                    if (currSequience.DistanceToSwitch == -1)
                        return int.MaxValue;

                    Line2 rightSwitchBorder = new Line2();

                    if (saveSideLine.angle == Patterns.AngleType._0)
                        rightSwitchBorder.Set(lastPoint.pos + lastPoint.height * Vector2.up +
                                                (keepStartPos.x + currSequience.KeepLength - lastPoint.pos.x + currSequience.DistanceToSwitch) * Vector2.right,
                                              new Vector2(1, -playerFallTan));

                    if (saveSideLine.angle == Patterns.AngleType._45)
                        rightSwitchBorder.Set(lastPoint.pos + lastPoint.height * Vector2.up +
                                                (keepStartPos.x + currSequience.KeepLength - lastPoint.pos.x + currSequience.DistanceToSwitch) * new Vector2(1, 1),
                                              new Vector2(1, -playerFallTan));

                    float endSwitchPos = MyMath.GetIntersections(rightSwitchBorder, new Line2(lastPoint.pos + Vector2.right, new Vector2(1, -1)))[0].x;

                    if (nextSequience.DistanceToSwitch == -1)
                        return MyMath.Round(endSwitchPos + nextSequience.KeepLength * 2 - lastPoint.pos.x, MyMath.Direction.Down);

                    return MyMath.Round(endSwitchPos + nextSequience.KeepLength - lastPoint.pos.x, MyMath.Direction.Down);
                }
            }

            if (!dangerSideLine.isSafe)
            {
                Line2 leftSwitchBorder = new Line2(new Vector2(keepStartPos.x + currSequience.KeepLength,
                                                               lastPoint.pos.y + lastPoint.height),
                                                   new Vector2(1, -playerFallTan));

                if (dangerSideLine.angle == Patterns.AngleType._45)
                    return MyMath.Round(MyMath.GetIntersections(new Line2(lastPoint.pos, new Vector2(1, 1)), leftSwitchBorder)[0].x - lastPoint.pos.x, MyMath.Direction.Down);
            }

            return int.MaxValue;
        }

        private IntRangeSet GetSwitchRangePossibleArgs(Patterns.BasePattern pattern)
        {
            int maxArg;
            int minArg = 0;

            if (playerGravity == MyMath.Direction.Up)
            {
                maxArg = GetSwitchMaxArg(pattern, pattern.GetTopLineType(1), pattern.GetBottomLineType(1));
                if (pattern.MayTakeNegativeArg)
                    minArg = -GetSwitchMaxArg(pattern, pattern.GetTopLineType(-1), pattern.GetBottomLineType(-1));
            }
            else
            {
                maxArg = GetSwitchMaxArg(pattern, pattern.GetBottomLineType(1), pattern.GetTopLineType(1));
                if (pattern.MayTakeNegativeArg)
                    minArg = -GetSwitchMaxArg(pattern, pattern.GetBottomLineType(-1), pattern.GetTopLineType(-1));
            }

            if (minArg != maxArg)
                return new IntRangeSet(new IntRange(minArg, maxArg));
            else
                return new IntRangeSet();
        }

        private int GetSwitchMaxArg(Patterns.BasePattern pattern, Patterns.LineType startSideLine, Patterns.LineType endSideLine)
        {
            return Mathf.Min(GetSwitchStartSideMaxArg(startSideLine, endSideLine),
                             GetSwitchEndSideMaxArg(pattern, startSideLine, endSideLine));
        }

        private int GetSwitchStartSideMaxArg(Patterns.LineType startSideLine, Patterns.LineType endSideLine)
        {
            KeepSwitchActionsSequience currSequience = currActionSequienceNode.Value;
            KeepSwitchActionsSequience nextSequience = currActionSequienceNode.Next?.Value;
            KeepSwitchActionsSequience nextNextSequience = currActionSequienceNode.Next?.Next?.Value;
            PathPoint lastPoint = points.Last.Value;

            float middlePos = lastPoint.pos.y + (float)lastPoint.height / 2;
            Line2 rightSwitchBorder = new Line2(new Vector2(switchEndBorder.point.x, switchEndBorder.direction.y > 0 ? -switchEndBorder.point.y + middlePos * 2 : switchEndBorder.point.y),
                                                new Vector2(1, -playerFallTan));

            if (lastPoint.pos.x < switchStartBorder.point.x + GetSwitchIntLength(currSequience, playerFallTan))
            {
                if (startSideLine.angle == Patterns.AngleType._0)
                {
                    if (endSideLine.isSafe)
                    {
                        Vector2 endNextKeepPos = default;

                        if (endSideLine.angle == Patterns.AngleType._0)
                        {
                            Vector2 endSwitchPos = MyMath.GetIntersections(new Line2(lastPoint.pos, Vector2.right), rightSwitchBorder)[0];
                            endNextKeepPos = endSwitchPos + Vector2.right * nextSequience.KeepLength;
                        }

                        if (endSideLine.angle == Patterns.AngleType._45)
                        {
                            Vector2 endSwitchPos = MyMath.GetIntersections(new Line2(lastPoint.pos + Vector2.right, new Vector2(1, -1)),
                                                                           rightSwitchBorder)[0];
                            endNextKeepPos = endSwitchPos + new Vector2(1, -1) * nextSequience.KeepLength;
                        }

                        if (endSideLine.angle == Patterns.AngleType._0 || endSideLine.angle == Patterns.AngleType._45)
                            return MyMath.Round(endNextKeepPos.x - 2 - lastPoint.pos.x, MyMath.Direction.Down);
                    }
                }
                else
                    return 0;
            }
            else
            {
                if (!currDangerPatternLoaded && startSideLine.isSafe)
                    return 0;

                if (startSideLine.isSafe && startSideLine.angle == Patterns.AngleType._45 && endSideLine.isSafe)
                {
                    if (nextSequience.DistanceToSwitch == -1)
                        return int.MaxValue;

                    Line2 rightNextSwitchBorder = new Line2();

                    if (endSideLine.angle == Patterns.AngleType._0)
                    {
                        Vector2 endCurrSwitchPos = MyMath.GetIntersections(rightSwitchBorder, new Line2(lastPoint.pos, Vector2.right))[0];
                        rightNextSwitchBorder.Set(endCurrSwitchPos + (nextSequience.KeepLength + nextSequience.DistanceToSwitch) * Vector2.right,
                                              new Vector2(1, playerFallTan));
                    }

                    if (endSideLine.angle == Patterns.AngleType._45)
                    {
                        Vector2 endCurrSwitchPos = MyMath.GetIntersections(rightSwitchBorder, new Line2(lastPoint.pos + Vector2.right, new Vector2(1, -1)))[0];
                        rightNextSwitchBorder.Set(endCurrSwitchPos + (currSequience.KeepLength + currSequience.DistanceToSwitch) * new Vector2(1, -1),
                                              new Vector2(1, playerFallTan));
                    }

                    float endNextSwitchPos = MyMath.GetIntersections(rightNextSwitchBorder,
                                                                     new Line2(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right,
                                                                               new Vector2(1, 1)))[0].x;

                    if (nextNextSequience.DistanceToSwitch == -1)
                        return MyMath.Round(endNextSwitchPos + nextNextSequience.KeepLength * 2 - lastPoint.pos.x, MyMath.Direction.Down);

                    return MyMath.Round(endNextSwitchPos + nextNextSequience.KeepLength - lastPoint.pos.x, MyMath.Direction.Down);
                }

                if (!startSideLine.isSafe)
                {
                    if (startSideLine.angle == Patterns.AngleType._45)
                    {
                        if (nextSequience.DistanceToSwitch == -1)
                            return int.MaxValue;

                        if (endSideLine.angle == Patterns.AngleType._0)
                        {
                            float endSwitchPos = MyMath.GetIntersections(rightSwitchBorder, new Line2(lastPoint.pos, Vector2.right))[0].x;
                            Line2 leftNextSwitchBorder = new Line2(new Vector2(endSwitchPos + nextSequience.KeepLength,
                                                                               lastPoint.pos.y),
                                                                   new Vector2(1, playerFallTan));

                            return MyMath.Round(MyMath.GetIntersections(leftNextSwitchBorder,
                                                                        new Line2(lastPoint.pos + lastPoint.height * Vector2.up,
                                                                                  new Vector2(1, -1)))[0].x -
                                                lastPoint.pos.x, MyMath.Direction.Down);
                        }
                    }

                    if (startSideLine.angle == Patterns.AngleType._90)
                    {
                        return Mathf.Clamp(MyMath.Round(lastPoint.pos.y + lastPoint.height -
                                                        MyMath.GetIntersections(rightSwitchBorder,
                                                                                new Line2(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right,
                                                                                          Vector2.down))[0].y,
                                                        MyMath.Direction.Down),
                                           0, int.MaxValue);
                    }
                }
            }

            return int.MaxValue;
        }

        private int GetSwitchEndSideMaxArg(Patterns.BasePattern pattern, Patterns.LineType startSideLine, Patterns.LineType endSideLine)
        {
            KeepSwitchActionsSequience nextSequience = currActionSequienceNode.Next?.Value;
            KeepSwitchActionsSequience nextNextSequience = currActionSequienceNode.Next?.Next?.Value;

            PathPoint lastPoint = points.Last.Value;

            float middlePos = lastPoint.pos.y + (float)lastPoint.height / 2;
            Line2 rightSwitchBorder = new Line2(new Vector2(switchEndBorder.point.x, switchEndBorder.direction.y > 0 ? -switchEndBorder.point.y + middlePos * 2 : switchEndBorder.point.y),
                                                new Vector2(1, -playerFallTan));
            Line2 leftSwitchBorder = new Line2(new Vector2(switchStartBorder.point.x, switchStartBorder.direction.y > 0 ? -switchStartBorder.point.y + middlePos * 2 : switchStartBorder.point.y),
                                                new Vector2(1, -playerFallTan));

            if (endSideLine.isSafe)
            {
                if (pattern.Angle != Patterns.AngleType._90)
                {
                    if (endSideLine.angle == Patterns.AngleType._0)
                    {
                        if (!startSideLine.isSafe)
                            return int.MaxValue;

                        float endSwitchPos = MyMath.GetIntersections(rightSwitchBorder, new Line2(lastPoint.pos, Vector2.right))[0].x;

                        if (nextSequience.DistanceToSwitch == -1)
                            return MyMath.Round(endSwitchPos + nextSequience.KeepLength * 2 - lastPoint.pos.x, MyMath.Direction.Down);

                        Line2 rightNextSwitchBorder = new Line2(new Vector2(endSwitchPos + nextSequience.KeepLength + nextSequience.DistanceToSwitch,
                                                                            lastPoint.pos.y),
                                                                new Vector2(1, playerFallTan));

                        Vector2 endNextNextKeepPos = default;

                        if (startSideLine.angle == Patterns.AngleType._0)
                        {
                            Vector2 endNextSwitchPos = MyMath.GetIntersections(new Line2(lastPoint.pos + lastPoint.height * Vector2.up, Vector2.right), rightNextSwitchBorder)[0];
                            endNextNextKeepPos = endNextSwitchPos + Vector2.right * nextNextSequience.KeepLength;
                        }

                        if (startSideLine.angle == Patterns.AngleType._45)
                        {
                            Vector2 endNextSwitchPos = MyMath.GetIntersections(new Line2(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right, new Vector2(1, 1)),
                                                                               rightNextSwitchBorder)[0];
                            endNextNextKeepPos = endNextSwitchPos + new Vector2(1, 1) * nextNextSequience.KeepLength;
                        }

                        if (startSideLine.angle == Patterns.AngleType._0 || startSideLine.angle == Patterns.AngleType._45)
                            return MyMath.Round(endNextNextKeepPos.x - 2 - lastPoint.pos.x, MyMath.Direction.Down);
                    }

                    if (endSideLine.angle == Patterns.AngleType._45)
                    {
                        float endSwitchPos = MyMath.GetIntersections(rightSwitchBorder, new Line2(lastPoint.pos + Vector2.right, new Vector2(1, -1)))[0].x;

                        if (nextSequience.DistanceToSwitch == -1)
                            return MyMath.Round(endSwitchPos + nextSequience.KeepLength * 2 - lastPoint.pos.x, MyMath.Direction.Down);

                        return MyMath.Round(endSwitchPos + nextSequience.KeepLength - lastPoint.pos.x, MyMath.Direction.Down);
                    }
                }
                else
                if (endSideLine.angle == Patterns.AngleType._90)
                    if (MyMath.GetIntersections(new Line2(lastPoint.pos + Vector2.right * 2, Vector2.down), rightSwitchBorder)[0].y <= lastPoint.pos.y + 1)
                        return 0;
            }

            if (!endSideLine.isSafe)
            {
                if (endSideLine.angle == Patterns.AngleType._45)
                    return Mathf.Clamp(MyMath.Round(MyMath.GetIntersections(new Line2(lastPoint.pos, new Vector2(1, 1)),
                                                                            leftSwitchBorder)[0].x -
                                                    lastPoint.pos.x, MyMath.Direction.Down),
                                       0, int.MaxValue);

                if (endSideLine.angle == Patterns.AngleType._90)
                    return Mathf.Clamp(MyMath.Round(MyMath.GetIntersections(new Line2(lastPoint.pos + Vector2.right, Vector2.up),
                                                                            leftSwitchBorder)[0].y -
                                                    lastPoint.pos.y, MyMath.Direction.Down),
                                       0, int.MaxValue);
            }

            return int.MaxValue;
        }

        private static int GetSwitchIntLength(KeepSwitchActionsSequience actionsSequience, float playerFallTan)
        {
            return MyMath.Round(actionsSequience.DistanceToSwitch + (1 / playerFallTan) - 1, MyMath.Direction.Up);
        }
    }
}
