﻿#define PLAYER_PATH_DEBUG

using UnityEngine;
using System.Collections.Generic;
using MyLibraries;

using Path;
using Patterns = Path.Patterns;

public class AbyssArea : AreaGenerator
{
    // path generation params
    private int Height { get { return 7; } }

    private IntRange KeepGravityPatternLengthRange { get { return new IntRange(3, 10); } }
    private IntRange SwitchGravityPatternLengthRange { get { return new IntRange(2, 6); } }
    private int PlayerPathPatternsCount { get { return Random.Range(5, 10); } }
    // path generation params

    public override IntRangeSet PossibleStartHeights(float difficulty)
    {
        return new IntRangeSet(new IntRange(Height));
    }

    protected override IEnumerator<LinkedList<PathPoint>> Generate(GenerateInfo info)
    {
        float playerFallTan = GlobalSpace.game.player.FallMovementTan();

        LinkedList<PathPoint> points = new LinkedList<PathPoint>();
        points.AddFirst(info.lastPoint);

        PlayerPath playerPath = new PlayerPath();
        playerPath.BasicBuild(DiscreteNormalDistributionByDifficulty(1 - info.difficulty, KeepGravityPatternLengthRange),
                                DiscreteNormalDistributionByDifficulty(1 - info.difficulty, SwitchGravityPatternLengthRange),
                                PlayerPathPatternsCount);

        MyMath.Direction playerState = (MyMath.Direction)MyRandom.RandomSign();
        foreach (var playerAction in playerPath)
        {
            if (playerAction.KeepLength > 0)
            {
                points.AddLast(new Patterns.Straight().Generate(points.Last.Value,
                                                                playerAction.KeepLength,
                                                                playerState == MyMath.Direction.Down,
                                                                playerState == MyMath.Direction.Up));

#if PLAYER_PATH_DEBUG
                info.area.localDebugLines.AddLast(new DebugLine(points.Last.Previous.Value.pos + (playerState == MyMath.Direction.Up ? Height : 0) * Vector2.up,
                                                                points.Last.Value.pos + (playerState == MyMath.Direction.Up ? Height : 0) * Vector2.up,
                                                                Color.yellow));
#endif
            }

            if (playerAction.DistanceToSwitch > 0)
            {
#if PLAYER_PATH_DEBUG
                info.area.localDebugLines.AddLast(new DebugLine(points.Last.Value.pos + (playerState == MyMath.Direction.Up ? Height : 0) * Vector2.up,
                                                                points.Last.Value.pos + (playerState == MyMath.Direction.Up ? Height : 0) * Vector2.up +
                                                                    new Vector2(1 / playerFallTan, -(int)playerState) * Height,
                                                                Color.yellow));

                info.area.localDebugLines.AddLast(new DebugLine(points.Last.Value.pos + (playerState == MyMath.Direction.Up ? Height : 0) * Vector2.up + playerAction.DistanceToSwitch * Vector2.right,
                                                                points.Last.Value.pos + (playerState == MyMath.Direction.Up ? Height : 0) * Vector2.up + playerAction.DistanceToSwitch * Vector2.right +
                                                                    new Vector2(1 / playerFallTan, -(int)playerState) * Height,
                                                                Color.yellow));
#endif

                int switchLength = playerAction.GetSwitchLength(Height, GlobalSpace.game.player.FallMovementTan());
                int startBorder = MyMath.Round(playerAction.DistanceToSwitch, MyMath.Direction.Up) - 1;
                int endBorder = MyMath.Round(Height / GlobalSpace.game.player.FallMovementTan(), MyMath.Direction.Down) + 1;

                int pos = 0;
                while (pos != switchLength)
                {
                    bool generateStart = pos < startBorder;
                    bool generateEnd = pos >= endBorder;

                    int startMaxArg = (pos < startBorder ? startBorder : switchLength) - pos;
                    int endMaxArg = (pos < endBorder ? endBorder : switchLength) - pos;

                    points.AddLast(new Patterns.Straight().Generate(points.Last.Value, Mathf.Min(startMaxArg, endMaxArg),
                                   playerState == MyMath.Direction.Down ? generateStart : generateEnd,
                                   playerState == MyMath.Direction.Up ? generateStart : generateEnd));

                    pos += Mathf.Min(startMaxArg, endMaxArg);
                }

                playerState = MyMath.Switch(playerState);
            }

            yield return null;
        }

        points.RemoveFirst();

        yield return points;
    }
}
