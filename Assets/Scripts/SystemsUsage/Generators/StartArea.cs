﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

using Path;
using Patterns = Path.Patterns;
using MyLibraries;

public class StartArea : AreaGenerator
{
    public override IntRangeSet PossibleStartHeights(float difficulty) { return IntRangeSet.Infinite; }

    protected override IEnumerator<LinkedList<PathPoint>> Generate(GenerateInfo info)
    {
        LinkedList<PathPoint> points = new LinkedList<PathPoint>();
        points.AddLast(new PathPoint(Vector2Int.zero, 7, null));
        points.AddLast(new Patterns.Straight().Generate(points.Last.Value, 30));
        yield return points;
    }
}
