﻿using System.Collections.Generic;
using UnityEngine;

using MyLibraries;
using Path;
using Patterns = Path.Patterns;

class RotatingPlanesArea : AreaGenerator
{
    // path generation params
    private int Height { get { return 7; } }
    private int BlockWidth(float difficulty) { return LerpParamByDifficulty(difficulty, 13, 5); }

    private int PlayerPathPatternsCount { get { return Random.Range(5, 10); } }
    // path generation params

    public override IntRangeSet PossibleStartHeights(float difficulty)
    {
        return new IntRangeSet(new IntRange(Height));
    }

    protected override IEnumerator<LinkedList<PathPoint>> Generate(GenerateInfo info)
    {
        if (BlockWidth(info.difficulty) / 2f > Height - 1)
            throw new System.Exception("not compatible height(" + Height + ") and BlockWith(" + BlockWidth(info.difficulty) + ")");

        //Build(out PlayerPath playerPath, info.difficulty);

        MyMath.Direction playerState = (MyMath.Direction)MyRandom.RandomSign();

        

        float length = 0.5f;
        for (int i = 0; i < PlayerPathPatternsCount; i++)
        {
            var rotatingBlock = LoadPrefab(info.area, GlobalSpace.game.prefabs.rotatingPlane,
                                           new Vector2(length + 0.5f + BlockWidth(info.difficulty) / 2f,
                                                       playerState == MyMath.Direction.Up ? Height + 0.5f : -0.5f),
                                           Quaternion.identity).GetComponent<RotatingPlane>();

            rotatingBlock.Width = BlockWidth(info.difficulty);
            rotatingBlock.Safe = true;
            rotatingBlock.RotDir = (int)playerState;

            rotatingBlock = LoadPrefab(info.area, GlobalSpace.game.prefabs.rotatingPlane,
                                       new Vector2(length + 0.5f + BlockWidth(info.difficulty) / 2f,
                                                   playerState == MyMath.Direction.Down ? Height + 0.5f : -0.5f),
                                       Quaternion.identity).GetComponent<RotatingPlane>();

            rotatingBlock.Width = BlockWidth(info.difficulty);
            rotatingBlock.Safe = false;
            rotatingBlock.RotDir = (int)playerState;

            length += BlockWidth(info.difficulty) + 1;
            playerState = MyMath.Switch(playerState);
        }

        LinkedList<PathPoint> points = new LinkedList<PathPoint>();
        points.AddLast(new Patterns.Straight().Generate(info.lastPoint, MyMath.Round(length + 0.5f), false, false));

        yield return points;
    }

    private void Build(out PlayerPath playerPath, float difficulty)
    {
        playerPath = new PlayerPath();

        int patternsCount = PlayerPathPatternsCount;
        float switchDistance = Mathf.Min(Height / GlobalSpace.game.player.FallMovementTan(), BlockWidth(difficulty) / 2);

        playerPath.AddLast(new KeepSwitchActionsSequience(MyMath.Round(BlockWidth(difficulty) - switchDistance, MyMath.Direction.Down), switchDistance));
        for (int i = 0; i < patternsCount; i++)
            playerPath.AddLast(new KeepSwitchActionsSequience(MyMath.Round(BlockWidth(difficulty) - switchDistance * 2, MyMath.Direction.Down), switchDistance));
    }
}
