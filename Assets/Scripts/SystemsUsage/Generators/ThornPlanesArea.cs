﻿//#define PLAYER_PATH_DEBUG

using UnityEngine;
using System.Collections.Generic;

using MyLibraries;

using Path;
using Patterns = Path.Patterns;

public class ThornPlanesArea : AreaGenerator
{
    // path generation params
    private int Height { get { return 7; } }

    private IntRange KeepGravityPatternLengthRange { get { return new IntRange(3, 10); } }
    private IntRange SwitchGravityPatternLengthRange { get { return new IntRange(2, 6); } }
    private int PlayerPathPatternsCount { get { return Random.Range(5, 10); } }
    // path generation params

    public override IntRangeSet PossibleStartHeights(float difficulty)
    {
        return new IntRangeSet(new IntRange(Height));
    }

    protected override IEnumerator<LinkedList<PathPoint>> Generate(GenerateInfo info)
    {
        var planeWithTwoSideThornsPrefab = GlobalSpace.game.prefabs.planeWithTwoSideThorns;
        var thornPrefab = GlobalSpace.game.prefabs.thorn;
        float playerFallTan = GlobalSpace.game.player.FallMovementTan();

        LinkedList<PathPoint> points = new LinkedList<PathPoint>();
        points.AddFirst(info.lastPoint);

        PlayerPath playerPath = new PlayerPath();
        playerPath.BasicBuild(DiscreteNormalDistributionByDifficulty(1 - info.difficulty, KeepGravityPatternLengthRange),
                                DiscreteNormalDistributionByDifficulty(1 - info.difficulty, SwitchGravityPatternLengthRange),
                                PlayerPathPatternsCount);

        MyMath.Direction playerState = (MyMath.Direction)MyRandom.RandomSign();

        float validThornShift = playerFallTan <= 2 ? 1 / playerFallTan : 0.5f;

        int firstLength = playerPath.First.Value.KeepLength + MyMath.Round(playerPath.First.Value.DistanceToSwitch + validThornShift) + 1;

        points.AddLast(new Patterns.Straight().Generate(points.Last.Value, firstLength, playerState == MyMath.Direction.Down, playerState == MyMath.Direction.Up));
        LoadPrefab(info.area, thornPrefab,
                   new Vector2(firstLength - 0.5f, 0.5f + (playerState == MyMath.Direction.Up ? Height - 1 : 0)),
                   Quaternion.Euler(0, 0, 90 + (int)playerState * 90));

#if PLAYER_PATH_DEBUG
        info.area.localDebugLines.AddLast(new DebugLine(new Vector2(0, playerState == MyMath.Direction.Up ? Height : 0),
                                                        new Vector2(playerPath.First.Value.KeepLength, playerState == MyMath.Direction.Up ? Height : 0),
                                                        Color.yellow));
        info.area.localDebugLines.AddLast(new DebugLine(new Vector2(playerPath.First.Value.KeepLength, playerState == MyMath.Direction.Up ? Height : 0),
                                                        new Vector2(playerPath.First.Value.KeepLength, playerState == MyMath.Direction.Up ? Height : 0) +
                                                            new Vector2(1 / playerFallTan, -(int)playerState) * Height,
                                                        Color.yellow));
        info.area.localDebugLines.AddLast(new DebugLine(new Vector2(playerPath.First.Value.KeepLength + playerPath.First.Value.DistanceToSwitch,
                                                                    playerState == MyMath.Direction.Up ? Height : 0),
                                                        new Vector2(playerPath.First.Value.KeepLength + playerPath.First.Value.DistanceToSwitch,
                                                        playerState == MyMath.Direction.Up ? Height : 0) +
                                                            new Vector2(1 / playerFallTan, -(int)playerState) * Height,
                                                        Color.yellow));
#endif

        playerState = MyMath.Switch(playerState);
        float startPos = playerPath.First.Value.KeepLength + Height / playerFallTan;
        float endPos;
        float prevSwitchDistance = playerPath.First.Value.DistanceToSwitch;

        int roundedStartPos;
        int roundedEndPos;

        for (var currNode = playerPath.First.Next; currNode != playerPath.Last; currNode = currNode.Next)
        {
            var playerAction = currNode.Value;

            endPos = startPos + prevSwitchDistance + playerAction.KeepLength + playerAction.DistanceToSwitch;

            roundedStartPos = MyMath.Round(startPos - validThornShift) - 1;
            roundedEndPos = MyMath.Round(endPos + validThornShift) + 1;

            var plane = LoadPrefab(info.area, planeWithTwoSideThornsPrefab,
                                   new Vector2((roundedStartPos + roundedEndPos) / 2f, -0.5f + (playerState == MyMath.Direction.Up ? Height + 1 : 0)),
                                   Quaternion.Euler(0, 0, 90 + (int)playerState * 90)).GetComponent<PlaneWithTwoSideThorns>();
            plane.Width = roundedEndPos - roundedStartPos;

#if PLAYER_PATH_DEBUG
            info.area.localDebugLines.AddLast(new DebugLine(new Vector2(startPos + prevSwitchDistance, playerState == MyMath.Direction.Up ? Height : 0),
                                                            new Vector2(startPos + prevSwitchDistance + playerAction.KeepLength,
                                                                        playerState == MyMath.Direction.Up ? Height : 0),
                                                            Color.yellow));
            info.area.localDebugLines.AddLast(new DebugLine(new Vector2(startPos + prevSwitchDistance + playerAction.KeepLength,
                                                                        playerState == MyMath.Direction.Up ? Height : 0),
                                                            new Vector2(startPos + prevSwitchDistance + playerAction.KeepLength,
                                                                        playerState == MyMath.Direction.Up ? Height : 0) +
                                                                new Vector2(1 / playerFallTan, -(int)playerState) * Height,
                                                            Color.yellow));
            info.area.localDebugLines.AddLast(new DebugLine(new Vector2(endPos, playerState == MyMath.Direction.Up ? Height : 0),
                                                            new Vector2(endPos, playerState == MyMath.Direction.Up ? Height : 0) +
                                                                new Vector2(1 / playerFallTan, -(int)playerState) * Height,
                                                            Color.yellow));
#endif

            playerState = MyMath.Switch(playerState);
            startPos += prevSwitchDistance + playerAction.KeepLength + Height / playerFallTan;
            prevSwitchDistance = playerAction.DistanceToSwitch;

            yield return null;
        }

        endPos = startPos + prevSwitchDistance + playerPath.Last.Value.KeepLength;
        roundedStartPos = MyMath.Round(startPos - validThornShift) - 1;
        roundedEndPos = MyMath.Round(endPos, MyMath.Direction.Up);

        points.AddLast(new Patterns.Straight().Generate(points.Last.Value, roundedStartPos - firstLength, false, false));
        points.AddLast(new Patterns.Straight().Generate(points.Last.Value, roundedEndPos - roundedStartPos, playerState == MyMath.Direction.Down, playerState == MyMath.Direction.Up));
        LoadPrefab(info.area, thornPrefab,
                   new Vector2(roundedStartPos + 0.5f, 0.5f + (playerState == MyMath.Direction.Up ? Height - 1 : 0)),
                   Quaternion.Euler(0, 0, 90 + (int)playerState * 90));

#if PLAYER_PATH_DEBUG
        info.area.localDebugLines.AddLast(new DebugLine(new Vector2(startPos + prevSwitchDistance, playerState == MyMath.Direction.Up ? Height : 0),
                                                        new Vector2(startPos + prevSwitchDistance + playerPath.Last.Value.KeepLength,
                                                                    playerState == MyMath.Direction.Up ? Height : 0),
                                                        Color.yellow));
#endif

        points.RemoveFirst();

        yield return points;
    }
}
