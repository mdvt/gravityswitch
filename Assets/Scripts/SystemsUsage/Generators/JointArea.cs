﻿using System.Collections.Generic;
using MyLibraries;

using Path;
using UnityEngine;
using Patterns = Path.Patterns;

public class JointArea : AreaGenerator
{
    public override IntRangeSet PossibleStartHeights(float difficulty) { return IntRangeSet.Infinite; }

    public int StraightPatternRandomRange { get { return 2; } }

    protected override IEnumerator<LinkedList<PathPoint>> Generate(GenerateInfo info)
    {
        LinkedList<PathPoint> points = new LinkedList<PathPoint>();
        IntRangeSet outHeights = info.additionalInfo as IntRangeSet;

        float playerFallAngleTan = GlobalSpace.game.player.FallMovementTan();

        points.AddLast(generateStraight(playerFallAngleTan, info.lastPoint));

        if (!outHeights.Contain(info.lastPoint.height))
        {
            points.AddLast(generateHeightChange(MyRandom.RandomFromRanges(outHeights), points.Last.Value));
            points.AddLast(generateStraight(playerFallAngleTan, points.Last.Value));
        }

        yield return points;
    }

    private PathPoint generateStraight(float playerFallAngleTan, PathPoint last)
    {
        int minLenght = (int)(last.height / playerFallAngleTan) + 1;
        return new Patterns.Straight().Generate(last, Random.Range(minLenght, minLenght + StraightPatternRandomRange));
    }

    private PathPoint generateHeightChange(int expectesHeight, PathPoint last)
    {
        Patterns.BasePattern pattern;
        if (expectesHeight > last.height)
            pattern = new Patterns.Expantion45();
        else
            pattern = new Patterns.Taper45();
        return pattern.Generate(last, (expectesHeight - last.height) * MyRandom.RandomSign());
    }
}
