﻿using System;
using UnityEngine;
using MyLibraries;
using Outlines;

class RotatingPlane : DynamicAreaObject
{
    [NonSerialized] public int Width;
    [NonSerialized] public bool Safe;
    [NonSerialized] public int RotDir;

    private OutlinePath outlinePath;
    private bool railsLoaded;
    private bool firstIter;

    protected override void afterCreate()
    {
        RotDir = (int)Mathf.Sign(RotDir);
        transform.localScale = new Vector3(Width, 1, 1);
        firstIter = true;
        railsLoaded = false;

        outlinePath = CreateOutlinePath(ColorizedPathDrawDir.Left, new Vector3(-(float)Width / 2, -0.5f, -1), true);

        for (int x = 0; x <= Width; x++)
            outlinePath.AddLastPoint(new Vector2Int(x, 1));
        for (int x = Width; x >= 0; x--)
            outlinePath.AddLastPoint(new Vector2Int(x, 0));

        Width++;
    }

    protected override void onUpdate(float playerGlobalPosByX)
    {
        float posDiff = playerGlobalPosByX - (transform.position.x - (float)Width / 2);

        if (MyMath.PositiveRemainder(((int)MyMath.Round(posDiff, Width, MyMath.Direction.Down) + (!Safe ? Width : 0)) / Width, 2) == 0)
        {
            transform.rotation = Quaternion.identity;
            if (firstIter || !railsLoaded)
            {
                outlinePath.ResetColors();
                outlinePath.Redraw();
                outlinePath.CreateRails();
                railsLoaded = true;
            }
        }
        else
        {
            if (railsLoaded)
                outlinePath.DeleteRails();

            float t = MyMath.PositiveRemainder(posDiff, Width) / Width;

            if (t < 0.5f)
            {
                transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(0, 90 * RotDir, t * t * 4));
            }
            else
                transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(180 * RotDir, 90 * RotDir, (1 - t) * (1 - t) * 4));

            if (firstIter || railsLoaded)
            {
                outlinePath.InitColors(outlinePath.IsFragmentDanger);
                outlinePath.Redraw();
                railsLoaded = false;
            }
        }

        firstIter = false;
        outlinePath.SyncOutlinesWithHandler();
    }

    protected override void GetMaxVisibleBorders(out float leftBorder, out float rightBorder)
    {
        float maxWidth = Mathf.Sqrt(1 + Width * Width);
        var pos = transform.localPosition;

        leftBorder = pos.x - maxWidth / 2 - AreaTransform.localPosition.x;
        rightBorder = pos.x + maxWidth / 2 - AreaTransform.localPosition.x;
    }

    protected override void GetMaxColliderBorders(out float leftBorder, out float rightBorder)
    {
        float maxWidth = Mathf.Sqrt(1 + Width * Width);
        var pos = transform.localPosition;

        leftBorder = pos.x - maxWidth / 2 - AreaTransform.localPosition.x;
        rightBorder = pos.x + maxWidth / 2 - AreaTransform.localPosition.x;
    }
}
