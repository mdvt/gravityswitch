﻿using UnityEngine;

[RequireComponent(typeof(Renderer)), DisallowMultipleComponent]
public class PalleteColoring : AreaComponent
{
    public bool dangerous;

    protected override void afterCreate()
    {
        ColorPallete pallete = GlobalSpace.game.InteractiveObjectsColors;
        GetComponent<Renderer>().material.color = dangerous ? pallete.danger : pallete.safe;
    }
}
