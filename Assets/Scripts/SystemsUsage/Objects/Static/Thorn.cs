﻿using System.Collections;
using UnityEngine;

public class Thorn : StaticAreaObject
{
    protected override IEnumerable GetLocalDangerPoints()
    {
        yield return Vector2.one * 0.5f;
        yield return -Vector2.one * 0.5f;
        yield return Vector2.right * 0.5f - Vector2.up * 0.5f;
        yield return -Vector2.right * 0.5f + Vector2.up * 0.5f;
    }
}
