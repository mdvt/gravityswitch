﻿using System;
using UnityEngine;

using Outlines;

public class Plane : StaticAreaObject
{
    [NonSerialized] public int Width;

    private OutlinePath outlinePath;

    protected override void afterCreate()
    {
        outlinePath = CreateOutlinePath(MyLibraries.ColorizedPathDrawDir.Left, 
                                        new Vector3(-Width / 2f, -0.5f, -1), true);

        for (int x = 0; x <= Width; x++)
            outlinePath.AddLastPoint(new Vector2Int(x, 1));
        for (int x = Width; x >= 0; x--)
            outlinePath.AddLastPoint(new Vector2Int(x, 0));

        outlinePath.InitColors(outlinePath.IsFragmentDanger);
        outlinePath.CreateRails();
        outlinePath.Redraw();

        transform.localScale = new Vector3(Width, 1, 1);
    }
}
