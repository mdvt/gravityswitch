﻿using System;

using UnityEngine;

using Outlines;

public class PlaneWithTwoSideThorns : StaticAreaObject
{
    [SerializeField] private Transform plane;
    [SerializeField] private Transform leftThorn;
    [SerializeField] private Transform rightThorn;

    [NonSerialized] public int Width;

    private OutlinePath outlinePath;

    protected override void afterCreate()
    {
        if (Width < 2)
            throw new Exception("Width can't be less then 2");

        outlinePath = CreateOutlinePath(MyLibraries.ColorizedPathDrawDir.Left,
                                        new Vector3(-Width / 2f, -0.5f, -1), true);

        for (int x = 0; x <= Width; x++)
            outlinePath.AddLastPoint(new Vector2Int(x, 1));
        for (int x = Width; x >= 0; x--)
            outlinePath.AddLastPoint(new Vector2Int(x, 0));

        outlinePath.InitColors(outlinePath.IsFragmentDanger);
        outlinePath.InitColors(delegate (Vector2Int first, Vector2Int second)
        {
            return first.y == 1 && second.y == 1 &&
                     (first.x == 0 && second.x == 1 ||
                     first.x == Width - 1 && second.x == Width);
        });

        outlinePath.CreateRails();
        outlinePath.Redraw();

        plane.localScale = new Vector3(Width, 1, 1);
        leftThorn.localPosition = new Vector3(-Width / 2f + 0.5f, 1);
        rightThorn.localPosition = new Vector3(Width / 2f - 0.5f, 1);
    }
}