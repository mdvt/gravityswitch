﻿using UnityEngine;
using UnityEngine.UI;

namespace MyGUI
{
    public class LevelCanvasManager: BaseCanvasManager
    {
        public GameObject PauseButton;
        public GameObject ContinueButton;

        public Text Distance;

        public void SetDifficulty(float difficulty, int distance)
        {
            Distance.text = ((int)(difficulty * 100)).ToString() + "%   " + distance;
        }

        /*Buttons*/
        public void Pause()
        {
            GlobalSpace.game.SetPause(true);
            PauseButton.SetActive(false);
            ContinueButton.SetActive(true);
        }

        public void Continue()
        {
            GlobalSpace.game.SetPause(false);
            ContinueButton.SetActive(false);
            PauseButton.SetActive(true);
        }
        /*Buttons*/
    }
}
