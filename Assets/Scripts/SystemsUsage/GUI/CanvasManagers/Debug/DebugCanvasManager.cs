﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace MyGUI
{
    public class DebugCanvasManager : BaseCanvasManager
    {
        protected override void OnInit()
        {
            enabled = true;
        }
    }
}
