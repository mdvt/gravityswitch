﻿using UnityEngine;
using UnityEngine.UI;

namespace MyGUI
{
    public abstract class BaseCanvasManager : MonoBehaviour
    {
        protected Canvas canvas;
        protected GraphicRaycaster raycaster;
        protected CanvasScaler scaler;

        public void Init()
        {
            canvas = GetComponent<Canvas>();
            raycaster = GetComponent<GraphicRaycaster>();
            scaler = GetComponent<CanvasScaler>();

            OnInit();
        }

        protected virtual void OnInit() { }

        public void Hide()
        {
            canvas.enabled = false;
        }

        public void Show()
        {
            canvas.enabled = true;

            /*scaler.enabled = false;
            scaler.enabled = true; // i hate this but still this is the only solution to fix the blurry text after change canvas*/
        }

        public void DeactivateInput()
        {
            raycaster.enabled = false;
        }

        public void ActivateInput()
        {
            raycaster.enabled = true;
        }
    }
}
