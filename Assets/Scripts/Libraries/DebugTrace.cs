﻿using UnityEngine;
using System.Collections.Generic;

namespace MyLibraries
{
    public class DebugTrace
    {
        public LinkedList<Vector3> trace { get; private set; }
        public float step;
        public float traceLength;
        public Color color;

        public DebugTrace(float traceLength, float step, Color color)
        {
            this.traceLength = traceLength;
            this.step = step;
            this.color = color;

            trace = new LinkedList<Vector3>();
        }

        public void Update(Vector3 pos, bool draw = true)
        {
            if (traceLength <= 0)
                return;

            if (trace.Count == 0 || ((trace.Last.Value - pos).sqrMagnitude > step * step))
                trace.AddLast(pos);

            while (trace.Count * step > traceLength)
                trace.RemoveFirst();

            if (draw)
                Draw(pos);
        }

        public void Draw(Vector3 pos)
        {
            Draw();

            if (pos != trace.Last.Value)
                Debug.DrawLine(trace.Last.Value, pos, color);
        }

        public void Draw()
        {
            if (trace.Count == 0)
                return;

            Vector3 prev = trace.First.Value;
            bool firstIter = true;

            foreach (Vector3 point in trace)
            {
                if (firstIter)
                {
                    firstIter = false;
                    continue;
                }

                Debug.DrawLine(prev, point, color);
                prev = point;
            }
        }
    }
}