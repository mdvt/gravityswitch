﻿using UnityEngine;

namespace MyLibraries
{
    public class MyTrail
    {
        LineRenderer line;

        public MyTrail(LineRenderer line, int segments)
        {
            this.line = line;
            line.positionCount = segments;
            Reset();
        }

        public void Reset()
        {
            for (int i = 0; i < line.positionCount; i++)
                line.SetPosition(i, line.transform.position);
        }

        public void AddPoint()
        {
            for (int i = line.positionCount - 1; i > 0; i--)
                line.SetPosition(i, line.GetPosition(i - 1));

            line.SetPosition(0, line.transform.position);
        }

        public void Teleport()
        {
            Vector3 delta = line.transform.position - line.GetPosition(0);
            for (int i = 0; i < line.positionCount; i++)
                line.SetPosition(i, line.GetPosition(i) + delta);
        }
    }
}