﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyLibraries
{
    public static class MyMath
    {
        public enum Direction
        {
            Up = 1,
            None = 0,
            Down = -1
        }

        public static Direction Switch(Direction normal)
        {
            return normal == Direction.Down ? Direction.Up : Direction.Down;
        }

        public static float Round(float value, float step, Direction dir)
        {
            return step * Round((value + (int)dir * step / 2) / step);
        }

        /// <summary>
        /// Round value to up if sign(dir) == {1} or down if sign(dir) == -1 or nearest dir if sign(dir) == 0
        /// </summary>
        /// <param name="value"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        public static int Round(float value, Direction dir)
        {
            return Round(value + (float)dir / 2);
        }

        /// <summary>
        /// Round value to nearest integer
        /// </summary>
        /// <param name="value"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public static float Round(float value, float step)
        {
            return step * Round(value / step);
        }

        public static int Round(float value)
        {
            return (int)(value > 0 ? value + 0.5f : value - 0.5f);
        }

        public static bool IsFloatNearToInt(float value, float error = float.Epsilon)
        {
            return Mathf.Abs(Mathf.Round(value) - value) < error;
        }

        public static bool Approximately(float valA, float valB, float error = float.Epsilon)
        {
            return Mathf.Abs(valA - valB) < error;
        }

        public static int Sign(float value)
        {
            if (value > 0) return 1;
            if (value < 0) return -1;

            return 0;
        }

        public static bool InRange(float value, float minValue, float maxValue)
        {
            return value >= minValue && value <= maxValue;
        }

        public static float PositiveRemainder(float numerator, float denominator)
        {
            float res = numerator % denominator;
            if (res < 0) res += denominator;
            return res;
        }

        /// <summary>
        /// mapping val from range r1 to r2
        /// </summary>
        /// <param name="val"></param>
        /// <param name="r1Start"></param>
        /// <param name="r1End"></param>
        /// <param name="r2Start"></param>
        /// <param name="r2End"></param>
        /// <returns></returns>
        public static float Map(float val, float r1Start, float r1End, float r2Start, float r2End)
        {
            return r2Start + (val / (r1End - r1Start)) * (r2End - r2Start);
        }

        public static Func<float, float> ByArgAbs(this Func<float, float> originalFunc)
        {
            return x => originalFunc(Math.Abs(x));
        }

        public static Vector2[] GetIntersections(Figures.Line2 first, Figures.Line2 second)
        {
            if ((first.direction.x == 0 && second.direction.x == 0) ||
                (first.direction.x != 0 && second.direction.x != 0 &&
                 Mathf.Approximately(first.direction.y / first.direction.x, second.direction.y / second.direction.x)))
                return new Vector2[0];

            Vector2 fp = first.point;
            Vector2 fd = first.direction;
            Vector2 sp = second.point;
            Vector2 sd = second.direction;

            float parameter = (sd.y * (fp.x - sp.x) + sd.x * (sp.y - fp.y)) / (sd.x * fd.y - fd.x * sd.y);

            return new Vector2[1] { first.point + parameter * first.direction };
        }
    }

    /// <summary>
    /// Discrete imagination of original float function as uniform values array and linear interpolation between them
    /// </summary>
    public class DiscreteFunction
    {
        private float[] Values;
        public Range ValuesRange { get; private set; }
        public Func<float, float> OriginalFunction { get; private set; }

        public int StoredValuesCount { get { return Values.Length; } }
        public float this[int index] { get { return Values[index]; } }

        public DiscreteFunction(int storedValuesCount, float rangeStart, float rangeEnd, Func<float, float> originalFunction)
        {
            Values = new float[storedValuesCount];
            ValuesRange = new Range(rangeStart, rangeEnd);
            OriginalFunction = originalFunction;
            SyncValues();
        }

        public float GetValue(float arg)
        {
            if (!MyMath.InRange(arg, ValuesRange.start, ValuesRange.end))
                throw new ArgumentException("function argument(" + arg + " must stay in arguments range" + ValuesRange);

            float mappedArg = MyMath.Map(arg, ValuesRange.start, ValuesRange.end, 0, Values.Length - 1);

            return Mathf.Lerp(Values[(int)mappedArg], Values[(int)mappedArg + 1], mappedArg - (int)mappedArg);
        }

        public float MapIndexToArgument(int index)
        {
            return MyMath.Map(index, 0, Values.Length - 1, ValuesRange.start, ValuesRange.end);
        }

        public void SetOriginalFunction(Func<float, float> newFunc, bool syncImmediatly = true)
        {
            OriginalFunction = newFunc;

            if (syncImmediatly)
                SyncValues();
        }

        public void SetStoredValuesCount(int storedValuesCount, bool syncImmediatly = true)
        {
            Values = new float[storedValuesCount];

            if (syncImmediatly)
                SyncValues();
        }

        public void SetValuesRange(float start, float end, bool syncImmediatly = true)
        {
            ValuesRange.Set(start, end);

            if (syncImmediatly)
                SyncValues();
        }

        public void SyncValues()
        {
            for (int i = 0; i < Values.Length; i++)
                Values[i] = OriginalFunction(MyMath.Map(i, 0, Values.Length - 1, ValuesRange.start, ValuesRange.end));
        }
    }
}
