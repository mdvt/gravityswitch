﻿using UnityEngine;
using System;

namespace MyLibraries
{
    namespace Figures
    {
        public struct Line2
        {
            public Vector2 point;
            public Vector2 direction;

            public Line2(Vector2 point, Vector2 direction)
            {
                if (direction == Vector2.zero)
                    throw new Exception("not valid direction");

                this.point = point;
                this.direction = direction;
            }

            public void Set(Vector2 point, Vector2 direction)
            {
                if (direction == Vector2.zero)
                    throw new Exception("not valid direction");

                this.point = point;
                this.direction = direction;
            }

            public float distanceFrom(Vector2 point)
            {
                return perpendicularFrom(point).magnitude;
            }

            public Vector2 perpendicularFrom(Vector2 point)
            {
                return Vector3.Project(this.point - point, Vector2.Perpendicular(direction));
            }

            public static bool operator ==(Line2 left, Line2 right)
            {
                return left.point == right.point && left.direction == right.direction;
            }

            public static bool operator !=(Line2 left, Line2 right)
            {
                return !(left == right);
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                return base.Equals(obj);
            }

            public static explicit operator Line2(Ray2 ray)
            {
                return new Line2(ray.point, ray.direction);
            }
        }

        public struct Ray2
        {
            public Vector2 point;
            public Vector2 direction;

            public Ray2(Vector2 point, Vector2 direction)
            {
                if (direction == Vector2.zero)
                    throw new Exception("not valid direction");

                this.point = point;
                this.direction = direction;
            }
        }

        public struct Line2Segment
        {
            public Vector2 startPoint;
            public Vector2 endPoint;

            public Line2Segment(Vector2 startPoint, Vector2 endPoint)
            {
                this.startPoint = startPoint;
                this.endPoint = endPoint;
            }

            public static implicit operator Line2Segment(Line3Segment segment)
            {
                return new Line2Segment(segment.startPoint, segment.endPoint);
            }
        }

        public struct Line3Segment
        {
            public Vector3 startPoint;
            public Vector3 endPoint;

            public Line3Segment(Vector3 startPoint, Vector3 endPoint)
            {
                this.startPoint = startPoint;
                this.endPoint = endPoint;
            }

            public static implicit operator Line3Segment(Line2Segment segment)
            {
                return new Line3Segment(segment.startPoint, segment.endPoint);
            }
        }
    }
}
