﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

namespace MyLibraries
{
    public static class MyRandom
    {
        public static int RandomSign()
        {
            return Random.Range(0, 2) * 2 - 1;
        }

        public static float RandomRange(float min, float max, float average)
        {
            if (average < min || average > max)
                throw new System.Exception("Average must be beetwin min and max");

            if (average - min < max - average && (average - min == 0 || Random.Range(0f, 1f) > 1 / (((max - average) / (average - min)) + 1)) ||
                average - min > max - average && (max - average != 0 && Random.Range(0f, 1f) < 1 / (((average - min) / (max - average)) + 1)))
                return Random.Range(min, average);
            else
                return Random.Range(average, max);
        }

        public static int RandomFromRanges(IntRangeSet rangeSet)
        {
            int rangesSum = 0;
            foreach (IntRange r in rangeSet.elements)
                rangesSum += r.Length;

            int rnd = Random.Range(0, rangesSum + 1);

            foreach (IntRange r in rangeSet.elements)
            {
                if (r.Length < rnd)
                    rnd -= r.Length;
                else
                    return r.start + rnd;
            }

            throw new Exception("Empty range set");
        }

        public static T RandomOptionByProbability<T>(IEnumerable<KeyValuePair<T, float>> OptionsWithProbability)
        {
            return RandomOptionByProbability(MyLibrary.ToModifiableKeyValuePairsEnumerable(OptionsWithProbability));
        }

        public static T RandomOptionByProbability<T>(IEnumerable<ModifiableKeyValuePair<T, float>> OptionsWithProbability)
        {
            float sum = 0;
            foreach (ModifiableKeyValuePair<T, float> pair in OptionsWithProbability)
            {
                sum += pair.Value;
            }

            if (sum == 0)
                return default(T);

            float rnd = Random.Range(0, sum);

            foreach (ModifiableKeyValuePair<T, float> pair in OptionsWithProbability)
            {
                if (pair.Value < rnd)
                    rnd -= pair.Value;
                else
                    return pair.Key;
            }

            throw new Exception("I will never happen");
        }

        /// <summary>
        /// Generates random value in given range with linear function of distribution density.
        /// line of distribution density function is defined by two points in {value, probability >= 0} space
        /// math model: https://www.desmos.com/calculator/yckuh5yued
        /// </summary>
        /// <param name="rangeStart"></param>
        /// <param name="rangeEnd"></param>
        /// <param name="startProbability"></param>
        /// <param name="endProbability"></param>
        /// <returns></returns>
        public static float RandomWithLinearDistributionDensity(float rangeStart, float rangeEnd, float startProbability, float endProbability)
        {
            return RandomWithLinearDistributionDensity(rangeStart, rangeEnd, startProbability, endProbability, Random.Range(0, 1));
        }

        /// <summary>        
        /// Transforming random value in (0,1) range from uniform to linear function of distribution density with given range.
        /// line of distribution density function is defined by two points in {value, probability >= 0} space
        /// math model: https://www.desmos.com/calculator/yckuh5yued
        /// </summary>
        /// <param name="rangeStart"></param>
        /// <param name="rangeEnd"></param>
        /// <param name="startProbability"></param>
        /// <param name="endProbability"></param>
        /// <param name="uniformRandom"></param>
        /// <returns></returns>
        private static float RandomWithLinearDistributionDensity(float rangeStart, float rangeEnd, float startProbability, float endProbability, float uniformRandom)
        {
            if (!MyMath.InRange(uniformRandom, 0, 1))
                throw new InvalidOperationException();

            if (startProbability < 0 || endProbability < 0 || rangeEnd < rangeStart)
                throw new InvalidOperationException();

            if (startProbability == endProbability) return Random.Range(rangeStart, rangeEnd);
            if (rangeStart == rangeEnd) return rangeStart;

            startProbability /= (startProbability + endProbability) / 2;

            /*linear distribution density function coefficients k*x+b*/
            float k = 2 - 2 * startProbability;
            float b = startProbability;
            /*linear distribution density function coefficients k*x+b*/

            /*quadratic distribution function coefficients a*x^2+z*x*/
            float a = k / 2;
            float z = (b + startProbability) / 2;
            /*quadratic distribution function coefficients a*x^2+z*x*/

            /*inversed quadratic distribution function coefficients*/
            float h = -z / (2 * a);
            float g = -(z * z) / (4 * a);
            /*inversed quadratic distribution function coefficients*/

            return Mathf.Lerp(rangeStart, rangeEnd, MyMath.Sign(a) * Mathf.Sqrt((uniformRandom - g) / a) + h);
        }

        public static float RandomByDistributionDensity(DiscreteFunction distributionDensity)
        {
            if (distributionDensity.ValuesRange.start == distributionDensity.ValuesRange.end)
                return distributionDensity.ValuesRange.start;

            float intervalsProbabilitySum = 0;
            for (int i = 0; i < distributionDensity.StoredValuesCount - 1; i++)
                intervalsProbabilitySum += (distributionDensity[i] + distributionDensity[i + 1]) / 2;

            if (intervalsProbabilitySum <= 0)
                throw new Exception("probability integral can not be zero or negative");

            float rnd = Random.Range(0, intervalsProbabilitySum);

            int ind;
            float intervalProbability = 0;
            for (ind = 0; ind < distributionDensity.StoredValuesCount - 1; ind++)
            {
                intervalProbability = (distributionDensity[ind] + distributionDensity[ind + 1]) / 2;
                if (rnd <= intervalProbability && intervalProbability > 0)
                    break;
                rnd -= intervalProbability;
            }

            float funcRangeStart = MyMath.Map(ind, 0, distributionDensity.StoredValuesCount - 1,
                                                distributionDensity.ValuesRange.start, distributionDensity.ValuesRange.end);
            float funcRangeEnd = MyMath.Map(ind + 1, 0, distributionDensity.StoredValuesCount - 1,
                                                distributionDensity.ValuesRange.start, distributionDensity.ValuesRange.end);
            float startVal = distributionDensity[ind];
            float endVal = distributionDensity[ind + 1];
            float remainingRandom = MyMath.Map(rnd, 0, intervalProbability, 0, 1);

            return RandomWithLinearDistributionDensity(funcRangeStart, funcRangeEnd, startVal, endVal, remainingRandom);
        }

        private static float sqrt2PI = (float)Math.Sqrt(2 * Math.PI);
        public static Func<float, float> NormalDistributionDensityFunc(float standartDeviation, float expectedValue)
        {
            if (standartDeviation < 0)
                throw new ArgumentException("deviation(" + standartDeviation + ") can not be negative");

            return x => Mathf.Exp((-(x - expectedValue) * (x - expectedValue)) / (2 * standartDeviation * standartDeviation)) / (standartDeviation * sqrt2PI);
        }

        public static Func<float, float> TriangleDistributionDensityFunc(float maxDeviation, float expectedValue)
        {
            if (maxDeviation < 0)
                throw new ArgumentException("deviation(" + maxDeviation + ") can not be negative");

            float k = 1 / maxDeviation;
            float b = 1 - expectedValue / maxDeviation;
            return x => Mathf.Max(0, Mathf.Min(k * x + b, -k * x + (2 - b)));
        }

        public static Func<float, float> UniformDistributionDensityFunc(float maxDeviation, float expectedValue)
        {
            if (maxDeviation < 0)
                throw new ArgumentException("deviation(" + maxDeviation + ") can not be negative");

            return x => MyMath.InRange(x, expectedValue - maxDeviation, expectedValue + maxDeviation) ? 1 : 0;
        }
    }
}