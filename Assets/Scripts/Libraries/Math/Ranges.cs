﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MyLibraries
{
    public class Range
    {
        public static Range Infinite { get { return new Range(float.MinValue, float.MaxValue); } }

        public float start { get; private set; }
        public float end { get; private set; }
        public float Length { get { return end - start; } }

        public Range(float start, float end)
        {
            Set(start, end);
        }

        public Range(float val)
        {
            Set(val, val);
        }

        public void Set(float start, float end)
        {
            if (start > end)
                throw new System.ArgumentException("start(" + start + ") of the range cannot be >= then it's end(" + end + ")");
            this.start = start;
            this.end = end;
        }

        public Range GetUnion(Range other)
        {
            if (other.start > end || start > other.end)
                return null;

            return new Range(Math.Min(start, other.start), Math.Max(end, other.end));
        }

        public Range GetIntersection(Range other)
        {
            if (other.start > end || start > other.end)
                return null;

            return new Range(Math.Max(start, other.start), Math.Min(end, other.end));
        }

        public bool Contain(int val)
        {
            return val >= start && val <= end;
        }

        public override string ToString()
        {
            return "(" + start + ", " + end + ")";
        }
    }

    public class IntRange : IEnumerable<int>
    {
        public static IntRange Infinite { get { return new IntRange(int.MinValue, int.MaxValue); } }

        public int start { get; private set; }
        public int end { get; private set; }
        public int Length { get { return end - start; } }

        public IntRange(int start, int end)
        {
            Set(start, end);
        }

        public IntRange(int val)
        {
            Set(val, val);
        }

        public void Set(int start, int end)
        {
            if (start > end)
                throw new ArgumentException("start(" + start + ") of the range cannot be >= then it's end(" + end + ")");
            this.start = start;
            this.end = end;
        }

        public IntRange GetUnion(IntRange other)
        {
            if (other.start > end + 1 || start > other.end + 1)
                return null;

            return new IntRange(Math.Min(start, other.start), Math.Max(end, other.end));
        }

        public IntRange GetIntersection(IntRange other)
        {
            if (other.start > end || start > other.end)
                return null;

            return new IntRange(Math.Max(start, other.start), Math.Min(end, other.end));
        }

        public bool Contain(int val)
        {
            return val >= start && val <= end;
        }

        public override string ToString()
        {
            return "(" + start + ", " + end + ")";
        }

        public IEnumerator<int> GetEnumerator()
        {
            for (int i = start; i <= end; i++)
                yield return i;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class IntRangeSet//ready to work woth float ranges
    {
        public static IntRangeSet Infinite { get { return new IntRangeSet(IntRange.Infinite); } }

        public LinkedList<IntRange> elements { get; private set; }
        public bool IsEmpty { get { return elements.Count == 0; } }

        public IntRangeSet(params IntRange[] ranges)
        {
            elements = new LinkedList<IntRange>();
            foreach (IntRange r in ranges)
                AddRange(r);
        }

        public void AddRange(IntRange toAdd)
        {
            LinkedListNode<IntRange> r = elements.First;

            if (IsEmpty)
            {
                elements.AddFirst(toAdd);
                return;
            }

            if (toAdd.start < r.Value.start)
            {
                elements.AddFirst(toAdd);
                r = elements.First;
            }
            else
            {
                while (r != null)//setting up toAdd to it's place
                {
                    if (r.Value.start <= toAdd.start && (r.Next == null || r.Next.Value.start > toAdd.start))
                    {
                        IntRange union = toAdd.GetUnion(r.Value);
                        if (union == null)
                        {
                            elements.AddAfter(r, toAdd);
                            r = r.Next;
                        }
                        else
                            r.Value = union;
                        break;
                    }
                    r = r.Next;
                }//r = toAdd or union of toAdd and first intersecting interval
            }

            while (r.Next != null)//add all intersecting with toAdd intervals to union
            {
                IntRange union = r.Value.GetUnion(r.Next.Value);

                if (union == null)
                    break;

                r.Value = union;
                elements.Remove(r.Next);
            }
        }

        public IntRangeSet GetIntersection(IntRangeSet other)
        {
            IntRangeSet result = new IntRangeSet();
            LinkedList<IntRange>[] sets = new LinkedList<IntRange>[] { elements, other.elements };
            LinkedListNode<IntRange>[] r = new LinkedListNode<IntRange>[] { sets[0].First, sets[1].First };
            int mode;
            int notMode;
            while (r[0] != null && r[1] != null)
            {
                if (r[0].Value.start < r[1].Value.start)
                    mode = 0;
                else
                    mode = 1;
                notMode = (mode + 1) % 2;

                IntRange intersection;
                bool anyAdded = false;
                do
                {
                    intersection = r[mode].Value.GetIntersection(r[notMode].Value);
                    if (intersection != null)
                    {
                        anyAdded = true;
                        result.elements.AddLast(intersection);
                        r[notMode] = r[notMode].Next;
                    }
                } while (intersection != null && r[notMode] != null);

                if (anyAdded)
                {
                    if (r[notMode] == null)
                        r[notMode] = sets[notMode].Last;
                    else
                        r[notMode] = r[notMode].Previous;
                }
                r[mode] = r[mode].Next;
            }
            return result;
        }

        public bool Contain(int val)
        {
            foreach (var element in elements)
                if (element.Contain(val))
                    return true;
            return false;
        }

        public override string ToString()
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder();

            result.Append("{ ");
            foreach (IntRange r in elements)
            {
                result.Append(r);
                result.Append(" ");
            }
            result.Append("}");

            return result.ToString();
        }
    }
}