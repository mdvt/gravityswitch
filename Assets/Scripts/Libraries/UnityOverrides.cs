﻿using UnityEngine;
using System.Reflection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

using MyLibraries;

public static class UnityOverrides
{
    public static T GetPropertyValue<T>(this Type type, object obj, string name)
    {
        PropertyInfo field = type.GetProperty(name);

        if (field == null)
            throw new Exception("Cannot find property '" + name + "' in " + type);

        return (T)field.GetValue(obj);
    }

    public static List<T> CreateInstancesOfInheritors<T>()
    {
        Type[] types = Assembly.GetAssembly(typeof(T)).GetTypes().Where(x => x.IsSubclassOf(typeof(T))).ToArray();

        List<T> instances = new List<T>(types.Length);
        for (int i = 0; i < types.Length; i++)
            instances.Add((T)Activator.CreateInstance(types[i]));

        return instances;
    }

    public static void Copy(this Transform transform, Transform other)
    {
        transform.position = other.position;
        transform.rotation = other.rotation;
        transform.localScale = other.localScale;
    }

    public static void AsyncDestroy(this Transform transform, MonoBehaviour destroyer)
    {
        CoroutineProcess<Transform> destroyProcess = new CoroutineProcess<Transform>(destroyer, AsyncDestroyIEnumerator, transform);
        destroyProcess.Start();
    }

    private static IEnumerator AsyncDestroyIEnumerator(Transform transform)
    {
        Stack<Transform> objects = new Stack<Transform>();
        objects.Push(transform);

        while (objects.Count != 0)
        {
            if (objects.Peek().childCount != 0)
                objects.Push(objects.Peek().GetChild(0));
            else
                UnityEngine.Object.DestroyImmediate(objects.Pop().gameObject);

            yield return null;
        }
    }

    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;

        return new Vector2((cos * tx) - (sin * ty), (sin * tx) + (cos * ty));
    }

    public static Vector2Int Left(this Vector2Int v)
    {
        return new Vector2Int(-v.y, v.x);
    }

    public static Vector2Int Right(this Vector2Int v)
    {
        return new Vector2Int(v.y, -v.x);
    }

    public static Vector2Int Back(this Vector2Int v)
    {
        return new Vector2Int(-v.x, -v.y);
    }

    public static Vector2 AngleLerp(Vector2 a, Vector2 b, float t)
    {
        t = Mathf.Clamp01(t);

        float startAngle = Vector2.SignedAngle(Vector2.up, a);
        float deltaAngle = Vector2.SignedAngle(a, b);

        if (deltaAngle < 0)
            deltaAngle += 360;

        return Quaternion.Euler(0, 0, startAngle + deltaAngle * t) * Vector2.up;
    }

    /// <summary>
    /// Return Vector2Int with rounded values
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector2Int ToVector2Int(this Vector2 v)
    {
        return new Vector2Int(MyMath.Round(v.x), MyMath.Round(v.y));
    }

    public static int RemoveAll<T>(this LinkedList<T> linkedList, Func<T, bool> predicate)
    {
        int i = 0;
        for (LinkedListNode<T> node = linkedList.First; node != null;)
        {
            LinkedListNode<T> next = node.Next;
            if (predicate(node.Value))
            {
                linkedList.Remove(node);
                i++;
            }
            node = next;
        }

        return i;
    }

    public static int RemoveAll<K, V>(this Dictionary<K, V> dictionary, Func<K, bool> predicate)
    {
        LinkedList<K> keys = new LinkedList<K>(dictionary.Keys);

        int i = 0;
        foreach (K key in keys)
        {
            if (predicate(key))
            {
                dictionary.Remove(key);
                i++;
            }
        }

        return i;
    }
}