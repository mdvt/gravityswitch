﻿using Action = System.Action;
using System;
using System.Collections;
using UnityEngine;

namespace MyLibraries
{
    public abstract class CoroutineObjectBase
    {
        public MonoBehaviour Owner { get; protected set; }
        public Coroutine Coroutine { get; protected set; }
        protected abstract IEnumerator Process();

        public Action Finished;
        public bool IsProcessing;

        public void Start()
        {
            Stop();
            Coroutine = Owner.StartCoroutine(ProcessHandler());
        }

        private IEnumerator ProcessHandler()
        {
            IsProcessing = true;
            yield return Process();
            IsProcessing = false;
            Finished?.Invoke();
        }

        public void Stop()
        {
            if (!IsProcessing)
                return;

            Owner.StopCoroutine(Coroutine);
            IsProcessing = false;
        }
    }

    public class CoroutineObject : CoroutineObjectBase
    {
        public Func<IEnumerator> Routine { get; private set; }

        public CoroutineObject(MonoBehaviour owner, Func<IEnumerator> routine)
        {
            Owner = owner;
            Routine = routine;
        }

        public void SetRoutine(Func<IEnumerator> routine)
        {
            if (IsProcessing) throw new InvalidOperationException("Cannot set routine while another routine is running");
            Routine = routine;
        }

        protected override IEnumerator Process()
        {
            yield return Routine.Invoke();
        }
    }

    public class CoroutineObject<T> : CoroutineObjectBase
    {
        public Func<T, IEnumerator> Routine { get; private set; }

        protected T arg;

        public CoroutineObject(MonoBehaviour owner, Func<T, IEnumerator> routine, T arg)
        {
            Owner = owner;
            SetRoutine(routine);
            SetArg(arg);
        }

        public void SetRoutine(Func<T, IEnumerator> routine)
        {
            if (IsProcessing) throw new InvalidOperationException("Cannot set routine while another routine is running");
            Routine = routine;
        }

        public void SetArg(T arg)
        {
            if (IsProcessing) throw new InvalidOperationException("Cannot set argument for running coroutine");
            this.arg = arg;
        }

        protected override IEnumerator Process()
        {
            yield return Routine.Invoke(arg);
        }
    }
}