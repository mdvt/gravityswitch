﻿using UnityEngine;
using System;
using System.Collections;

namespace MyLibraries
{
    public class CoroutineProcess<T> : CoroutineObject<T>
    {
        private float ProcessTimePerFrame;

        public CoroutineProcess(MonoBehaviour owner, Func<T, IEnumerator> routine, T arg, float processTimePerFrameInMicrosec = 3000) :
            base(owner, routine, arg)
        {
            ProcessTimePerFrame = processTimePerFrameInMicrosec / 1000000f;
        }

        protected override IEnumerator Process()
        {
            float startTime;
            var routine = Routine.Invoke(arg);
            bool routineRunning = true;

            while (routineRunning)
            {
                startTime = Time.realtimeSinceStartup;
                while ((routineRunning = routine.MoveNext()) && Time.realtimeSinceStartup - startTime < ProcessTimePerFrame) ;
                yield return routine.Current;
            }
        }
    }
    public class CoroutineProcess : CoroutineObject
    {
        private float ProcessTimePerFrame;

        public CoroutineProcess(MonoBehaviour owner, Func<IEnumerator> routine, float processTimePerFrameInMicrosec = 3000) :
            base(owner, routine)
        {
            ProcessTimePerFrame = processTimePerFrameInMicrosec / 1000000f;
        }

        protected override IEnumerator Process()
        {
            float startTime;
            var routine = Routine.Invoke();
            bool routineRunning = true;

            while (routineRunning)
            {
                startTime = Time.realtimeSinceStartup;
                while ((routineRunning = routine.MoveNext()) && Time.realtimeSinceStartup - startTime < ProcessTimePerFrame) ;
                yield return routine.Current;
            }
        }
    }
}