﻿using System.Collections.Generic;
using MyLibraries.Figures;
using UnityEngine;

namespace MyLibraries
{
    public static class MyLibrary
    {
        public static IEnumerable<ModifiableKeyValuePair<TKey, TValue>> ToModifiableKeyValuePairsEnumerable<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> enumerable)
        {
            ModifiableKeyValuePair<TKey, TValue> current;
            foreach (var pair in enumerable)
            {
                current.Value = pair.Value;
                current.Key = pair.Key;
                yield return current;
            }
        }
    }

    public struct ModifiableKeyValuePair<TKey, TValue>
    {
        public TKey Key;
        public TValue Value;

        public ModifiableKeyValuePair(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public ModifiableKeyValuePair(KeyValuePair<TKey, TValue> toClone)
        {
            Key = toClone.Key;
            Value = toClone.Value;
        }

        public KeyValuePair<TKey, TValue> GetKeyValuePair()
        {
            return new KeyValuePair<TKey, TValue>(Key, Value);
        }
    }

    public struct DebugLine
    {
        public Line3Segment segment;
        public Color color;

        public DebugLine(Line3Segment segment, Color color)
        {
            this.segment = segment;
            this.color = color;
        }

        public DebugLine(Vector3 start, Vector3 end, Color color)
        {
            segment = new Line3Segment(start, end);
            this.color = color;
        }
    }
}