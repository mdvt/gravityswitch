﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyLibraries
{
    public abstract partial class ColorizedPathBase
    {
        public class ColorizedPoint
        {
            public Vector2Int pos;
            public Color color;

            public Transform prevFragment;
            public Vector3 prevFragmentLocalPos;
            public Quaternion prevFragmentLocalRot;
            public int prevFragmentHashCode;

            public ColorizedPoint(Vector2Int pos, Color color)
            {
                this.pos = pos;
                this.color = color;
            }
        }

        protected class ColorizedInterval
        {
            public ColorizedPoint start;
            public ColorizedPoint end;

            public ColorizedInterval()
            { }

            public ColorizedInterval(ColorizedPoint start, ColorizedPoint end)
            {
                this.start = start;
                this.end = end;
            }

            public void Set(ColorizedPoint start, ColorizedPoint end)
            {
                this.start = start;
                this.end = end;
            }

            public Vector2Int dir
            {
                get { return end.pos - start.pos; }
            }

            public Transform fragment
            {
                get { return end.prevFragment; }
                set { end.prevFragment = value; }
            }

            public Vector3 fragmentLocalPos
            {
                get { return end.prevFragmentLocalPos; }
                set { end.prevFragmentLocalPos = value; }
            }

            public Quaternion fragmentLocalRot
            {
                get { return end.prevFragmentLocalRot; }
                set { end.prevFragmentLocalRot = value; }
            }

            public int fragmentHashCode
            {
                get { return end.prevFragmentHashCode; }
                set { end.prevFragmentHashCode = value; }
            }
        }
    }

    public abstract partial class ColorizedPathBase
    {
        public readonly bool Closed;
        public Vector3 Shift;
        public Transform Handler;

        protected readonly LinkedList<ColorizedPoint> ColorizedPoints;

        /*optimization*/
        protected ColorizedInterval prevInterval;
        protected ColorizedInterval interval;
        protected ColorizedInterval nextInterval;
        /*optimization*/

        public ColorizedPathBase(GameObject handler, Vector3 shift =  default, bool closed = false)
        {
            prevInterval = new ColorizedInterval();
            interval = new ColorizedInterval();
            nextInterval = new ColorizedInterval();

            ColorizedPoints = new LinkedList<ColorizedPoint>();

            Shift = shift;
            Closed = closed;
            Handler = handler.transform;
        }

        public void AddFirstPoint(Vector2Int point, Color color)
        {
            if (!IsFirstPointValid(point))
                throw new ArgumentException("Point not valid");

            ColorizedPoints.AddFirst(new ColorizedPoint(point, color));
        }

        public void AddLastPoint(Vector2Int point, Color color)
        {
            if (!IsLastPointValid(point))
                throw new Exception("Point not valid");

            ColorizedPoints.AddLast(new ColorizedPoint(point, color));
        }

        public void SyncOutlinesWithHandler()
        {
            foreach (ColorizedPoint point in ColorizedPoints)
            {
                if (point.prevFragment != null)
                {
                    point.prevFragment.localPosition = Handler.position + Handler.rotation * (Shift + point.prevFragmentLocalPos);
                    point.prevFragment.localRotation = Quaternion.AngleAxis(Handler.eulerAngles.z + point.prevFragmentLocalRot.eulerAngles.z, Vector3.forward);
                }
            }
        }

        public void Redraw()
        {
            var routine = RedrawEnumerator();
            while (routine.MoveNext()) ;
        }

        public IEnumerator RedrawEnumerator()
        {
            var routine = BaseRedrawEnumerator();
            while (routine.MoveNext()) yield return null;
        }

        protected virtual IEnumerator BaseRedrawEnumerator(Func<ColorizedInterval, bool> predicate = null)
        {
            if (predicate == null)
                predicate = x => true;

            if (Closed && !IsClosedStateValid())
                throw new Exception("Closed path not valid");

            for (LinkedListNode<ColorizedPoint> node = ColorizedPoints.First.Next; node != null; node = node.Next)
            {
                interval.Set(node.Previous.Value, node.Value);

                if (!predicate(interval))
                    continue;

                bool isPrevIntervalNull = false;
                if (node.Previous.Previous != null)
                    prevInterval.Set(node.Previous.Previous.Value, node.Previous.Value);
                else if (Closed)
                    prevInterval.Set(ColorizedPoints.Last.Value, node.Previous.Value);
                else
                    isPrevIntervalNull = true;

                bool isNextIntervalNull = false;
                if (node.Next != null)
                    nextInterval.Set(node.Value, node.Next.Value);
                else if (Closed)
                    nextInterval.Set(node.Value, ColorizedPoints.First.Value);
                else
                    isNextIntervalNull = true;

                GenerateOutlineByIntervals(interval, isPrevIntervalNull ? null : prevInterval, isNextIntervalNull ? null : nextInterval);

                yield return null;
            }

            if (Closed)
            {
                interval.Set(ColorizedPoints.Last.Value, ColorizedPoints.First.Value);

                if (predicate(interval))
                {
                    prevInterval.Set(ColorizedPoints.Last.Previous.Value, ColorizedPoints.Last.Value);
                    nextInterval.Set(ColorizedPoints.First.Value, ColorizedPoints.First.Next.Value);

                    GenerateOutlineByIntervals(interval, prevInterval, nextInterval);
                }
            }
        }

        public void Clear()
        {
            var routine = ClearEnumerator();
            while (routine.MoveNext()) ;
        }

        protected Vector2 GetGlobalPointPos(Vector2 pos)
        {
            return Handler.position + Handler.rotation * ((Vector2)Shift + pos);
        }

        public abstract IEnumerator ClearEnumerator();

        protected abstract void GenerateOutlineByIntervals(ColorizedInterval interval, ColorizedInterval prevInterval, ColorizedInterval nextInterval);

        protected abstract bool IsClosedStateValid();

        protected abstract bool IsFirstPointValid(Vector2Int point);

        protected abstract bool IsLastPointValid(Vector2Int point);
    }
}