﻿using UnityEngine;

namespace MyLibraries
{
    public enum ColorizedPathDrawDir
    {
        Left = -1,
        Right = 1
    }

    public abstract class ColorizedPathOptimized90DegreeBase : ColorizedPathOptimizedBase
    {
        public readonly float Width;
        public readonly ColorizedPathDrawDir DrawDir;

        public ColorizedPathOptimized90DegreeBase(GameObject handler,
                                                  ColorizedPathDrawDir drawDir,
                                                  float width,
                                                  ColorizedPathOptimizedPrefabsHandler prefabsHandler,
                                                  Vector3 shift = default,
                                                  bool closed = false) :
            base(handler, prefabsHandler, shift, closed)
        {
            DrawDir = drawDir;
            Width = width;
        }

        protected override void GetFragmentLocalTransform(ColorizedInterval interval, ColorizedInterval prevInterval, ColorizedInterval nextInterval, out Vector3 localPos, out Quaternion localRot)
        {
            // calculating helper variables
            float startShiftByX = 0; // shift setup
            float endShiftByX = 0;
            if (prevInterval != null && (DrawDir == ColorizedPathDrawDir.Left && prevInterval.dir.Left() == interval.dir ||
                DrawDir == ColorizedPathDrawDir.Right && prevInterval.dir.Right() == interval.dir))
                startShiftByX = Width;
            if (nextInterval != null && (DrawDir == ColorizedPathDrawDir.Left && interval.dir.Left() == nextInterval.dir ||
                DrawDir == ColorizedPathDrawDir.Right && interval.dir.Right() == nextInterval.dir))
                endShiftByX = Width;
            // calculating helper variables

            Vector2 start = interval.start.pos - (Vector2)interval.dir * startShiftByX; // setup position
            Vector2 end = interval.end.pos + (Vector2)interval.dir * endShiftByX;
            Vector2 middle = (start + end) / 2;
            localPos = middle + (Vector2)interval.dir.Left() * (int)DrawDir * (Width / 2);

            localRot = Quaternion.AngleAxis(Vector2.SignedAngle(Vector2.right, interval.dir), Vector3.forward);
        }

        protected override bool IsFirstPointValid(Vector2Int point)
        {
            if (ColorizedPoints.Count == 0)
                return true;

            Vector2Int dir = point - ColorizedPoints.First.Value.pos;
            if (Mathf.Abs(dir.x) == Mathf.Abs(dir.y) ||
                (dir.x != -1 && dir.x != 0 && dir.x != 1) ||
                (dir.y != -1 && dir.y != 0 && dir.y != 1))
                return false;

            if (ColorizedPoints.Count == 1)
                return true;

            Vector2Int firstPointsDir = ColorizedPoints.First.Value.pos - ColorizedPoints.First.Next.Value.pos;

            return (firstPointsDir.x == 0 || dir.x != -firstPointsDir.x) &&
                   (firstPointsDir.y == 0 || dir.y != -firstPointsDir.y);
        }

        protected override bool IsLastPointValid(Vector2Int point)
        {
            if (ColorizedPoints.Count == 0)
                return true;

            Vector2Int dir = point - ColorizedPoints.Last.Value.pos;
            if (Mathf.Abs(dir.x) == Mathf.Abs(dir.y) ||
                (dir.x != -1 && dir.x != 0 && dir.x != 1) ||
                (dir.y != -1 && dir.y != 0 && dir.y != 1))
                return false;

            if (ColorizedPoints.Count == 1)
                return true;

            Vector2Int lastPointsDiff = ColorizedPoints.Last.Value.pos - ColorizedPoints.Last.Previous.Value.pos;

            return (lastPointsDiff.x == 0 || dir.x != -lastPointsDiff.x) &&
                   (lastPointsDiff.y == 0 || dir.y != -lastPointsDiff.y);
        }

        protected override bool IsClosedStateValid()
        {
            Vector2Int dir = ColorizedPoints.First.Value.pos - ColorizedPoints.Last.Value.pos;

            if (Mathf.Abs(dir.x) == Mathf.Abs(dir.y) ||
                (dir.x != -1 && dir.x != 0 && dir.x != 1) ||
                (dir.y != -1 && dir.y != 0 && dir.y != 1))
                return false;

            return true;
        }
    }
}
