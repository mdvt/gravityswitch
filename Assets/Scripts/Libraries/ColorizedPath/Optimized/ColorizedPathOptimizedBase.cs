﻿using UnityEngine;
using System.Collections;

namespace MyLibraries
{
    public abstract class ColorizedPathOptimizedBase : ColorizedPathBase
    {
        private ColorizedPathOptimizedPrefabsHandler PrefabsHandler;

        public ColorizedPathOptimizedBase(GameObject handler, ColorizedPathOptimizedPrefabsHandler prefabsHandler, Vector3 shift = default, bool closed = false) :
            base(handler, shift, closed)
        {
            PrefabsHandler = prefabsHandler;
        }

        public override IEnumerator ClearEnumerator()
        {
            foreach (ColorizedPoint point in ColorizedPoints)
            {
                if (point.prevFragment != null)
                    PrefabsHandler.RemoveOutlineFromScene(point.prevFragment.gameObject);
                yield return null;
            }

            ColorizedPoints.Clear();
        }

        protected override void GenerateOutlineByIntervals(ColorizedInterval interval, ColorizedInterval prevInterval, ColorizedInterval nextInterval)
        {
            int intervalHashCode = GetIntervalHashCode(interval, prevInterval, nextInterval);

            if (interval.fragment == null || interval.fragmentHashCode != intervalHashCode)
            {
                if (interval.fragment != null)
                    PrefabsHandler.RemoveOutlineFromScene(interval.fragment.gameObject);

                // founding outline
                var transform = PrefabsHandler.AddOutlineToScene(intervalHashCode).transform;
                // founding outline

                // setup fragment transform
                GetFragmentLocalTransform(interval, prevInterval, nextInterval, out Vector3 localPos, out Quaternion localRot);

                transform.localPosition = Handler.position + Handler.rotation * (Shift + localPos);
                transform.localRotation = Quaternion.AngleAxis(Handler.eulerAngles.z + localRot.eulerAngles.z, Vector3.forward);
                // setup fragment transform

                // setup interval
                interval.fragment = transform;
                interval.fragmentLocalPos = localPos;
                interval.fragmentLocalRot = localRot;
                interval.fragmentHashCode = intervalHashCode;
                // setup interval
            }
            else
            {
                interval.fragment.localPosition = Handler.position + Handler.rotation * (Shift + interval.fragmentLocalPos);
                interval.fragment.localRotation = Quaternion.AngleAxis(Handler.eulerAngles.z + interval.fragmentLocalRot.eulerAngles.z, Vector3.forward);
            }
        }

        protected abstract int GetIntervalHashCode(ColorizedInterval interval, ColorizedInterval prevInterval, ColorizedInterval nextInterval);

        protected abstract void GetFragmentLocalTransform(ColorizedInterval interval, ColorizedInterval prevInterval, ColorizedInterval nextInterval, out Vector3 localPos, out Quaternion localRot);
    }
}
