﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyLibraries
{
    public abstract class ColorizedPathOptimizedPrefabsHandler
    {
        private GameObjectStorage Storage;

        private Dictionary<int, GameObject> Anchors;
        private Transform AnchorsHadler;

        public ColorizedPathOptimizedPrefabsHandler(GameObjectStorage storage)
        {
            AnchorsHadler = new GameObject("OutlineAnchorsHandler").transform;
            AnchorsHadler.gameObject.SetActive(false);

            Anchors = new Dictionary<int, GameObject>();

            Storage = storage;
        }

        public GameObject AddOutlineToScene(int intervalHashCode)
        {
            if (Anchors.TryGetValue(intervalHashCode, out GameObject anchor))
                return Storage.AddToScene(anchor);
            else
                throw new Exception("anchor doesn't exist");
        }

        public void RemoveOutlineFromScene(GameObject outline)
        {
             Storage.RemoveFromScene(outline);
        }

        public void GenerateStorageAnchors()
        {
            foreach (ModifiableKeyValuePair<int, GameObject> pair in GenerateAnchors())
            {
                pair.Value.transform.parent = AnchorsHadler;
                Anchors.Add(pair.Key, pair.Value);
            }
        }

        protected abstract IEnumerable GenerateAnchors();
    }
}
