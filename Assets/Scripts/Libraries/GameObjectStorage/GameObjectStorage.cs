﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace MyLibraries
{
    public class GameObjectStorage : MonoBehaviour
    {
        private Dictionary<int, Group> GroupsOfObjects;

        public Action<GameObject> OnCreate;
        public Action<GameObject> OnDestroy;

        public void Start()
        {
            transform.position = Vector3.zero;
            GroupsOfObjects = new Dictionary<int, Group>();
        }

        public void RemoveFromScene(GameObject obj)
        {
            var storableObj = obj.GetComponent<StorableObject>();
            if (storableObj == null)
                throw new Exception("Object must have storage container");

            OnDestroy?.Invoke(obj);

            if (!GroupsOfObjects.TryGetValue(storableObj.GroupId, out Group group))
                group = CreateGroup(storableObj.GroupId);

            group.AddLast(obj);

            obj.transform.localPosition = Vector3.down * 10000;
        }

        public GameObject AddToScene(GameObject anchor, Vector3 position, Quaternion rotation, Transform parent, bool setInLocalSpace)
        {
            GameObject result = GetGOByAnchor(anchor);

            result.transform.parent = parent;
            if (setInLocalSpace)
            {
                result.transform.localPosition = position;
                result.transform.localRotation = rotation;
            }
            else
            {
                result.transform.position = position;
                result.transform.rotation = rotation;
            }

            OnCreate?.Invoke(result);

            return result;
        }

        public GameObject AddToScene(GameObject anchor, Vector3 position, Quaternion rotation)
        {
            GameObject result = GetGOByAnchor(anchor);

            result.transform.position = position;
            result.transform.rotation = rotation;

            OnCreate?.Invoke(result);

            return result;
        }

        public GameObject AddToScene(GameObject anchor)
        {
            GameObject result = GetGOByAnchor(anchor);

            OnCreate?.Invoke(result);

            return result;
        }

        private GameObject GetGOByAnchor(GameObject anchor)
        {
            GameObject result;

            int id = anchor.GetInstanceID();
            if (GroupsOfObjects.TryGetValue(id, out Group group))
            {
                result = group.Last.Value;

                group.RemoveLast();

                if (group.Count == 0)
                    RemoveGroup(id);
            }
            else
            {
                result = Instantiate(anchor);
                result.transform.parent = transform;

                var storableObj = result.GetComponent<StorableObject>();
                if (storableObj == null)
                    throw new Exception("Object must have storage container");

                storableObj.GroupId = anchor.GetInstanceID();
            }

            return result;
        }

        private Group CreateGroup(int id)
        {
            Group group = new Group();
            GroupsOfObjects.Add(id, group);

            return group;
        }

        private void RemoveGroup(int id)
        {
            if (!GroupsOfObjects.TryGetValue(id, out Group group))
                throw new Exception("deleting key doesn't exist");

            GroupsOfObjects.Remove(id);
        }

        private class Group : LinkedList<GameObject>
        {}
    }
}
