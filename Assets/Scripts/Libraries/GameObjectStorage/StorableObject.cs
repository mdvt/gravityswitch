﻿using System;
using UnityEngine;

namespace MyLibraries
{
    public class StorableObject : MonoBehaviour
    {
        [NonSerialized] public int GroupId;
    }
}
