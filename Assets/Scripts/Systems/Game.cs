﻿//#define FRAME_LOG
//#define FREEZE_DETECT
//#define LOAD_DEBUG_CANVAS

using UnityEngine;
using System.Collections;
using System;

using MyLibraries;
using MyGUI;

public class Game : MonoBehaviour
{
    [NonSerialized] public float DeltaTime = 1 / 240f;
    [NonSerialized] public TouchInput input;
    [NonSerialized] public Player player;
    [NonSerialized] public CameraController cam;
    [NonSerialized] public Map map;
    public Storages storages;
    public Prefabs prefabs;
    public ColorPallete InteractiveObjectsColors;
    public Materials materials;
    public Canvases canvases;

    public Action DebugDraw;

    private bool needToSwitchGravity;
    private bool paused;

    // Start is called before the first frame update
    void Start()
    {
        paused = true;
        needToSwitchGravity = false;
        GlobalSpace.game = this;
        Time.fixedDeltaTime = DeltaTime;

        prefabs.Init();
        canvases.Init();

        map = GetComponent<Map>();
        input = GetComponent<TouchInput>();
        player = FindObjectOfType<Player>();
        cam = FindObjectOfType<CameraController>();

        map.start();
        player.start(map);
        cam.start(player.transform.position);
    }

#if FRAME_LOG || FREEZE_DETECT
    float mapTime, playerTime, camTime, minTime = float.MaxValue, maxTime = 0, timeSum;
    int frames = 0;
#endif
    private void FixedUpdate()
    {
        if (paused || map.RequireExtraTimeForLoading(cam))
            return;

        float dt = Time.fixedDeltaTime;
#if FRAME_LOG || FREEZE_DETECT
        var startTime = Time.realtimeSinceStartup;
        mapTime = Time.realtimeSinceStartup;
#endif
        map.update(dt, player, cam);
#if FRAME_LOG || FREEZE_DETECT
        mapTime = (Time.realtimeSinceStartup - mapTime) * 1000000;
#endif

#if FRAME_LOG || FREEZE_DETECT
        playerTime = Time.realtimeSinceStartup;
#endif
        player.update(map, ref needToSwitchGravity, dt);
#if FRAME_LOG || FREEZE_DETECT
        playerTime = (Time.realtimeSinceStartup - playerTime) * 1000000;


        var time = (Time.realtimeSinceStartup - startTime) * 1000000;
        timeSum += time;
        minTime = time < minTime ? time : minTime;
        maxTime = time > maxTime ? time : maxTime;
#endif

#if !FRAME_LOG && FREEZE_DETECT
        if (time > 3000)
        {
#endif

#if FRAME_LOG || FREEZE_DETECT
            frames++;
            string result = "frame: " + frames +
                        (input.TouchedOnFrame ? "  Touched!" : "") +
                        "  time: " + time +
                        "  min: " + minTime +
                        "  max: " + maxTime +
                        "  average: " + (timeSum / frames) +
                        "  (map: " + mapTime +
                            "  player: " + playerTime + ")";
#endif

#if FRAME_LOG && FREEZE_DETECT
        if (time > 3000)
        {
#endif
#if FREEZE_DETECT
            Debug.LogError("Freeze detected!   " + result);
        }
#endif
#if FRAME_LOG
#if FREEZE_DETECT
        else
#endif
            Debug.Log(result);
#endif
    }

    private void Update()
    {
        DebugDraw?.Invoke();

        if (map.RequireExtraTimeForLoading(cam))
            return;

        input.update();
        if (input.TouchedOnFrame)
        {
            if (paused)
            {
                if (player.dead)
                    Reset();
                else
                    SetPause(false);
            }
            else
                needToSwitchGravity = !needToSwitchGravity;
        }

        if (!paused)
        {
            cam.update(player.transform.position, Time.deltaTime);
            map.beforeRender(player, cam);
        }
    }

    public void Reset()
    {
        needToSwitchGravity = false;
        map.reset();
        player.reset(map);
        cam.reset(player.transform.position);
    }

    public void SetPause(bool state)
    {
        paused = state;
    }
}

[Serializable]
public class Storages
{
    public GameObjectStorage AreaObjectsStorage;
    public GameObjectStorage OutlinesStorage;
}

[Serializable]
public class Canvases
{
    public MenuCanvasManager menu;
    public LevelCanvasManager level;
    public DebugCanvasManager debug;

    public void Init()
    {
        menu.Init();
        level.Init();
#if LOAD_DEBUG_CANVAS
        debug.Init();
#endif
    }
}

[Serializable]
public class Prefabs
{
    public GameObject outline;
    public GameObject pillar;
    [NonSerialized] public float pillarHeight;
    public GameObject thorn;
    public GameObject plane;
    public GameObject planeWithTwoSideThorns;
    public GameObject rotatingPlane;

    public void Init()
    {
        pillarHeight = pillar.transform.localScale.y;
    }
}

[Serializable]
public class Materials
{
}

[Serializable]
public class ColorPallete
{
    public Color safe;
    public Color danger;
}