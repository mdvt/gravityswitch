﻿using UnityEngine;

namespace RailsSystem
{
    [DisallowMultipleComponent]
    public class RailsHandler : AreaComponent
    {
        public Vector2[] rails;

        protected override void afterCreate()
        {
            foreach (Vector2 railPos in rails)
                GlobalSpace.game.map.railMap.AddRail(((Vector2)transform.position + railPos).ToVector2Int());
        }
    }
}
