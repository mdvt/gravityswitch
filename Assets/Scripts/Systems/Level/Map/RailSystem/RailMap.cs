﻿using System.Collections.Generic;
using UnityEngine;
using MyLibraries;

namespace RailsSystem
{
    public class RailMap : List<LinkedList<int>>
    {
        public bool this[int x, int y]
        {
            get
            {
                if (!MyMath.InRange(x, 0, Count - 1))
                    throw new System.Exception("Not valid rail pos by x");

                foreach (int yPos in this[x])
                {
                    if (yPos == y)
                        return true;
                }

                return false;
            }
        }

        public void AddRail(Vector2Int pos)
        {
            if (!MyMath.InRange(pos.x, 0, Count - 1))
                throw new System.Exception("rail must be added to existing layer");

            if (this[pos.x].Contains(pos.y))
                throw new System.Exception("added rail already exist");

            this[pos.x].AddLast(pos.y);
        }

        public bool TryRemoveRail(Vector2Int pos)
        {
            if (!MyMath.InRange(pos.x, 0, Count - 1))
                return false;

            if (!this[pos.x].Remove(pos.y))
                return false;

            return true;
        }

        public void AddRailsLayers(int numOfLayers)
        {
            LinkedList<LinkedList<int>> layersToAdd = new LinkedList<LinkedList<int>>();
            for (int i = 0; i < numOfLayers; i++)
                layersToAdd.AddLast(new LinkedList<int>());
            AddRange(layersToAdd);
        }

        public void RemoveRailsLayers(int numOfLayers)
        {
            RemoveRange(0, numOfLayers);
        }

        public void Reset()
        {
            Clear();
        }

        public void DebugDraw()
        {
            for (int x = 0; x < Count; x++)
                foreach (int y in this[x])
                    Debug.DrawLine(new Vector3(x, y, -5), new Vector3(x + 1, y, -5), Color.green);
        }
    }
}