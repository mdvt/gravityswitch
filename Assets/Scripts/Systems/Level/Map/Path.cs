﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Path.Patterns;
using MyLibraries;

namespace Path
{
    public struct PathPoint
    {
        public Vector2Int pos;
        public int height;
        public BasePattern generatingPattern;
        public bool generateTop;
        public bool generateBottom;

        public static PathPoint zero { get { return new PathPoint(Vector2Int.zero, 0, null); } }

        public PathPoint(Vector2Int pos, int height, BasePattern generatingPattern, bool generateTop = true, bool generateBottom = true)
        {
            this.pos = pos;
            this.height = height;
            this.generatingPattern = generatingPattern;
            this.generateTop = generateTop;
            this.generateBottom = generateBottom;
        }
    }

    public struct PathPointF
    {
        public Vector2 pos;
        public float height;
        public bool generateTop;
        public bool generateBottom;

        public static PathPointF zero = new PathPointF(PathPoint.zero);

        public PathPointF(Vector2 pos, float height, bool generateTop = true, bool generateBottom = true)
        {
            this.pos = pos;
            this.height = height;
            this.generateTop = generateTop;
            this.generateBottom = generateBottom;
        }

        public PathPointF(PathPoint other)
        {
            pos = other.pos;
            height = other.height;
            generateTop = other.generateTop;
            generateBottom = other.generateBottom;
        }

        public PathPoint ToPathPoint()
        {
            return new PathPoint(new Vector2Int((int)Mathf.Round(pos.x), (int)Mathf.Round(pos.y)), (int)Mathf.Round(pos.y + height) - (int)Mathf.Round(pos.y), 
                             null, generateTop, generateBottom);
        }
    }

    public class LocalPath
    {
        private GlobalPath Path;

        public int startInd;
        public int length;

        public LocalPath(int startInd, int length)
        {
            Path = GlobalSpace.game.map.globalPath;

            this.startInd = startInd;
            this.length = length;
        }

        public PathPoint this[int i] { get { return Path.Points[startInd + i]; } }

        public PathPoint GetPoint(int posByX)
        {
            if (!MyMath.InRange(posByX, 0, Path.Points[startInd + length - 1].pos.x - Path.Points[startInd].pos.x))
                throw new Exception("pos out of local path range");

            PathPoint result = Path.GetPoint(posByX + Path.Points[startInd].pos.x);

            result.pos -= Path.Points[startInd].pos;

            return result;
        }
    }

    public class GlobalPath
    {
        public List<PathPoint> Points { get; private set; }
        private List<LocalPath> LocalPaths;

        public GlobalPath()
        {
            Points = new List<PathPoint>();
            LocalPaths = new List<LocalPath>();
        }

        public void Reset()
        {
            Points.Clear();
            LocalPaths.Clear();
        }

        public LocalPath AddLocalPath(List<PathPoint> pointsToAdd)
        {
            LocalPath result;
            if (Points.Count > 0)
                result = new LocalPath(Points.Count - 1, pointsToAdd.Count + 1);
            else
                result = new LocalPath(Points.Count, pointsToAdd.Count);
            LocalPaths.Add(result);

            int prevPosByX = 0;
            if (Points.Count > 0)
                for (int i = 0; i < pointsToAdd.Count; i++)
                {
                    if (prevPosByX >= pointsToAdd[i].pos.x)
                        throw new Exception("Point not valid");
                    else
                        prevPosByX = pointsToAdd[i].pos.x;

                    PathPoint temp = pointsToAdd[i];
                    temp.pos += Points[Points.Count - 1].pos;
                    pointsToAdd[i] = temp;
                }

            Points.AddRange(pointsToAdd);

            return result;
        }

        public LocalPath GetLastLocalPath()
        {
            return LocalPaths.Last();
        }

        public void RemoveFirstLocalPath()
        {
            Points.RemoveRange(0, LocalPaths[0].length - 1);
            LocalPaths.RemoveAt(0);
        }

        public void MoveToZeroByX()
        {
            int shift = Points[0].pos.x;
            for (int i = 0; i < Points.Count; i++)
            {
                PathPoint temp = Points[i];
                temp.pos += Vector2Int.left * shift;
                Points[i] = temp;
            }

            shift = LocalPaths[0].startInd;
            for (int i = 0; i < LocalPaths.Count; i++)
                LocalPaths[i].startInd -= shift;
        }

        public PathPoint GetPoint(int posByX)
        {
            return GetPointF(posByX).ToPathPoint();
        }

        public PathPointF GetPointF(float posByX)
        {
            if (!MyMath.InRange(posByX, Points[0].pos.x, Points[Points.Count - 1].pos.x))
                throw new Exception("pos out of path range");

            int startPointIndex;
            for (startPointIndex = 0; startPointIndex < Points.Count - 1 && Points[startPointIndex].pos.x <= posByX; startPointIndex++) ;
            startPointIndex--;

            PathPointF result = new PathPointF();

            result.pos.y = Mathf.Lerp(Points[startPointIndex].pos.y, Points[startPointIndex + 1].pos.y,
                                      (posByX - Points[startPointIndex].pos.x) / (Points[startPointIndex + 1].pos.x - Points[startPointIndex].pos.x));
            result.pos.x = posByX;

            result.height = Mathf.Lerp(Points[startPointIndex].height, Points[startPointIndex + 1].height,
                                      (posByX - Points[startPointIndex].pos.x) / (Points[startPointIndex + 1].pos.x - Points[startPointIndex].pos.x));

            result.generateTop = Points[startPointIndex + 1].generateTop;
            result.generateBottom = Points[startPointIndex + 1].generateBottom;
            return result;
        }

        public void DebugDraw()
        {
            PathPoint prev = PathPoint.zero;
            bool firstIter = true;
            foreach (PathPoint p in Points)
            {
                if (firstIter)
                    firstIter = false;
                else
                    Debug.DrawLine(new Vector3(prev.pos.x, prev.pos.y + (float)prev.height / 2, 0),
                                   new Vector3(p.pos.x, p.pos.y + (float)p.height / 2, 0), Color.blue);

                Debug.DrawLine(new Vector3(p.pos.x, p.pos.y + p.height, 0),
                               new Vector3(p.pos.x, p.pos.y, 0), Color.blue);

                prev = p;
            }
        }
    }
}