﻿//#define DontTeleporting
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

using MyLibraries;

using CollidersOptimizatingSystem;
using RailsSystem;
using Outlines;
using Path;

public class Map : MonoBehaviour
{
    [NonSerialized] public LinkedList<AreaHandler> ActiveAreas;
    [NonSerialized] public CoroutineProcess<AreaGenerator.GenerateInfo> AreaGeneratorRoutine;
    [NonSerialized] public ReadOnlyCollection<AreaGenerator> AreaGenerators;
    [NonSerialized] public ReadOnlyCollection<Path.Patterns.BasePattern> PathPatterns;

    [NonSerialized] public RailMap railMap;
    [NonSerialized] public GlobalPath globalPath;
    [NonSerialized] public CollidersManager collidersManager;
    [NonSerialized] public AreaComponentsManager areaObjectsManager;
    [NonSerialized] public OutlinePathPrefabsHandler outlinePathPrefabsHandler;


    public int Length { get; private set; }

    [Range(0, int.MaxValue - 64)] public int MinLoadedDistance; // int.maxValue doesn't work. See https://issuetracker.unity3d.com/issues/range-1-int-dot-maxvalue-produces-negative-result

    [Range(0, int.MaxValue - 64)] public int MaxDifficultyDistance;

    [Range(0, float.MaxValue)] public float OutlineWidth;

    public void start()
    {
        Length = 0;

        AreaGeneratorsSetup generatorsSetupInfo = GetComponent<AreaGeneratorsSetup>();
        List<AreaGenerator> areaGenerators = UnityOverrides.CreateInstancesOfInheritors<AreaGenerator>();
        for (int i = 0; i < areaGenerators.Count; i++)
        {
            AreaGeneratorsSetup.GeneratorInfo info = generatorsSetupInfo.Generators.Find(x => x.Title == areaGenerators[i].GetType().ToString());

            if (info == null || !info.Enabled)
            {
                areaGenerators.RemoveAt(i);
                i--;
                continue;
            }

            areaGenerators[i].StartDifficulty = info.StartDifficulty;
            areaGenerators[i].EndDifficulty = info.EndDifficulty;
        }
        AreaGenerators = areaGenerators.AsReadOnly();

        PathPatterns = UnityOverrides.CreateInstancesOfInheritors<Path.Patterns.BasePattern>().AsReadOnly();

        ActiveAreas = new LinkedList<AreaHandler>();
        AreaGeneratorRoutine = new CoroutineProcess<AreaGenerator.GenerateInfo>(this, null, null);

        railMap = new RailMap();
        globalPath = new GlobalPath();
        collidersManager = new CollidersManager();
        areaObjectsManager = new AreaComponentsManager(GlobalSpace.game.storages.AreaObjectsStorage);
        outlinePathPrefabsHandler = new OutlinePathPrefabsHandler(GlobalSpace.game.prefabs.outline,
                                                                  GlobalSpace.game.storages.OutlinesStorage,
                                                                  OutlineWidth,
                                                                  GlobalSpace.game.InteractiveObjectsColors.safe,
                                                                  GlobalSpace.game.InteractiveObjectsColors.danger);
        outlinePathPrefabsHandler.GenerateStorageAnchors();

        GenerateArea(new StartArea());

        //GlobalSpace.game.DebugDraw += railMap.DebugDraw;
        //GlobalSpace.game.DebugDraw += globalPath.DebugDraw;
        GlobalSpace.game.DebugDraw += AreasDebug;
    }

    public void update(float dt, Player player, CameraController cam)
    {
        /*Load new area if all areas are visible*/
        if (ActiveAreas.Last.Value.transform.localPosition.x < cam.RightCameraBorderByX + MinLoadedDistance &&
            !AreaGeneratorRoutine.IsProcessing)
        {
            var generator = GetRandomAreaGenerator(Length);
            var difficulty = GetLinearDifficulty(Length);
            Action newAreaGenerateStart = delegate { AsyncGenerateArea(generator); };
            AsyncGenerateArea(new JointArea(), generator.PossibleStartHeights(difficulty), newAreaGenerateStart);
        }
        /*Load new area if all areas are visible*/

        /*Destroy invisible area*/
        if (ActiveAreas.First.Value.transform.localPosition.x + ActiveAreas.First.Value.Length < cam.LeftCameraBorderByX)
        {
            AreaHandler firstArea = ActiveAreas.First.Value;

            int shift = -ActiveAreas.First.Value.Length;
            ActiveAreas.RemoveFirst();
            firstArea.OnRemove();

#if !DontTeleporting
            Teleport(shift);
#endif
        }
        /*Destroy invisible area*/

        float playerPosByX = player.transform.position.x + player.ShiftByX(dt);
        areaObjectsManager.UpdateAreaObjects(playerPosByX,
                                             playerPosByX - 0.5f,
                                             playerPosByX + 0.5f);
        collidersManager.update(playerPosByX);
    }

    public void beforeRender(Player player, CameraController cam)
    {
        areaObjectsManager.UpdateAreaObjects(player.transform.localPosition.x,
                                             cam.LeftCameraBorderByX,
                                             cam.RightCameraBorderByX);
    }

    public bool RequireExtraTimeForLoading(CameraController cam)
    {
        if (!AreaGeneratorRoutine.IsProcessing)
            return false;

        return ActiveAreas.Last.Value.transform.localPosition.x - 1 < cam.RightCameraBorderByX;
    }

    public void reset()
    {
        AreaGeneratorRoutine.Stop();
        foreach (AreaHandler area in ActiveAreas)
        {
            area.RemoveAllObjects();
            Destroy(area.gameObject);
        }

        ActiveAreas.Clear();
        globalPath.Reset();
        collidersManager.Reset();
        railMap.Reset();
        areaObjectsManager.Reset();

        Length = 0;
        GenerateArea(new StartArea());
    }

    public float GetLinearDifficulty(int distance)
    {
        return Mathf.Clamp01((float)distance / MaxDifficultyDistance);
    }

    private void AsyncGenerateArea(AreaGenerator generator, object additionalGenerateInfo = null, Action onFinish = null)
    {
        if (AreaGeneratorRoutine.IsProcessing)
            throw new Exception("can't start new area generating process while another running");

        var info = SetupNewArea(generator, additionalGenerateInfo);

        AreaGeneratorRoutine.SetRoutine(generator.GenerateNewArea);
        AreaGeneratorRoutine.SetArg(info);
        AreaGeneratorRoutine.Finished = null;
        AreaGeneratorRoutine.Finished += delegate { Length += info.area.Length; onFinish?.Invoke(); };

        AreaGeneratorRoutine.Start();
    }

    private void GenerateArea(AreaGenerator generator, object additionalGenerateInfo = null)
    {
        var info = SetupNewArea(generator, additionalGenerateInfo);
        var generateFunc = generator.GenerateNewArea(info);
        while (generateFunc.MoveNext()) ;

        Length += info.area.Length;
    }

    private AreaGenerator.GenerateInfo SetupNewArea(AreaGenerator generator, object additionalGenerateInfo)
    {
        var lastArea = ActiveAreas.Last?.Value;

        AreaHandler area = InitNewArea(generator);
        ActiveAreas.AddLast(area);
        areaObjectsManager.SetArea(area);

        PathPoint lastPoint = (globalPath.Points.Count > 0) ? globalPath.Points[globalPath.Points.Count - 1] : PathPoint.zero;
        lastPoint.pos = Vector2Int.zero;

        return new AreaGenerator.GenerateInfo(area,
                                              GetLinearDifficulty(Length),
                                              lastPoint,
                                              lastArea,
                                              additionalGenerateInfo);
    }

    private AreaGenerator GetRandomAreaGenerator(int distance)
    {
        int numOfActiveAreas = 0;
        foreach (AreaGenerator gen in AreaGenerators)
        {
            if (MyMath.InRange(GetLinearDifficulty(distance), gen.StartDifficulty, gen.EndDifficulty))
                numOfActiveAreas++;
        }

        int pos = UnityEngine.Random.Range(0, numOfActiveAreas);
        int i = 0;
        foreach (AreaGenerator gen in AreaGenerators)
        {
            if (MyMath.InRange(GetLinearDifficulty(distance), gen.StartDifficulty, gen.EndDifficulty))
            {
                if (i == pos)
                    return gen;
                i++;
            }
        }

        throw new Exception("Active generators list is empty");
    }

    private AreaHandler InitNewArea(AreaGenerator gen)
    {
        var area = new GameObject("AreaHandler (" + Length + ")").AddComponent<AreaHandler>();
        area.Init();

        area.generator = gen;

        if (ActiveAreas.Count != 0)
            area.transform.localPosition = (Vector3)(Vector2)globalPath.Points.Last().pos + Vector3.back;
        else
            area.transform.localPosition = Vector3.zero + Vector3.back;

        return area;
    }

    private void Teleport(int move)
    {
        Vector3Int shift = new Vector3Int(move, 0, 0);
        foreach (AreaHandler area in ActiveAreas)
            area.Move(shift);

        areaObjectsManager.OnTeleport();
        collidersManager.Move(shift.x);
        globalPath.RemoveFirstLocalPath();
        globalPath.MoveToZeroByX();
        railMap.RemoveRailsLayers(-shift.x);

        GlobalSpace.game.player.transform.position += shift;
        GlobalSpace.game.cam.transform.position += shift;
        GlobalSpace.game.player.OnTeleport(shift);
    }

    private void AreasDebug()
    {
        foreach (AreaHandler area in ActiveAreas)
            area.DebugDraw();
    }
}
