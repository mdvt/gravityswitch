﻿//#define BORDERS_DEBUG

using System;
using System.Collections.Generic;

using UnityEngine;

namespace CollidersOptimizatingSystem
{
    [RequireComponent(typeof(PolygonCollider2D)), DisallowMultipleComponent]
    public class ObjectWithOptimizedCollider : AreaComponent
    {
        [SerializeField] private bool InitByOtherScript;
        [NonSerialized, Range(float.MinValue, 0)] public float LeftMaxBorder;
        [NonSerialized, Range(0, float.MaxValue)] public float RightMaxBorder;

        private LinkedList<PolygonCollider2D> ObjColliders;

        private bool _active;
        public void SetActive(bool value)
        {
            if (_active != value)
            {
                foreach (PolygonCollider2D collider in ObjColliders)
                    collider.enabled = value;
                _active = value;
            }
        }

        protected override void afterCreate()
        {
            if (!InitByOtherScript)
            {
                ObjColliders = new LinkedList<PolygonCollider2D>(GetComponentsInChildren<PolygonCollider2D>(true));

                _active = false;
                foreach (PolygonCollider2D c in ObjColliders)
                    c.enabled = _active;

                GenerateBorders(ObjColliders);

                GlobalSpace.game.map.collidersManager.AddOptimizedObject(this);

#if BORDERS_DEBUG
                Area.localDebugLines.AddLast(new MyLibraries.DebugLine(Vector3.up * transform.position.y + Vector3.right * LeftMaxBorder,
                                                                       Vector3.up * transform.position.y + Vector3.right * RightMaxBorder,
                                                                       Color.green));
#endif
            }
        }

        public void Init(float leftMaxBorder, float rightMaxBorder)
        {
            if (InitByOtherScript)
            {
                ObjColliders = new LinkedList<PolygonCollider2D>(GetComponentsInChildren<PolygonCollider2D>(true));

                _active = false;
                foreach (PolygonCollider2D c in ObjColliders)
                    c.enabled = _active;

                LeftMaxBorder = leftMaxBorder;
                RightMaxBorder = rightMaxBorder;
                GlobalSpace.game.map.collidersManager.AddOptimizedObject(this);

#if BORDERS_DEBUG
                Area.localDebugLines.AddLast(new MyLibraries.DebugLine(Vector3.up * transform.position.y + Vector3.right * LeftMaxBorder,
                                                                       Vector3.up * transform.position.y + Vector3.right * RightMaxBorder,
                                                                       Color.green));
#endif
            }
        }

        private void GenerateBorders(LinkedList<PolygonCollider2D> colliders)
        {
            LeftMaxBorder = float.MaxValue;
            RightMaxBorder = float.MinValue;

            foreach (PolygonCollider2D collider in colliders)
            {
                var transform = collider.transform;

                Vector2[] points = collider.points;

                for (int i = 0; i < points.Length; i++)
                {
                    float diffByX = transform.TransformPoint(points[i]).x;

                    if (diffByX < LeftMaxBorder) LeftMaxBorder = diffByX;
                    if (diffByX > RightMaxBorder) RightMaxBorder = diffByX;
                }
            }

            LeftMaxBorder -= AreaTransform.localPosition.x;
            RightMaxBorder -= AreaTransform.localPosition.x;
        }

        private void Reset()
        {
            InitByOtherScript = false;
        }
    }
}
