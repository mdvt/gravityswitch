﻿#define CORRECT_CHECK

using System.Collections.Generic;
using UnityEngine;

using MyLibraries;

namespace CollidersOptimizatingSystem
{
    public class CollidersArea
    {
        public int leftBorder;
        public int rightBorder;

        public bool wasActivated { get; private set; }
        public bool wasDeactivated { get; private set; }

        public LinkedList<ObjectWithOptimizedCollider> objectsToActivate;
        public LinkedList<ObjectWithOptimizedCollider> objectsToDeactivate;

        private bool _active;
        public bool active
        {
            get
            {
                return _active;
            }
            set
            {
                if (_active != value)
                {
                    if (value && !wasActivated)
                    {
                        wasActivated = true;
                        foreach (ObjectWithOptimizedCollider obj in objectsToActivate)
                            if (obj != null)
                                obj.SetActive(true);
                    }

                    if (!value && !wasDeactivated)
                    {
                        wasDeactivated = true;
                        foreach (ObjectWithOptimizedCollider obj in objectsToDeactivate)
                            if (obj != null)
                                obj.SetActive(false);
                    }

                    _active = value;
                }
            }
        }

        public CollidersArea()
        {
            objectsToActivate = new LinkedList<ObjectWithOptimizedCollider>();
            objectsToDeactivate = new LinkedList<ObjectWithOptimizedCollider>();

            _active = false;
            wasActivated = false;
            wasDeactivated = false;
        }

        public CollidersArea(int leftBorder, int rightBorder) : this()
        {
            this.leftBorder = leftBorder;
            this.rightBorder = rightBorder;
        }
    }

    public class CollidersManager
    {
        private LinkedList<CollidersArea> Areas;
        private int AreaLength;

        public CollidersManager(int AreaLength = 2)
        {
            this.AreaLength = AreaLength;

            Areas = new LinkedList<CollidersArea>();
            Areas.AddLast(new CollidersArea(0, AreaLength));
        }

        public void update(float playerPosByX)
        {
            foreach (CollidersArea area in Areas)
            {
                if (MyMath.InRange(playerPosByX, area.leftBorder - 0.5f, area.rightBorder + 0.5f))
                    area.active = true;
                else
                    area.active = false;
            }

            while (Areas.Count != 0 && playerPosByX > Areas.First.Value.rightBorder + 0.5f)
                Areas.RemoveFirst();
        }

        public void Reset()
        {
            Areas.Clear();
            Areas.AddLast(new CollidersArea(0, AreaLength));
        }

        public void AddOptimizedObject(ObjectWithOptimizedCollider obj)
        {
            float startPosByX = obj.AreaTransform.localPosition.x + obj.LeftMaxBorder;
            float endPosByX = obj.AreaTransform.localPosition.x + obj.RightMaxBorder;

            CollidersArea startArea = new CollidersArea();
            CollidersArea endArea = new CollidersArea();

            LinkedListNode<CollidersArea> currNode = Areas.Last;
            for (; currNode != null; currNode = currNode.Previous)
            {
                CollidersArea area = currNode.Value;

                if (endPosByX > area.rightBorder)
                {
                    endArea.leftBorder = area.rightBorder + (int)MyMath.Round(endPosByX - area.rightBorder, AreaLength, MyMath.Direction.Down);
                    endArea.rightBorder = endArea.leftBorder + AreaLength;

                    Areas.AddAfter(currNode, endArea);
                    currNode = currNode.Next;
                    break;
                }

                if (MyMath.InRange(endPosByX, area.leftBorder, area.rightBorder))
                {
                    endArea = area;
                    break;
                }
            }

            if (currNode == null)
            {
                CollidersArea area = Areas.First.Value;

                endArea.rightBorder = area.leftBorder - (int)MyMath.Round(area.leftBorder - endPosByX, AreaLength, MyMath.Direction.Down);
                endArea.leftBorder = endArea.rightBorder - AreaLength;

                Areas.AddFirst(endArea);
                currNode = Areas.First;
            }

            for (; currNode != null; currNode = currNode.Previous)
            {
                CollidersArea area = currNode.Value;

                if (startPosByX > area.rightBorder)
                {
                    startArea.leftBorder = area.rightBorder + (int)MyMath.Round(startPosByX - area.rightBorder, AreaLength, MyMath.Direction.Down);
                    startArea.rightBorder = startArea.leftBorder + AreaLength;

                    Areas.AddAfter(currNode, startArea);
                    break;
                }

                if (MyMath.InRange(startPosByX, area.leftBorder, area.rightBorder))
                {
                    startArea = area;
                    break;
                }
            }

            if (currNode == null)
            {
                CollidersArea area = Areas.First.Value;

                startArea.rightBorder = area.leftBorder - (int)MyMath.Round(area.leftBorder - startPosByX, AreaLength, MyMath.Direction.Down);
                startArea.leftBorder = startArea.rightBorder - AreaLength;

                Areas.AddFirst(startArea);
            }

            if (startArea.wasActivated && !endArea.wasDeactivated)
                obj.SetActive(true);
            else
                obj.SetActive(false);

            startArea.objectsToActivate.AddLast(obj);
            endArea.objectsToDeactivate.AddLast(obj);
        }

        public void Move(int shift)
        {
            foreach (CollidersArea area in Areas)
            {
                area.leftBorder += shift;
                area.rightBorder += shift;
            }
        }
    }
}