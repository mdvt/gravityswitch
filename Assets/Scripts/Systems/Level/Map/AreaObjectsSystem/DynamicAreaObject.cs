﻿#define VISIBLE_AREA_DEBUG

using UnityEngine;
using System;

using CollidersOptimizatingSystem;

[DisallowMultipleComponent, RequireComponent(typeof(ObjectWithOptimizedCollider))]
public abstract class DynamicAreaObject : StaticAreaObject
{
    [NonSerialized, Range(float.MinValue, 0)] public float LeftMaxVisibleBorder;
    [NonSerialized, Range(0, float.MaxValue)] public float RightMaxVisibleBorder;

    public override void AfterCreate()
    {
        GetMaxVisibleBorders(out LeftMaxVisibleBorder, out RightMaxVisibleBorder);

#if VISIBLE_AREA_DEBUG
        Area.localDebugLines.AddLast(new MyLibraries.DebugLine(Vector3.up * transform.position.y + Vector3.right * LeftMaxVisibleBorder,
                                                               Vector3.up * transform.position.y + Vector3.right * RightMaxVisibleBorder,
                                                               Color.red));
#endif

        GetMaxColliderBorders(out float leftBorder, out float rightBorder);

        GetComponent<ObjectWithOptimizedCollider>().Init(leftBorder, rightBorder);

        base.AfterCreate();
    }

    public void OnUpdate(float playerGlobalPosByX)
    {
        onUpdate(playerGlobalPosByX);
    }

    protected abstract void onUpdate(float playerGlobalPosByX);

    protected abstract void GetMaxVisibleBorders(out float leftBorder, out float rightBorder);

    protected abstract void GetMaxColliderBorders(out float leftBorder, out float rightBorder);
}