﻿using System;

using UnityEngine;

public abstract class AreaComponent : MonoBehaviour
{
    [NonSerialized] public AreaHandler Area;
    [NonSerialized] public Transform AreaTransform;

    public virtual void AfterCreate() 
    {
        afterCreate();
    }

    public virtual void BeforeDestroy() 
    {
        beforeDestroy();
    }

    protected virtual void afterCreate() { }
    protected virtual void beforeDestroy() { }
}
