﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using MyLibraries;

public class AreaComponentsManager
{
    private AreaHandler AreaForNewObjects;
    private Transform AreaForNewObjectsTransform;

    private Dictionary<GameObject, LinkedList<AreaComponent>> AreaComponents;
    private LinkedList<StaticAreaObject> StaticAreaObjects;
    private Dictionary<DynamicAreaObject, Transform> DynamicAreaObjects;

    private Dictionary<GameObject, LinkedList<AreaComponent>> AreaComponentsToActivate;

    public AreaComponentsManager(GameObjectStorage goStorage)
    {
        AreaComponents = new Dictionary<GameObject, LinkedList<AreaComponent>>();
        StaticAreaObjects = new LinkedList<StaticAreaObject>();
        DynamicAreaObjects = new Dictionary<DynamicAreaObject, Transform>();

        AreaComponentsToActivate = new Dictionary<GameObject, LinkedList<AreaComponent>>();

        goStorage.OnCreate += AddAreaObject;
        goStorage.OnDestroy += RemoveAreaObject;
    }

    public void SetArea(AreaHandler area)
    {
        AreaForNewObjects = area;
        AreaForNewObjectsTransform = area.transform;
    }

    public void UpdateAreaObjects(float playerPosByX, float leftUpdateAreaBorder, float rightUpdateAreaBorder)
    {
        foreach (KeyValuePair<DynamicAreaObject, Transform> pair in DynamicAreaObjects)
        {
            var dao = pair.Key;
            var transform = pair.Value;

            var daoLeftBorder = dao.AreaTransform.localPosition.x + dao.LeftMaxVisibleBorder;
            var daoRightBorder = dao.AreaTransform.localPosition.x + dao.RightMaxVisibleBorder;
            if (transform.gameObject.activeInHierarchy &&
                    (MyMath.InRange(daoLeftBorder, leftUpdateAreaBorder, rightUpdateAreaBorder) ||
                     MyMath.InRange(daoRightBorder, leftUpdateAreaBorder, rightUpdateAreaBorder) ||
                     (daoLeftBorder < leftUpdateAreaBorder && daoRightBorder > rightUpdateAreaBorder)))
                dao.OnUpdate(playerPosByX);
        }
    }

    public void OnTeleport()
    {
        foreach (StaticAreaObject sao in StaticAreaObjects)
            sao.OnTransformChanged();
    }

    public void DisableDynamicAreaObjectsFrom(AreaHandler area)
    {
        DynamicAreaObjects.RemoveAll(x => x.Area == area);
    }

    public void Reset()
    {
        AreaComponents.Clear();
        StaticAreaObjects.Clear();
        DynamicAreaObjects.Clear();

        AreaComponentsToActivate.Clear();
    }

    public IEnumerator ActivateAreaObjectsEnumerator()
    {
        foreach (KeyValuePair<GameObject, LinkedList<AreaComponent>> pair in AreaComponentsToActivate)
        {
            foreach (AreaComponent ac in pair.Value)
            {
                ac.AfterCreate();

                if (ac is StaticAreaObject)
                {
                    StaticAreaObjects.AddLast(ac as StaticAreaObject);

                    if (ac is DynamicAreaObject)
                        DynamicAreaObjects.Add(ac as DynamicAreaObject, ac.transform);
                }

                yield return null;
            }

            AreaComponents.Add(pair.Key, pair.Value);
        }

        AreaComponentsToActivate.Clear();
    }

    private void AddAreaObject(GameObject go)
    {
        AreaForNewObjects.AddObject(go.transform);

        LinkedList<AreaComponent> areaComponents = new LinkedList<AreaComponent>(go.GetComponentsInChildren<AreaComponent>());

        foreach (AreaComponent ac in areaComponents)
        {
            ac.Area = AreaForNewObjects;
            ac.AreaTransform = AreaForNewObjectsTransform;
        }

        if (areaComponents.Count != 0)
            AreaComponentsToActivate.Add(go, areaComponents);
    }

    private void RemoveAreaObject(GameObject go)
    {
        if (!AreaComponents.TryGetValue(go, out LinkedList<AreaComponent> areaComponents))
            return;

        AreaComponents.Remove(go);

        foreach (AreaComponent ac in areaComponents)
        {
            ac.BeforeDestroy();

            if (ac is StaticAreaObject)
            {
                StaticAreaObjects.Remove(ac as StaticAreaObject);

                if (ac is DynamicAreaObject)
                    DynamicAreaObjects.Remove(ac as DynamicAreaObject);
            }
        }
    }
}
