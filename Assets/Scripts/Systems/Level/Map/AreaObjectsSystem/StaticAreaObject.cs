﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Outlines;

public abstract class StaticAreaObject : AreaComponent
{
    private LinkedList<OutlinePath> OutlinePaths;

    public override void AfterCreate()
    {
        var points = GetLocalDangerPoints();

        if (points != null)
        {
            Vector2 localTrapPos = transform.localPosition - Area.transform.localPosition;

            if (points != null)
                foreach (Vector2 point in points)
                    Area.DangerPoints.AddLast((localTrapPos + point).ToVector2Int());
        }

        OutlinePaths = new LinkedList<OutlinePath>();

        base.AfterCreate();
    }

    public override void BeforeDestroy()
    {
        base.BeforeDestroy();

        foreach (OutlinePath path in OutlinePaths)
            path.Clear();
    }

    public void OnTransformChanged()
    {
        foreach (OutlinePath path in OutlinePaths)
            path.SyncOutlinesWithHandler();

        onTransformChanged();
    }

    protected OutlinePath CreateOutlinePath(MyLibraries.ColorizedPathDrawDir drawDir, Vector3 shift = default, bool closed = default)
    {
        OutlinePath result = new OutlinePath(gameObject, drawDir, GlobalSpace.game.map.outlinePathPrefabsHandler, shift, closed);

        OutlinePaths.AddLast(result);

        return result;
    }

    protected virtual void onTransformChanged() { }

    protected virtual IEnumerable GetLocalDangerPoints() { return null; }
}
