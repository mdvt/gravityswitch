﻿using UnityEngine;
using MyLibraries;
using MyLibraries.Figures;

namespace Path
{
    namespace Patterns
    {
        public enum HeightChangeTypes
        {
            none,
            increase,
            decrease,
            doubleIncrease,
            doubleDecrease,
        }

        public enum AngleType
        {
            _0,
            _45,
            _90
        }

        public struct LineType
        {
            public AngleType angle;
            public bool isSafe;

            public LineType(AngleType angle, bool isSafe)
            {
                this.angle = angle;
                this.isSafe = isSafe;
            }
        }

        public abstract class BasePattern
        {
            public abstract HeightChangeTypes ChangingHeight { get; }
            public abstract AngleType Angle { get; }
            public virtual bool MayTakeNegativeArg { get { return true; } }

            public abstract PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true);

            public abstract LineType GetTopLineType(int arg);
            public abstract LineType GetBottomLineType(int arg);

            public abstract Line2 GetTopLine(PathPoint lastPoint, int arg);
            public abstract Line2 GetBottomLine(PathPoint lastPoint, int arg);

            public override int GetHashCode()
            {
                return (int)Angle + 3 * (int)ChangingHeight + 12 * (MayTakeNegativeArg ? 1 : 0);
            }

            protected Line2 GetStraightLine(Vector2 pos)
            {
                return new Line2(pos, Vector2.right);
            }

            protected Line2 Get45Line(Vector2 pos, int dir)
            {
                return new Line2(pos, new Vector2(1, MyMath.Sign(dir)));
            }

            protected Line2 Get90Line(Vector2 pos, int dir)
            {
                return new Line2(pos, new Vector2(0, MyMath.Sign(dir)));
            }
        }

        public class Straight : BasePattern
        {
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.none; } }
            public override bool MayTakeNegativeArg { get { return false; } }
            public override AngleType Angle { get { return AngleType._0; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg <= 0)
                    throw new NegativeOrZeroPatternLenght();

                return new PathPoint(lastPoint.pos + Vector2Int.right * arg, lastPoint.height, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                return new LineType(AngleType._0, true);
            }

            public override LineType GetTopLineType(int arg)
            {
                return new LineType(AngleType._0, true);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                return GetStraightLine(lastPoint.pos);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                return GetStraightLine(lastPoint.pos + Vector2.up * lastPoint.height);
            }
        }

        public class Shift45 : BasePattern
        {
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.none; } }
            public override AngleType Angle { get { return AngleType._45; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg == 0)
                    throw new NegativeOrZeroPatternLenght();

                return new PathPoint(lastPoint.pos + new Vector2Int(Mathf.Abs(arg), arg), lastPoint.height, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                return new LineType(AngleType._45, arg < 0);
            }

            public override LineType GetTopLineType(int arg)
            {
                return new LineType(AngleType._45, arg > 0);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                return Get45Line(lastPoint.pos + (arg < 0 ? Vector2.right : Vector2.zero), arg);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                return Get45Line(lastPoint.pos + lastPoint.height * Vector2.up + (arg > 0 ? Vector2.right : Vector2.zero), arg);
            }
        }

        public class Expantion45 : BasePattern
        {
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.increase; } }
            public override AngleType Angle { get { return AngleType._45; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg == 0)
                    throw new NegativeOrZeroPatternLenght();

                if (arg > 0)
                    return new PathPoint(lastPoint.pos + Vector2Int.right * arg, lastPoint.height + arg, this, generateTop, generateBottom);
                else
                    return new PathPoint(lastPoint.pos + new Vector2Int(-arg, arg), lastPoint.height - arg, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._0, true);
                else
                    return new LineType(AngleType._45, true);
            }

            public override LineType GetTopLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._45, true);
                else
                    return new LineType(AngleType._0, true);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return GetStraightLine(lastPoint.pos);
                else
                    return Get45Line(lastPoint.pos + Vector2.right, -1);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return Get45Line(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right, 1);
                else
                    return GetStraightLine(lastPoint.pos + lastPoint.height * Vector2.up);
            }
        }

        public class Taper45 : BasePattern
        {
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.decrease; } }
            public override AngleType Angle { get { return AngleType._45; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg == 0)
                    throw new NegativeOrZeroPatternLenght();

                if (lastPoint.height <= Mathf.Abs(arg))
                    throw new NotValidArgument();

                if (arg < 0)
                    return new PathPoint(lastPoint.pos + Vector2Int.right * -arg, lastPoint.height + arg, this, generateTop, generateBottom);
                else
                    return new PathPoint(lastPoint.pos + new Vector2Int(arg, arg), lastPoint.height - arg, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._45, false);
                else
                    return new LineType(AngleType._0, true);
            }

            public override LineType GetTopLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._0, true);
                else
                    return new LineType(AngleType._45, false);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return Get45Line(lastPoint.pos, 1);
                else
                    return GetStraightLine(lastPoint.pos);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return GetStraightLine(lastPoint.pos + lastPoint.height * Vector2.up);
                else
                    return Get45Line(lastPoint.pos + lastPoint.height * Vector2.up, -1);
            }
        }

        public class Expantion90 : BasePattern
        {
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.increase; } }
            public override AngleType Angle { get { return AngleType._90; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg == 0)
                    throw new NegativeOrZeroPatternLenght();

                if (arg > 0)
                    return new PathPoint(lastPoint.pos + Vector2Int.right, lastPoint.height + arg, this, generateTop, generateBottom);
                else
                    return new PathPoint(lastPoint.pos + new Vector2Int(1, arg), lastPoint.height - arg, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._0, true);
                else
                    return new LineType(AngleType._90, true);
            }

            public override LineType GetTopLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._90, true);
                else
                    return new LineType(AngleType._0, true);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return GetStraightLine(lastPoint.pos);
                else
                    return Get90Line(lastPoint.pos + Vector2.right, -1);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return Get90Line(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right, 1);
                else
                    return GetStraightLine(lastPoint.pos + lastPoint.height * Vector2.up);
            }
        }

        public class Taper90 : BasePattern
        {
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.decrease; } }
            public override AngleType Angle { get { return AngleType._90; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg == 0)
                    throw new NegativeOrZeroPatternLenght();

                if (lastPoint.height <= Mathf.Abs(arg))
                    throw new NotValidArgument();

                if (arg < 0)
                    return new PathPoint(lastPoint.pos + Vector2Int.right, lastPoint.height + arg, this, generateTop, generateBottom);
                else
                    return new PathPoint(lastPoint.pos + new Vector2Int(1, arg), lastPoint.height - arg, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._90, false);
                else
                    return new LineType(AngleType._0, true);
            }

            public override LineType GetTopLineType(int arg)
            {
                if (arg > 0)
                    return new LineType(AngleType._0, true);
                else
                    return new LineType(AngleType._90, false);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return Get90Line(lastPoint.pos + Vector2.right, 1);
                else
                    return GetStraightLine(lastPoint.pos);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                if (arg > 0)
                    return GetStraightLine(lastPoint.pos + lastPoint.height * Vector2.up);
                else
                    return Get90Line(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right, -1);
            }
        }

        public class DoubleHeightExpantion45 : BasePattern
        {
            public override bool MayTakeNegativeArg { get { return false; } }
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.doubleIncrease; } }
            public override AngleType Angle { get { return AngleType._45; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg <= 0)
                    throw new NegativeOrZeroPatternLenght();

                return new PathPoint(lastPoint.pos + new Vector2Int(arg, -arg), lastPoint.height + arg * 2, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                return new LineType(AngleType._45, true);
            }

            public override LineType GetTopLineType(int arg)
            {
                return new LineType(AngleType._45, true);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                return Get45Line(lastPoint.pos + Vector2.right, -1);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                return Get45Line(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right, 1);
            }
        }

        public class DoubleHeightTaper45 : BasePattern
        {
            public override bool MayTakeNegativeArg { get { return false; } }
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.doubleDecrease; } }
            public override AngleType Angle { get { return AngleType._45; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg <= 0)
                    throw new NegativeOrZeroPatternLenght();

                if (lastPoint.height - arg * 2 <= 0)
                    throw new NotValidArgument();

                return new PathPoint(lastPoint.pos + new Vector2Int(arg, arg), lastPoint.height - arg * 2, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                return new LineType(AngleType._45, false);
            }

            public override LineType GetTopLineType(int arg)
            {
                return new LineType(AngleType._45, false);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                return Get45Line(lastPoint.pos, 1);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                return Get45Line(lastPoint.pos + lastPoint.height * Vector2.up, -1);
            }
        }

        public class DoubleHeightExpantion90 : BasePattern
        {
            public override bool MayTakeNegativeArg { get { return false; } }
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.doubleIncrease; } }
            public override AngleType Angle { get { return AngleType._90; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg <= 0)
                    throw new NegativeOrZeroPatternLenght();

                return new PathPoint(lastPoint.pos + new Vector2Int(1, -arg), lastPoint.height + arg * 2, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                return new LineType(AngleType._90, true);
            }

            public override LineType GetTopLineType(int arg)
            {
                return new LineType(AngleType._90, true);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                return Get90Line(lastPoint.pos + Vector2.right, -1);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                return Get90Line(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right, 1);
            }
        }

        public class DoubleHeightTaper90 : BasePattern
        {
            public override bool MayTakeNegativeArg { get { return false; } }
            public override HeightChangeTypes ChangingHeight { get { return HeightChangeTypes.doubleDecrease; } }
            public override AngleType Angle { get { return AngleType._90; } }

            public override PathPoint Generate(PathPoint lastPoint, int arg, bool generateBottom = true, bool generateTop = true)
            {
                if (arg <= 0)
                    throw new NegativeOrZeroPatternLenght();

                if (lastPoint.height - arg * 2 <= 0)
                    throw new NotValidArgument();

                return new PathPoint(lastPoint.pos + new Vector2Int(1, arg), lastPoint.height - arg * 2, this, generateTop, generateBottom);
            }

            public override LineType GetBottomLineType(int arg)
            {
                return new LineType(AngleType._90, false);
            }

            public override LineType GetTopLineType(int arg)
            {
                return new LineType(AngleType._90, false);
            }

            public override Line2 GetBottomLine(PathPoint lastPoint, int arg)
            {
                return Get90Line(lastPoint.pos + Vector2.right, 1);
            }

            public override Line2 GetTopLine(PathPoint lastPoint, int arg)
            {
                return Get90Line(lastPoint.pos + lastPoint.height * Vector2.up + Vector2.right, -1);
            }
        }

        public class NegativeOrZeroPatternLenght : System.Exception
        {
            public NegativeOrZeroPatternLenght() : base("Pattern lenght must be positive")
            { }
        }

        public class NotValidArgument : System.Exception
        {
            public NotValidArgument() : base("Not valid path pattern argument")
            { }
        }
    }
}