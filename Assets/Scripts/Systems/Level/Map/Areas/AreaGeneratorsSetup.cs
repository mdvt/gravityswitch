﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AreaGeneratorsSetup : MonoBehaviour
{
    [Serializable]
    public class GeneratorInfo
    {
        public string Title;
        public bool Enabled;
        [Range(0, 1)] public float StartDifficulty;
        [Range(0, 1)] public float EndDifficulty;
    }

    public List<GeneratorInfo> Generators;
}
