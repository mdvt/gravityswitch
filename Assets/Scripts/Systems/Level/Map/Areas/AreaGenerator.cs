﻿//#define LOAD_TIME_DEBUG

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyLibraries;

using Path;
using Outlines;
using System.Linq;
using System;

public abstract class AreaGenerator
{
    public float StartDifficulty;
    public float EndDifficulty;
    public abstract IntRangeSet PossibleStartHeights(float difficulty);
    protected abstract IEnumerator<LinkedList<PathPoint>> Generate(GenerateInfo info);

    private static GameObject abyssDangerOutline = abyssOutlineInit(GlobalSpace.game.InteractiveObjectsColors.danger);
    private static GameObject abyssSafeOutline = abyssOutlineInit(GlobalSpace.game.InteractiveObjectsColors.safe);

    private static GameObject abyssOutlineInit(Color color)
    {
        var go = GameObject.Instantiate(GlobalSpace.game.prefabs.outline);

        var transform = go.transform;
        transform.position = Vector3.up * 10000;
        transform.localScale = new Vector3(GlobalSpace.game.prefabs.pillarHeight - 1, GlobalSpace.game.map.OutlineWidth, 1);
        transform.parent = GlobalSpace.game.transform;
        transform.eulerAngles = new Vector3(0, 0, 90);

        var renderer = go.GetComponent<SpriteRenderer>();
        renderer.material.SetVector("_LeftColor", color);
        renderer.material.SetVector("_RightColor", color);

        return go;
    }

    public IEnumerator GenerateNewArea(GenerateInfo info)
    {
#if LOAD_TIME_DEBUG
        float sumTime = 0;
        float startTime = Time.realtimeSinceStartup;
        string message = "pattern: " + GetType();
#endif

        var generateRoutine = Generate(info);
#if !LOAD_TIME_DEBUG
        while (generateRoutine.MoveNext()) { yield return null; }
#else
        while (generateRoutine.MoveNext()) ;
        message += "\nGenerate() time = " + (Time.realtimeSinceStartup - startTime) * 1000 + " milliseconds";
        sumTime += (Time.realtimeSinceStartup - startTime) * 1000;
#endif

        info.area.Length = generateRoutine.Current.Last.Value.pos.x;
        info.area.path = GlobalSpace.game.map.globalPath.AddLocalPath(generateRoutine.Current.ToList());
        GlobalSpace.game.map.railMap.AddRailsLayers(info.area.Length);

        /*post generate*/
        var subroutine = LoadBounds(info.area);
#if !LOAD_TIME_DEBUG
        while (subroutine.MoveNext()) { yield return null; }
#else
        startTime = Time.realtimeSinceStartup;
        while (subroutine.MoveNext()) ;
        message += "\nLoadBounds() time = " + (Time.realtimeSinceStartup - startTime) * 1000 + " milliseconds";
        sumTime += (Time.realtimeSinceStartup - startTime) * 1000;
#endif

#if LOAD_TIME_DEBUG
        startTime = Time.realtimeSinceStartup;
#endif

        /*Activate all AreaObjects*/
        subroutine = GlobalSpace.game.map.areaObjectsManager.ActivateAreaObjectsEnumerator();
        while (subroutine.MoveNext()) yield return null;
        /*Activate all AreaObjects*/

        SetupOutlinePaths(info.area); // setup outlines points

        // connect with prev area
        if (info.lastArea != null)
        {
            ConnectPaths(info.lastArea.topOutlinePaths, info.area.topOutlinePaths, info.lastArea, info.area);
            ConnectPaths(info.lastArea.bottomOutlinePaths, info.area.bottomOutlinePaths, info.lastArea, info.area);

            InitOutlinesColors(info.lastArea);
        }
        // connect with prev area

        // mark zones that danger for player
        InitOutlinesColors(info.area);
        // mark zones thar danger for player

        // setup rails by points
        LoadRailsByOutlines(info.area);
        // setup rails by points

#if LOAD_TIME_DEBUG
        message += "\nline setup time = " + (Time.realtimeSinceStartup - startTime) * 1000 + " milliseconds";
        sumTime += (Time.realtimeSinceStartup - startTime) * 1000;
#endif

#if LOAD_TIME_DEBUG
        startTime = Time.realtimeSinceStartup;
#endif
        // applying changes to prev area outlines 
        if (info.lastArea != null)
        {
            if (info.lastArea.topOutlinePaths.Count != 0)
                subroutine = info.lastArea.topOutlinePaths.Last.Value.RedrawEnumerator();
#if !LOAD_TIME_DEBUG
            while (subroutine.MoveNext()) { yield return null; }
#else
            while (subroutine.MoveNext()) ;
#endif
            if (info.lastArea.bottomOutlinePaths.Count != 0)
                subroutine = info.lastArea.bottomOutlinePaths.Last.Value.RedrawEnumerator();
#if !LOAD_TIME_DEBUG
            while (subroutine.MoveNext()) { yield return null; }
#else
            while (subroutine.MoveNext()) ;
#endif
        }
        // applying changes to prev area outlines 
#if LOAD_TIME_DEBUG
        message += "\nprev area outlines load time = " + (Time.realtimeSinceStartup - startTime) * 1000 + " milliseconds";
        sumTime += (Time.realtimeSinceStartup - startTime) * 1000;
#endif

#if LOAD_TIME_DEBUG
        startTime = Time.realtimeSinceStartup;
#endif
        // draw curr area outlines
        foreach (OutlinePath path in info.area.topOutlinePaths)
        {
            subroutine = path.RedrawEnumerator();
#if !LOAD_TIME_DEBUG
            while (subroutine.MoveNext()) { yield return null; }
#else
            while (subroutine.MoveNext()) ;
#endif
        }
        foreach (OutlinePath path in info.area.bottomOutlinePaths)
        {
            subroutine = path.RedrawEnumerator();
#if !LOAD_TIME_DEBUG
            while (subroutine.MoveNext()) { yield return null; }
#else
            while (subroutine.MoveNext()) ;
#endif
        }
        // draw curr area outlines
#if LOAD_TIME_DEBUG
        message += "\ncurr area outlines load time = " + (Time.realtimeSinceStartup - startTime) * 1000 + " milliseconds";
        sumTime += (Time.realtimeSinceStartup - startTime) * 1000;
#endif
        /*post generate*/

#if LOAD_TIME_DEBUG
        message += "\nload time = " + sumTime;

        Debug.Log(message);
        yield return null;
#endif
    }

    protected static GameObject LoadPrefab(AreaHandler area, GameObject prefab, Vector2 localPos, Quaternion rot)
    {
        var goStorage = GlobalSpace.game.storages.AreaObjectsStorage;
        return goStorage.AddToScene(prefab, (Vector2)area.transform.position + localPos, rot);
    }

    protected static GameObject LoadPrefab(AreaHandler area, GameObject prefab)
    {
        var goStorage = GlobalSpace.game.storages.AreaObjectsStorage;
        return goStorage.AddToScene(prefab);
    }

    private static IEnumerator LoadBounds(AreaHandler area)
    {
        for (int x = 0; x < area.Length; x++)
        {
            PathPoint point = area.path.GetPoint(x);

            if (point.generateTop)
                LoadPrefab(area, GlobalSpace.game.prefabs.pillar,
                           point.pos + Vector2.up * point.height + Vector2.up * GlobalSpace.game.prefabs.pillarHeight / 2 + Vector2.right * 0.5f,
                           Quaternion.identity);

            yield return null;

            if (point.generateBottom)
                LoadPrefab(area, GlobalSpace.game.prefabs.pillar,
                           point.pos + Vector2.down * GlobalSpace.game.prefabs.pillarHeight / 2 + Vector2.right * 0.5f,
                           Quaternion.identity);

            yield return null;
        }
    }

    private static void InitOutlinesColors(AreaHandler area)
    {
        foreach (OutlinePath path in area.topOutlinePaths)
        {
            path.InitColors(delegate (Vector2Int first, Vector2Int second) { return area.DangerPoints.Contains(first) && area.DangerPoints.Contains(second); });
            path.InitColors(path.IsFragmentDanger);
        }

        foreach (OutlinePath path in area.bottomOutlinePaths)
        {
            path.InitColors(delegate (Vector2Int first, Vector2Int second) { return area.DangerPoints.Contains(first) && area.DangerPoints.Contains(second); });
            path.InitColors(path.IsFragmentDanger);
        }
    }

    private static void LoadRailsByOutlines(AreaHandler area)
    {
        foreach (OutlinePath path in area.topOutlinePaths)
            path.CreateRails();

        foreach (OutlinePath path in area.bottomOutlinePaths)
            path.CreateRails();
    }

    private static void SetupOutlinePaths(AreaHandler area)
    {
        LinkedList<OutlinePath> topPaths = area.topOutlinePaths;
        LinkedList<OutlinePath> bottomPaths = area.bottomOutlinePaths;

        OutlinePath lastTopPath = null;
        OutlinePath lastBottomPath = null;

        bool prevGenerateBottom = false;
        bool prevGenerateTop = false;
        for (int x = 0; x < area.Length; x++)
        {
            PathPoint point = area.path.GetPoint(x);

            if (point.generateTop)
            {
                if (!prevGenerateTop)
                {
                    lastTopPath = topPaths.AddLast(new OutlinePath(area.gameObject, ColorizedPathDrawDir.Right, GlobalSpace.game.map.outlinePathPrefabsHandler)).Value;

                    if (x != 0)
                    {
                        lastTopPath.AddLastPoint(point.pos + new Vector2Int(0, point.height + 1));
                        var transform = GlobalSpace.game.storages.OutlinesStorage.AddToScene(abyssDangerOutline).transform;
                        transform.position = area.transform.position + (Vector3)(Vector2)lastTopPath.Points.Last.Value.pos +
                                             (GlobalSpace.game.map.OutlineWidth / 2) * Vector3.right +
                                             ((GlobalSpace.game.prefabs.pillarHeight - 1) / 2) * Vector3.up + Vector3.back;
                        area.AddObject(transform);
                    }
                    else
                        lastTopPath.AddLastPoint(point.pos + new Vector2Int(0, point.height));
                }

                if (lastTopPath.Points.Last.Value.pos.y != point.pos.y + point.height)
                {
                    int dir = MyMath.Sign(point.pos.y + point.height - lastTopPath.Points.Last.Value.pos.y);
                    for (int y = lastTopPath.Points.Last.Value.pos.y + dir; y != point.pos.y + point.height + dir; y += dir)
                        lastTopPath.AddLastPoint(new Vector2Int(point.pos.x, y));
                }

                lastTopPath.AddLastPoint(point.pos + new Vector2Int(0, point.height) + Vector2Int.right);
            }
            else if (prevGenerateTop)
            {
                lastTopPath.AddLastPoint(point.pos + new Vector2Int(0, point.height + 1));
                var transform = GlobalSpace.game.storages.OutlinesStorage.AddToScene(abyssSafeOutline).transform;
                transform.position = area.transform.position + (Vector3)(Vector2)lastTopPath.Points.Last.Value.pos +
                                     GlobalSpace.game.map.OutlineWidth / 2 * Vector3.left +
                                     (GlobalSpace.game.prefabs.pillarHeight - 1) / 2 * Vector3.up + Vector3.back;
                area.AddObject(transform);
            }

            prevGenerateTop = point.generateTop;

            if (point.generateBottom)
            {
                if (!prevGenerateBottom)
                {
                    lastBottomPath = bottomPaths.AddLast(new OutlinePath(area.gameObject, ColorizedPathDrawDir.Left, GlobalSpace.game.map.outlinePathPrefabsHandler)).Value;

                    if (x != 0)
                    {
                        lastBottomPath.AddLastPoint(point.pos + Vector2Int.down);
                        var transform = GlobalSpace.game.storages.OutlinesStorage.AddToScene(abyssDangerOutline).transform;
                        transform.position = area.transform.position + (Vector3)(Vector2)lastBottomPath.Points.Last.Value.pos +
                                             (GlobalSpace.game.map.OutlineWidth / 2) * Vector3.right +
                                             ((GlobalSpace.game.prefabs.pillarHeight - 1) / 2) * Vector3.down + Vector3.back;
                        area.AddObject(transform);
                    }
                    else
                        lastBottomPath.AddLastPoint(point.pos);
                }

                if (lastBottomPath.Points.Last.Value.pos.y != point.pos.y)
                {
                    int dir = MyMath.Sign(point.pos.y - lastBottomPath.Points.Last.Value.pos.y);
                    for (int y = lastBottomPath.Points.Last.Value.pos.y + dir; y != point.pos.y + dir; y += dir)
                        lastBottomPath.AddLastPoint(new Vector2Int(point.pos.x, y));
                }

                lastBottomPath.AddLastPoint(point.pos + Vector2Int.right);
            }
            else if (prevGenerateBottom)
            {
                lastBottomPath.AddLastPoint(point.pos + Vector2Int.down);
                var transform = GlobalSpace.game.storages.OutlinesStorage.AddToScene(abyssSafeOutline).transform;
                transform.position = area.transform.position + (Vector3)(Vector2)lastBottomPath.Points.Last.Value.pos +
                                     GlobalSpace.game.map.OutlineWidth / 2 * Vector3.left +
                                     (GlobalSpace.game.prefabs.pillarHeight - 1) / 2 * Vector3.down + Vector3.back;
                area.AddObject(transform);
            }

            prevGenerateBottom = point.generateBottom;
        }
    }

    private static void ConnectPaths(LinkedList<OutlinePath> firstPaths, LinkedList<OutlinePath> secondPaths, AreaHandler firstArea, AreaHandler secondArea)
    {
        if (firstPaths.Count == 0 && secondPaths.Count == 0)
            return;

        OutlinePath first = firstPaths.Last?.Value;
        OutlinePath second = secondPaths.First?.Value;

        if (firstPaths.Count == 0)
            SetupAbyssOutlinesOnStart(second, secondArea);
        else if (secondPaths.Count == 0)
            SetupAbyssOutlinesOnEnd(first, firstArea);
        else if (first.Handler.transform.position.x + first.Points.Last.Value.pos.x ==
                second.Handler.transform.position.x + second.Points.First.Value.pos.x)
            first.NextAttachedPath = second;
        else if (second.Points.First.Value.pos.x == 0)
            SetupAbyssOutlinesOnStart(second, secondArea);
        else
            SetupAbyssOutlinesOnEnd(first, firstArea);
    }

    private static void SetupAbyssOutlinesOnStart(OutlinePath path, AreaHandler area)
    {
        path.AddFirstPoint(path.Points.First.Value.pos + new Vector2Int(0, (int)path.DrawDir));
        var transform = GlobalSpace.game.storages.OutlinesStorage.AddToScene(abyssDangerOutline).transform;
        transform.position = area.transform.position + (Vector3)(Vector2)path.Points.First.Value.pos +
                             (GlobalSpace.game.map.OutlineWidth / 2) * Vector3.right +
                             ((GlobalSpace.game.prefabs.pillarHeight - 1) / 2) * (int)path.DrawDir * Vector3.up + Vector3.back;
        area.AddObject(transform);
    }

    private static void SetupAbyssOutlinesOnEnd(OutlinePath path, AreaHandler area)
    {
        path.AddLastPoint(path.Points.Last.Value.pos + new Vector2Int(0, (int)path.DrawDir));
        var transform = GlobalSpace.game.storages.OutlinesStorage.AddToScene(abyssSafeOutline).transform;
        transform.position = area.transform.position + (Vector3)(Vector2)path.Points.Last.Value.pos +
                             (GlobalSpace.game.map.OutlineWidth / 2) * Vector3.left +
                             ((GlobalSpace.game.prefabs.pillarHeight - 1) / 2) * (int)path.DrawDir * Vector3.up + Vector3.back;
        area.AddObject(transform);
    }

    public static DiscreteFunction DiscreteNormalDistributionByDifficulty(float difficulty, IntRange valuesRange, float maxDispersionRelevantToRangeSize = 1)
    {
        return new DiscreteFunction(50, valuesRange.start, valuesRange.end, NormalDistributionByDifficulty(difficulty, valuesRange, maxDispersionRelevantToRangeSize));
    }

    public static Func<float, float> NormalDistributionByDifficulty(float difficulty, IntRange valuesRange, float maxDispersionRelevantToRangeSize = 1)
    {
        return MyRandom.NormalDistributionDensityFunc((valuesRange.Length / 3f) * maxDispersionRelevantToRangeSize,
                                                        MyMath.Map(difficulty, 0, 1, valuesRange.start, valuesRange.end));
    }

    protected static int LerpParamByDifficulty(float difficulty, int from, int to)
    {
        difficulty = Mathf.Clamp01(difficulty);
        return from + Mathf.RoundToInt(difficulty * (to - from));
    }

    public class GenerateInfo
    {
        public AreaHandler area;
        public readonly float difficulty;
        public readonly PathPoint lastPoint;
        public AreaHandler lastArea;
        public object additionalInfo;

        public GenerateInfo(AreaHandler area, float difficulty, PathPoint lastPoint, AreaHandler lastArea, object additionalInfo = null)
        {
            this.area = area;
            this.difficulty = difficulty;
            this.lastPoint = lastPoint;
            this.lastArea = lastArea;
            this.additionalInfo = additionalInfo;
        }
    }
}