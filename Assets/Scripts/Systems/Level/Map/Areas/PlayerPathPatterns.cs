﻿using System.Collections.Generic;
using System;
using MyLibraries;
using Mathf = UnityEngine.Mathf;

public class KeepSwitchActionsSequience
{
    public readonly int KeepLength;
    public readonly float DistanceToSwitch;

    public KeepSwitchActionsSequience(int keepLength, float distanceToSwitch)
    {
        KeepLength = keepLength;
        DistanceToSwitch = distanceToSwitch;
    }

    public int GetSwitchLength(int height, float playerFallTan)
    {
        return (int)(DistanceToSwitch + height / playerFallTan) + 1;
    }
}

public class PlayerPath : LinkedList<KeepSwitchActionsSequience>
{
    public void BasicBuild(DiscreteFunction keepLengthRandomDistributionDensityFunct, DiscreteFunction distanceToSwitchRandomDistributionDensityFunct, int minActionsCount)
    {
        if (minActionsCount <= 0)
            throw new Exception("min patterns count must be > 0");

        if (Count != 0)
            Clear();

        for (int i = 0; i < minActionsCount - 1; i += 2)
        {
            AddLast(
                new KeepSwitchActionsSequience(
                    Mathf.RoundToInt(MyRandom.RandomByDistributionDensity(keepLengthRandomDistributionDensityFunct)),
                    MyRandom.RandomByDistributionDensity(distanceToSwitchRandomDistributionDensityFunct)));
        }
        AddLast(new KeepSwitchActionsSequience(Mathf.RoundToInt(MyRandom.RandomByDistributionDensity(keepLengthRandomDistributionDensityFunct)), -1));
    }
}