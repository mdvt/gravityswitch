﻿using System.Collections.Generic;
using UnityEngine;
using System;

using Path;
using Outlines;
using MyLibraries;
using System.Collections;

public class AreaHandler : MonoBehaviour
{
    [NonSerialized] public int Length;
    [NonSerialized] public LocalPath path;
    [NonSerialized] public AreaGenerator generator;
    [NonSerialized] public LinkedList<OutlinePath> topOutlinePaths;
    [NonSerialized] public LinkedList<OutlinePath> bottomOutlinePaths;
    [NonSerialized] public LinkedList<Vector2Int> DangerPoints;

    [NonSerialized] public LinkedList<DebugLine> localDebugLines;

    private Transform cachedTransform;
    private LinkedList<Transform> areaObjects;

    public void Init()
    {
        cachedTransform = transform;
        areaObjects = new LinkedList<Transform>();

        topOutlinePaths = new LinkedList<OutlinePath>();
        bottomOutlinePaths = new LinkedList<OutlinePath>();
        DangerPoints = new LinkedList<Vector2Int>();

        localDebugLines = new LinkedList<DebugLine>();
    }

    public void AddObject(Transform transform)
    {
        areaObjects.AddLast(transform);
    }

    public void OnRemove()
    {
        if (topOutlinePaths.Count != 0)
            topOutlinePaths.Last.Value.NextAttachedPath = null;
        if (bottomOutlinePaths.Count != 0)
            bottomOutlinePaths.Last.Value.NextAttachedPath = null;

        GlobalSpace.game.map.areaObjectsManager.DisableDynamicAreaObjectsFrom(this);
        AsyncRemoveAllObjects(delegate { Destroy(gameObject); });
    }

    public void Move(Vector3 shift)
    {
        cachedTransform.localPosition += shift;
        UpdateObjects(shift);
    }

    public void RemoveAllObjects()
    {
        var routine = RemoveAllObjectsEnumerator();
        while (routine.MoveNext()) ;
    }

    public void AsyncRemoveAllObjects(Action onFinish)
    {
        cachedTransform.localPosition += Vector3.up * 10000;
        UpdateObjects(Vector3.up * 10000);

        CoroutineProcess process = new CoroutineProcess(this, RemoveAllObjectsEnumerator, 500);
        process.Finished = onFinish;
        process.Start();
    }

    private void UpdateObjects(Vector3 shift)
    {
        foreach (Transform t in areaObjects)
            t.localPosition += shift;

        foreach (OutlinePath path in topOutlinePaths)
            path.SyncOutlinesWithHandler();
        foreach (OutlinePath path in bottomOutlinePaths)
            path.SyncOutlinesWithHandler();
    }

    private IEnumerator RemoveAllObjectsEnumerator()
    {
        GameObjectStorage storage = GlobalSpace.game.storages.AreaObjectsStorage;

        foreach (Transform t in areaObjects)
        {
            storage.RemoveFromScene(t.gameObject);
            yield return null;
        }
        areaObjects.Clear();

        foreach (OutlinePath path in topOutlinePaths)
        {
            var routine = path.ClearEnumerator();
            while (routine.MoveNext()) yield return null;
        }

        foreach (OutlinePath path in bottomOutlinePaths)
        {
            var routine = path.ClearEnumerator();
            while (routine.MoveNext()) yield return null;
        }
    }

    public void DebugDraw()
    {
        foreach (DebugLine debugLine in localDebugLines)
            Debug.DrawLine(cachedTransform.position + debugLine.segment.startPoint + Vector3.back,
                           cachedTransform.position + debugLine.segment.endPoint + Vector3.back,
                           debugLine.color);

        Debug.DrawLine(cachedTransform.position + Vector3.up * 100, cachedTransform.position + Vector3.down * 100, Color.red);
        Debug.DrawLine(cachedTransform.position + Vector3.right * Length + Vector3.up * 100, cachedTransform.position + Vector3.right * Length + Vector3.down * 100, Color.red);
    }
}