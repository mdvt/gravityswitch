﻿using System.Collections;
using System;
using UnityEngine;
using MyLibraries;

namespace Outlines
{
    public class OutlinePathPrefabsHandler : ColorizedPathOptimizedPrefabsHandler
    {
        private GameObject BaseOutline;
        private float Width;
        private Color Safe;
        private Color Danger;

        public OutlinePathPrefabsHandler(GameObject baseOutline, GameObjectStorage storage, float width, Color safe, Color danger) : base(storage)
        {
            BaseOutline = baseOutline;
            Width = width;
            Safe = safe;
            Danger = danger;
        }

        protected override IEnumerable GenerateAnchors()
        {
            ModifiableKeyValuePair<int, GameObject> pair = new ModifiableKeyValuePair<int, GameObject>();
            for (int shift = 0; shift < 4; shift++)
            {
                for (int leftType = 4; leftType < 7; leftType++)
                {
                    for (int rightType = 7; rightType < 10; rightType++)
                    {
                        for (int startColor = 10; startColor < 12; startColor++)
                        {
                            for (int endColor = 12; endColor < 14; endColor++)
                            {
                                pair.Key = (1 << shift) + (1 << leftType) + (1 << rightType) + (1 << startColor) + (1 << endColor);

                                pair.Value = GameObject.Instantiate(BaseOutline);
                                var renderer = pair.Value.GetComponent<SpriteRenderer>();
                                var transform = pair.Value.transform;

                                float startShiftByX = shift == 1 || shift == 3 ? Width : 0; // shift setup
                                float endShiftByX = shift == 2 || shift == 3 ? Width : 0;

                                transform.localScale = new Vector3(1 + startShiftByX + endShiftByX, Width, 1);

                                if (leftType == 5) // start cut setup
                                    renderer.material.SetFloat("_LeftTopPointX", Width / transform.localScale.x);
                                if (leftType == 6)
                                    renderer.material.SetFloat("_LeftBottomPointX", Width / transform.localScale.x);

                                if (rightType == 8) // end cut setup
                                    renderer.material.SetFloat("_RightTopPointX", 1 - Width / transform.localScale.x);
                                if (rightType == 9)
                                    renderer.material.SetFloat("_RightBottomPointX", 1 - Width / transform.localScale.x);

                                renderer.material.SetVector("_LeftColor", startColor == 10 ? Safe : Danger);
                                renderer.material.SetVector("_RightColor", endColor == 12 ? Safe : Danger);

                                yield return pair;
                            }
                        }
                    }
                }
            }
        }
    }
}
