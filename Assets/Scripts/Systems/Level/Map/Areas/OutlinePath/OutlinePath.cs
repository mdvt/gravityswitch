﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;
using MyLibraries;

namespace Outlines
{
    public enum FragmentColorType { danger, safe, gradient }

    public class OutlinePathPoint
    {
        private readonly ColorizedPathBase.ColorizedPoint point;

        private readonly Color danger;
        private readonly Color safe;

        public FragmentColorType prevFragmentType;

        public OutlinePathPoint(ColorizedPathBase.ColorizedPoint point, Color danger, Color safe)
        {
            this.point = point;
            this.danger = danger;
            this.safe = safe;
            prevFragmentType = FragmentColorType.safe;
        }

        public Vector2Int pos
        {
            get
            {
                return point.pos;
            }
        }

        public bool hasDangerColor
        {
            get
            {
                return point.color == danger;
            }
            set
            {
                point.color = value ? danger : safe;
            }
        }
    }

    public class OutlinePath : ColorizedPathOptimized90DegreeBase
    {
        public LinkedList<OutlinePathPoint> Points;

        private readonly Color DangerColor;
        private readonly Color SafeColor;

        private OutlinePath prevAttachedPath;
        private OutlinePath nextAttachedPath;

        public OutlinePath PrevAttachedPath
        {
            get
            {
                return prevAttachedPath;
            }

            set
            {
                if (value != null)
                    ConnectPaths(value, this);
                else if (prevAttachedPath != null)
                {
                    prevAttachedPath.nextAttachedPath = null;
                    prevAttachedPath = null;
                }
            }
        }

        public OutlinePath NextAttachedPath
        {
            get
            {
                return nextAttachedPath;
            }

            set
            {
                if (value != null)
                    ConnectPaths(this, value);
                else if (nextAttachedPath != null)
                {
                    nextAttachedPath.prevAttachedPath = null;
                    nextAttachedPath = null;
                }
            }
        }

        public OutlinePath(GameObject handler, ColorizedPathDrawDir drawDir, OutlinePathPrefabsHandler prefabsHandler, Vector3 shift = default, bool closed = false) :
            base(handler, drawDir, GlobalSpace.game.map.OutlineWidth, prefabsHandler, shift, closed)
        {
            Points = new LinkedList<OutlinePathPoint>();

            DangerColor = GlobalSpace.game.InteractiveObjectsColors.danger;
            SafeColor = GlobalSpace.game.InteractiveObjectsColors.safe;

            prevAttachedPath = null;
            nextAttachedPath = null;
        }

        public void AddLastPoint(Vector2Int point)
        {
            if (nextAttachedPath != null)
                throw new Exception("Can't add point to the end connected with another path");

            AddLastPoint(point, SafeColor);

            Points.AddLast(new OutlinePathPoint(ColorizedPoints.Last.Value, DangerColor, SafeColor));
        }

        public void AddFirstPoint(Vector2Int point)
        {
            if (prevAttachedPath != null)
                throw new Exception("Can't add point to the end connected with another path");

            AddFirstPoint(point, SafeColor);

            Points.AddFirst(new OutlinePathPoint(ColorizedPoints.First.Value, DangerColor, SafeColor));
        }

        protected override IEnumerator BaseRedrawEnumerator(Func<ColorizedInterval, bool> predicate = null)
        {
            ConnectionColorizingFix();

            var subroutine = base.BaseRedrawEnumerator(x => (prevAttachedPath == null || x.start != ColorizedPoints.First.Value) &&
                                                       (nextAttachedPath == null || x.end != ColorizedPoints.Last.Value));
            while (subroutine.MoveNext()) yield return null;

            if (prevAttachedPath != null)
            {
                prevInterval.Set(new ColorizedPoint((prevAttachedPath.GetGlobalPointPos(prevAttachedPath.ColorizedPoints.Last.Previous.Value.pos) -
                                                                    (Vector2)(Handler.position + Shift)).ToVector2Int(),
                                                                   prevAttachedPath.ColorizedPoints.Last.Value.color),
                                 ColorizedPoints.First.Value);

                interval.Set(ColorizedPoints.First.Value, ColorizedPoints.First.Next.Value);

                nextInterval.Set(ColorizedPoints.First.Next.Value, ColorizedPoints.First.Next.Next.Value);

                GenerateOutlineByIntervals(interval, prevInterval, nextInterval);
            }

            yield return null;

            if (nextAttachedPath != null)
            {
                prevInterval.Set(ColorizedPoints.Last.Previous.Previous.Value, ColorizedPoints.Last.Previous.Value);

                interval.Set(ColorizedPoints.Last.Previous.Value, ColorizedPoints.Last.Value);


                nextInterval.Set(ColorizedPoints.Last.Value,
                                 new ColorizedPoint((nextAttachedPath.GetGlobalPointPos(nextAttachedPath.ColorizedPoints.First.Next.Value.pos) -
                                                        (Vector2)(Handler.position + Shift)).ToVector2Int(),
                                                    nextAttachedPath.ColorizedPoints.First.Value.color));

                GenerateOutlineByIntervals(interval, prevInterval, nextInterval);
            }
        }

        public bool IsFragmentDanger(Vector2Int first, Vector2Int second)
        {
            Vector2 dir = second - first;
            return Handler.eulerAngles.z % 90 != 0 ||
                   dir.Rotate(Handler.eulerAngles.z).ToVector2Int() == (int)DrawDir * Vector2.down;
        }

        public void ResetColors()
        {
            foreach(OutlinePathPoint point in Points)
            {
                point.hasDangerColor = false;
                point.prevFragmentType = FragmentColorType.safe;
            }
        }

        public void InitColors(Func<Vector2Int, Vector2Int, bool> dangerCheck)
        {
            if (Closed && !IsClosedStateValid())
                throw new Exception("Closed path not valid");

            for (LinkedListNode<OutlinePathPoint> node = Points.First.Next; node != null; node = node.Next)
            {
                if (dangerCheck(node.Previous.Value.pos, node.Value.pos))
                {
                    node.Previous.Value.hasDangerColor = node.Value.hasDangerColor = true;
                    node.Value.prevFragmentType = FragmentColorType.danger;

                    if (node.Previous.Value.prevFragmentType == FragmentColorType.safe)
                        node.Previous.Value.prevFragmentType = FragmentColorType.gradient;
                }
                else if (node.Value.prevFragmentType == FragmentColorType.safe)
                {
                    if (node.Previous.Value.prevFragmentType == FragmentColorType.danger)
                        node.Value.prevFragmentType = FragmentColorType.gradient;
                }
            }

            if (Closed)
            {
                if (dangerCheck(Points.Last.Value.pos, Points.First.Value.pos))
                {
                    Points.Last.Value.hasDangerColor = Points.First.Value.hasDangerColor = true;
                    Points.First.Value.prevFragmentType = FragmentColorType.danger;

                    if (Points.Last.Value.prevFragmentType == FragmentColorType.safe)
                        Points.Last.Value.prevFragmentType = FragmentColorType.gradient;

                    if (Points.First.Next.Value.prevFragmentType == FragmentColorType.safe)
                        Points.First.Next.Value.prevFragmentType = FragmentColorType.gradient;
                }
                else if (Points.First.Value.prevFragmentType == FragmentColorType.safe)
                {
                    if (Points.Last.Value.prevFragmentType == FragmentColorType.danger ||
                        Points.First.Next.Value.prevFragmentType == FragmentColorType.danger)
                        Points.First.Value.prevFragmentType = FragmentColorType.gradient;
                }
            }
        }

        public void CreateRails()
        {
            if (Handler.eulerAngles.z % 90 != 0 ||
                !MyMath.IsFloatNearToInt(Handler.position.x + Shift.x) ||
                !MyMath.IsFloatNearToInt(Handler.position.y + Shift.y))
                return;

            var railMap = GlobalSpace.game.map.railMap;

            for (LinkedListNode<OutlinePathPoint> node = Points.First.Next; node != null; node = node.Next)
            {
                OutlinePathPoint prevPoint = node.Previous.Value;
                OutlinePathPoint point = node.Value;

                if (((Handler.eulerAngles.z % 180 == 0 && point.pos.y == prevPoint.pos.y) ||
                     (Handler.eulerAngles.z % 180 != 0 && point.pos.x == prevPoint.pos.x)) &&
                    (point.prevFragmentType == FragmentColorType.gradient || point.prevFragmentType == FragmentColorType.safe))
                {
                    Vector2 first = GetGlobalPointPos(point.pos);
                    Vector2 second = GetGlobalPointPos(prevPoint.pos);
                    railMap.AddRail(new Vector2Int(Mathf.Min(MyMath.Round(first.x), MyMath.Round(second.x)), MyMath.Round(first.y)));
                }
            }
        }

        public void DeleteRails()
        {
            if (Handler.eulerAngles.z % 90 != 0 ||
                !MyMath.IsFloatNearToInt(Handler.position.x + Shift.x) ||
                !MyMath.IsFloatNearToInt(Handler.position.y + Shift.y))
                return;

            var railMap = GlobalSpace.game.map.railMap;

            for (LinkedListNode<OutlinePathPoint> node = Points.First.Next; node != null; node = node.Next)
            {
                OutlinePathPoint prevPoint = node.Previous.Value;
                OutlinePathPoint point = node.Value;

                if (((Handler.eulerAngles.z % 180 == 0 && point.pos.y == prevPoint.pos.y) ||
                     (Handler.eulerAngles.z % 180 != 0 && point.pos.x == prevPoint.pos.x)) &&
                    (point.prevFragmentType == FragmentColorType.gradient || point.prevFragmentType == FragmentColorType.safe))
                {
                    Vector2 first = GetGlobalPointPos(point.pos);
                    Vector2 second = GetGlobalPointPos(prevPoint.pos);
                    railMap.TryRemoveRail(new Vector2Int(Mathf.Min(MyMath.Round(first.x), MyMath.Round(second.x)), MyMath.Round(first.y)));
                }
            }
        }

        private void ConnectionColorizingFix()
        {
            if (prevAttachedPath != null)
                Points.First.Value.hasDangerColor = prevAttachedPath.Points.Last.Value.hasDangerColor =
                    (Points.First.Value.hasDangerColor || prevAttachedPath.Points.Last.Value.hasDangerColor);
            if (nextAttachedPath != null)
                Points.Last.Value.hasDangerColor = nextAttachedPath.Points.First.Value.hasDangerColor =
                    (Points.Last.Value.hasDangerColor || nextAttachedPath.Points.First.Value.hasDangerColor);
        }

        private static void ConnectPaths(OutlinePath prevPath, OutlinePath nextPath)
        {
            if (!CanConnectPaths(prevPath, nextPath))
                throw new Exception("Can't connect paths");

            Vector2Int point = (nextPath.GetGlobalPointPos(nextPath.ColorizedPoints.First.Value.pos) - (Vector2)prevPath.Handler.position).ToVector2Int();

            if (prevPath.GetGlobalPointPos(prevPath.ColorizedPoints.Last.Value.pos).y != nextPath.GetGlobalPointPos(nextPath.ColorizedPoints.First.Value.pos).y)
            {
                int dir = MyMath.Sign(point.y - prevPath.Points.Last.Value.pos.y);
                for (int y = prevPath.Points.Last.Value.pos.y + dir; y != point.y + dir; y += dir)
                    prevPath.AddLastPoint(new Vector2Int(point.x, y));
            }
            else if (prevPath.GetGlobalPointPos(prevPath.ColorizedPoints.Last.Value.pos).x != nextPath.GetGlobalPointPos(nextPath.ColorizedPoints.First.Value.pos).x)
            {
                int dir = MyMath.Sign(point.x - prevPath.Points.Last.Value.pos.x);
                for (int x = prevPath.Points.Last.Value.pos.x + dir; x != point.x + dir; x += dir)
                    prevPath.AddLastPoint(new Vector2Int(x, point.y));
            }

            nextPath.prevAttachedPath = prevPath;
            prevPath.nextAttachedPath = nextPath;
        }

        private static bool CanConnectPaths(OutlinePath prevPath, OutlinePath nextPath)
        {
            return !prevPath.Closed && !nextPath.Closed &&
                    prevPath.HandlerPosCorrect() && nextPath.HandlerPosCorrect() &&
                    prevPath.DrawDir == nextPath.DrawDir &&
                   (prevPath.GetGlobalPointPos(prevPath.ColorizedPoints.Last.Value.pos).x == nextPath.GetGlobalPointPos(nextPath.ColorizedPoints.First.Value.pos).x ||
                    prevPath.GetGlobalPointPos(prevPath.ColorizedPoints.Last.Value.pos).y == nextPath.GetGlobalPointPos(nextPath.ColorizedPoints.First.Value.pos).y);
        }

        private bool HandlerPosCorrect()
        {
            Vector2 pos = Handler.position + Shift;
            return MyMath.IsFloatNearToInt(pos.x) && MyMath.IsFloatNearToInt(pos.y);
        }

        protected override int GetIntervalHashCode(ColorizedInterval interval, ColorizedInterval prevInterval, ColorizedInterval nextInterval)
        {
            int shift = 0;
            if (prevInterval != null && (DrawDir == ColorizedPathDrawDir.Left && prevInterval.dir.Left() == interval.dir ||
                DrawDir == ColorizedPathDrawDir.Right && prevInterval.dir.Right() == interval.dir))
                shift = 1;

            if (nextInterval != null && (DrawDir == ColorizedPathDrawDir.Left && interval.dir.Left() == nextInterval.dir ||
                DrawDir == ColorizedPathDrawDir.Right && interval.dir.Right() == nextInterval.dir))
                shift = 2;

            if (prevInterval != null && (DrawDir == ColorizedPathDrawDir.Left && prevInterval.dir.Left() == interval.dir ||
                DrawDir == ColorizedPathDrawDir.Right && prevInterval.dir.Right() == interval.dir) &&
                nextInterval != null && (DrawDir == ColorizedPathDrawDir.Left && interval.dir.Left() == nextInterval.dir ||
                DrawDir == ColorizedPathDrawDir.Right && interval.dir.Right() == nextInterval.dir))
                shift = 3;

            int leftType = 4;
            if (prevInterval != null)
            {
                if (prevInterval.dir.Left() == interval.dir)
                    leftType = 5;
                if (prevInterval.dir.Right() == interval.dir)
                    leftType = 6;
            }

            int rightType = 7;
            if (nextInterval != null)
            {
                if (interval.dir.Left() == nextInterval.dir)
                    rightType = 8;
                if (interval.dir.Right() == nextInterval.dir)
                    rightType = 9;
            }

            int startColor = interval.start.color == DangerColor ? 11 : 10;
            int endColor = interval.end.color == DangerColor ? 13 : 12;

            return (1 << shift) + (1 << leftType) + (1 << rightType) + (1 << startColor) + (1 << endColor);
        }
    }
}