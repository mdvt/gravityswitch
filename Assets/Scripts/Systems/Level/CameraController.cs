﻿//#define Debug

using System.Linq;
using System.Collections.Generic;
using UnityEngine;

using Path;

public class CameraController : MonoBehaviour
{
    public float heightMultiplyer; //camera height / visible path height
    public float abyssPointHeightMultiplyer; //increasing visible area height on point.height * abyssPointHeightMultiplyer
    public float playerPosOnScreen; //percents of screen width from left side
    [Range(0, 60)] public float camZoomSpeed; // m/sec
    [Range(0, 1)] public float camInertia;
    [Range(0, 1)] public float frontScreenPartWithPathHeightConsidering;
    public float camZoomInTimeout; // sec
    private float time;

    Camera cam;

    public float LeftCameraBorderByX
    {
        get
        {
            return cam.ViewportToWorldPoint(new Vector2(0, 0)).x;
        }
    }

    public float RightCameraBorderByX
    {
        get
        {
            return cam.ViewportToWorldPoint(new Vector2(1, 1)).x;
        }
    }
#if Debug
    DebugTrace trace;
#endif

    public void start(Vector3 playerPos)
    {
        GlobalPath path = GlobalSpace.game.map.globalPath;
        cam = GetComponent<Camera>();
#if Debug
        trace = new DebugTrace(200, 0.1f, Color.red);
#endif

        time = 0;
        lastZoomDelta = 0;
        curZoom = path.Points[0].height * heightMultiplyer;
        cam.orthographicSize = curZoom / 2;

        float width = cam.ViewportToWorldPoint(new Vector2(1, 1)).x - cam.ViewportToWorldPoint(new Vector2(0, 0)).x;
        transform.position = new Vector3(playerPos.x + 0.5f + width / 2 - width * playerPosOnScreen, path.Points[0].height / 2f, transform.position.z);
    }

    public void update(Vector3 playerPos, float deltaTime)
    {
        time += deltaTime;
        float leftBorder = cam.ViewportToWorldPoint(new Vector2(0, 0)).x;
        float rightBorder = cam.ViewportToWorldPoint(new Vector2(1, 1)).x;
        List<PathPointF> visiblePath = GetSubGlobalPath(leftBorder, rightBorder);
        List<PathPointF> heightVisiblePath = GetSubGlobalPath(Mathf.Lerp(leftBorder, rightBorder, 1 - frontScreenPartWithPathHeightConsidering), rightBorder);

        float minHeight = float.MaxValue,
            maxHeight = float.MinValue,
            pathIntegral = 0;

        for (int i = 0; i < heightVisiblePath.Count; i++)
        {
            PathPointF point = heightVisiblePath[i];

            float curBottom = point.pos.y;
            float curTop = point.pos.y + point.height;

            if (!point.generateBottom)//abyss
                curBottom -= point.height * abyssPointHeightMultiplyer;
            if (!point.generateTop)//abyss
                curTop += point.height * abyssPointHeightMultiplyer;

            if (curBottom < minHeight)
                minHeight = curBottom;
            if (curTop > maxHeight)
                maxHeight = curTop;
        }

        for (int i = 1; i < visiblePath.Count; i++)
        {
            PathPointF point = visiblePath[i];
            PathPointF prevPoint = visiblePath[i - 1];

            pathIntegral += ((prevPoint.pos.y + point.pos.y + (point.height + prevPoint.height) / 2) * (point.pos.x - prevPoint.pos.x)) / 2; //trap square
        }

        maxHeight -= transform.position.y;
        minHeight -= transform.position.y;
        float height = Mathf.Max(Mathf.Abs(maxHeight), Mathf.Abs(minHeight)) * 2;
        ZoomAndMove(playerPos, pathIntegral / (visiblePath.Last().pos.x - visiblePath.First().pos.x), height * heightMultiplyer, time, deltaTime);

#if Debug
        trace.Update(transform.position + Vector3.forward);
#endif
    }

    private float curZoom, lastZoomOutTime, lastZoomDelta;
    public void ZoomAndMove(Vector3 playerPos, float posY, float height, float time, float deltaTime)
    {
        float delta = (height - curZoom) * camZoomSpeed * deltaTime;
        if (delta > 0)
            lastZoomOutTime = time;
        if (delta < 0 && time - lastZoomOutTime <= camZoomInTimeout)
            delta = 0;
        delta -= (delta - lastZoomDelta) * camInertia;
        lastZoomDelta = delta;

        curZoom += delta;
        cam.orthographicSize = curZoom / 2; // because orthographicSize is half-size of camera

        float width = cam.ViewportToWorldPoint(new Vector2(1, 1)).x - cam.ViewportToWorldPoint(new Vector2(0, 0)).x;
        Vector3 pos = transform.position;
        pos.x = playerPos.x + 0.5f + width / 2 - width * playerPosOnScreen;
        pos.y = posY;
        transform.position = pos;
    }

    private List<PathPointF> GetSubGlobalPath(float leftBorder, float rightBorder)
    {
        leftBorder = Mathf.Clamp(leftBorder, GlobalSpace.game.map.globalPath.Points[0].pos.x, GlobalSpace.game.map.globalPath.Points.Last().pos.x);
        rightBorder = Mathf.Clamp(rightBorder, GlobalSpace.game.map.globalPath.Points[0].pos.x, GlobalSpace.game.map.globalPath.Points.Last().pos.x);

        List<PathPointF> result = new List<PathPointF>();

        result.Add(GlobalSpace.game.map.globalPath.GetPointF(leftBorder));

        foreach (PathPoint p in GlobalSpace.game.map.globalPath.Points)
        {
            if (p.pos.x > leftBorder)
            {
                if (p.pos.x < rightBorder)
                    result.Add(new PathPointF(p));
                else
                    break;
            }
        }

        result.Add(GlobalSpace.game.map.globalPath.GetPointF(rightBorder));

        return result;
    }

    public void reset(Vector3 playerPos)
    {
        start(playerPos);
    }
}
