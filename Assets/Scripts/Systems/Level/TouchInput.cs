﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchInput : MonoBehaviour
{
    [HideInInspector]
    public bool TouchedOnFrame;

    public void update()
    {
        TouchedOnFrame = false;
        if (Input.touchSupported && Input.touchCount > 0)
        {
            Touch touch;

            for (int i = 0; i < Input.touchCount; i++)
            {
                touch = Input.GetTouch(i);
                if (touch.phase == TouchPhase.Began && !IsTouchOverUIObject(touch.position))
                    TouchedOnFrame = true;
            }
        }
        else if (Input.mousePresent)
        {
            if (Input.GetMouseButtonDown(0) && !IsTouchOverUIObject(Input.mousePosition))
                TouchedOnFrame = true;
        }
    }

    private bool IsTouchOverUIObject(Vector2 touchPos)
    {
        if (EventSystem.current == null)
            return false;

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = touchPos;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
