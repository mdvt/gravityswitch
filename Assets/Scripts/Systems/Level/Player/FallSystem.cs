﻿using UnityEngine;
using MyLibraries;

public partial class Player : MonoBehaviour
{
    public float FallSpeed;

    private bool Falling;

    private Vector2 StartPos;

    private bool IsLandingPosExist;
    private Vector2 LandingPos;
    private int LandingRailPosByY;

    public void FallInit(Map map)
    {
        Falling = true;
        StartPos = transform.position;

        CalculateLandingParams(map, transform.position);
    }

    public float FallMovementTan()
    {
        return FallSpeed / Speed;
    }

    private void FallDisable()
    {
        transform.position = new Vector3(transform.position.x, LandingPos.y, transform.position.z);

        CurrRailPosByY = LandingRailPosByY;
        Falling = false;
    }

    private void FallUpdate(Map map, float dt)
    {
        float shift = (int)GravityDirection * FallSpeed * dt;
        if ((int)(transform.position.y + 0.5f) != (int)(transform.position.y + 0.5f + shift))
            FallInit(map);

        transform.position += Vector3.up * shift;

        if (IsLandingPosExist && (int)GravityDirection != MyMath.Sign(LandingPos.y - transform.position.y))
            FallDisable();
    }

    private void CalculateLandingParams(Map map, Vector2 currPos)
    {
        IsLandingPosExist = false;

        bool newRailWasFounded;
        for (int x = MyMath.Round(currPos.x - 0.5f, MyMath.Direction.Down); x < map.railMap.Count; x++)
        {
            newRailWasFounded = false;

            foreach (int y in map.railMap[x])
            {
                if ((int)GravityDirection != MyMath.Sign(y - currPos.y))
                    continue;

                float PosOnLandingByY = y - 0.5f * (int)GravityDirection;
                float deltaPosByY = Mathf.Abs(PosOnLandingByY - currPos.y);
                float PosOnLandingByX = currPos.x + Speed * deltaPosByY / FallSpeed;

                if (MyMath.InRange(PosOnLandingByX, x - 0.5f, x + 1 + 0.5f) &&
                    (!IsLandingPosExist || Mathf.Abs(y - currPos.y) < Mathf.Abs(LandingRailPosByY - currPos.y)))
                {
                    LandingPos = new Vector2(PosOnLandingByX, PosOnLandingByY);
                    LandingRailPosByY = y;
                    IsLandingPosExist = true;
                    newRailWasFounded = true;
                }
            }

            if (IsLandingPosExist && !newRailWasFounded)
                return;
        }
    }
}
