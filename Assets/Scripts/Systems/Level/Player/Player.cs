﻿using System;
using UnityEngine;
using MyLibraries;

public partial class Player : MonoBehaviour
{
    public float Speed;
    public float trailLength;

    [NonSerialized] public bool dead;

    private int CurrRailPosByY;
    private MyMath.Direction GravityDirection;
    private int Distance;
    private float ZPos;
    private MyTrail trail;

    public void start(Map map)
    {
        dead = false;
        Distance = 0;
        CurrRailPosByY = 0;

        ZPos = transform.position.z;
        transform.position = new Vector3(0.5f, CurrRailPosByY + 0.5f, ZPos);
        transform.rotation = Quaternion.identity;
        Falling = false;
        GravityDirection = MyMath.Direction.Down;

        if (trail == null)
            trail = new MyTrail(GetComponent<LineRenderer>(),
                                (int)(trailLength / (Time.fixedDeltaTime * Speed)));
        else
            trail.Reset();

        //GlobalSpace.game.DebugDraw += FallDebug;
    }

    public void update(Map map, ref bool changeGravity, float dt)
    {
        float posByXBeforeMove = transform.position.x;

        transform.position += Vector3.right * Speed * dt;

        if (MyMath.Round(posByXBeforeMove, MyMath.Direction.Down) != MyMath.Round(transform.position.x, MyMath.Direction.Down))
        {
            Distance++;
            GlobalSpace.game.canvases.level.SetDifficulty(map.GetLinearDifficulty(Distance), Distance);
        }

        if (Falling)
            FallUpdate(map, dt);

        if (changeGravity && !Falling)
        {
            GravityDirection = MyMath.Switch(GravityDirection);//switch up to down and back
            FallInit(map);
            changeGravity = false;
        }
        else if (!Falling && IsCurrRailOver(map))
        {
            float deltaX = transform.position.x - (MyMath.Round(transform.position.x - 0.5f, MyMath.Direction.Down) + 0.5f);
            transform.position += Vector3.up * (int)GravityDirection * FallSpeed * deltaX / Speed;
            FallInit(map);
        }

        trail.AddPoint();
    }

    public void FallDebug()
    {
        if (Falling)
        {
            Debug.DrawLine(StartPos, LandingPos, Color.magenta);
            Debug.DrawLine(StartPos + Vector2.right * 0.5f + Vector2.up * (int)GravityDirection * 0.5f, LandingPos + Vector2.right * 0.5f + Vector2.up * (int)GravityDirection * 0.5f, Color.red);
            Debug.DrawLine(StartPos - Vector2.right * 0.5f + Vector2.up * (int)GravityDirection * 0.5f, LandingPos - Vector2.right * 0.5f + Vector2.up * (int)GravityDirection * 0.5f, Color.yellow);
        }
    }

    public void reset(Map map)
    {
        start(map);
    }

    private bool IsCurrRailOver(Map map)
    {
        int startCheckRail;
        int endCheckRail;
        if (MyMath.IsFloatNearToInt(transform.position.x - 0.5f))
        {
            startCheckRail = MyMath.Round(transform.position.x, MyMath.Direction.Down) - 1;
            endCheckRail = MyMath.Round(transform.position.x, MyMath.Direction.Down) + 1;
        }
        else
        {
            startCheckRail = MyMath.Round(transform.position.x - 0.5f, MyMath.Direction.Down);
            endCheckRail = MyMath.Round(transform.position.x + 0.5f, MyMath.Direction.Down);
        }

        for (int x = startCheckRail; x <= endCheckRail; x++)
        {
            if (GlobalSpace.game.map.railMap[x, CurrRailPosByY])
                return false;
        }

        return true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GlobalSpace.game.SetPause(true);
        dead = true;
    }

    public void OnTeleport(Vector3 teleport)
    {
        FallInit(GlobalSpace.game.map);
        trail.Teleport();
    }

    public float ShiftByX(float time)
    {
        return time * Speed;
    }
}