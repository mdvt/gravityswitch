﻿using UnityEngine;
using MyLibraries;
using System.Collections;

public class SystemsTest : MonoBehaviour
{

    DebugTrace DistributionDensityGraph;
    DebugTrace TimeGraph;
    DebugTrace QuadraticDeviationErrorGraph;
    DebugTrace LinearDeviationErrorGraph;

    void Start()
    {
        DistributionDensityGraph = new DebugTrace(1000, Mathf.Epsilon, Color.green);
        TimeGraph = new DebugTrace(1000, Mathf.Epsilon, Color.blue);
        QuadraticDeviationErrorGraph = new DebugTrace(1000, Mathf.Epsilon, Color.red);
        LinearDeviationErrorGraph = new DebugTrace(1000, Mathf.Epsilon, Color.cyan);

        CoroutineProcess process = new CoroutineProcess(this, TestsProcess, 10000);//10ms
        process.Start();
    }

    private void Update()
    {
        DistributionDensityGraph.Draw();
        TimeGraph.Draw();
        QuadraticDeviationErrorGraph.Draw();
        LinearDeviationErrorGraph.Draw();
    }

    IEnumerator TestsProcess()
    {
        int iters = 10000;
        float start = -10, end = 10;
        float M = 0;
        float S = 1f;
        float realM, realS, linearDeviation;
        float[] rands = new float[iters];

        DiscreteFunction dFunc = new DiscreteFunction(250, start, end, MyRandom.NormalDistributionDensityFunc(S, M));

        for (int i = 0; i < dFunc.StoredValuesCount; i++)
        {
            DistributionDensityGraph.Update(new Vector3(dFunc.MapIndexToArgument(i), dFunc[i]), false);
            yield return null;
        }

        for (int j = 50; j < 1000; j += 1)
        {
            dFunc.SetStoredValuesCount(j);

            float time = Time.realtimeSinceStartup;
            for (int i = 0; i < iters; i++)
                rands[i] = MyRandom.RandomByDistributionDensity(dFunc);
            time = ((Time.realtimeSinceStartup - time) / iters) * 1000000;

            float sum = 0;
            foreach (float r in rands)
            {
                sum += r;
                yield return null;
            }
            realM = sum / iters;

            sum = 0;
            float sum1 = 0;
            foreach (float r in rands)
            {
                sum += (r - realM) * (r - realM);
                sum1 += Mathf.Abs(r - realM);
                yield return null;
            }
            realS = Mathf.Sqrt(sum / iters);
            linearDeviation = sum1 / iters;

            Debug.Log("real M: " + realM + "   expected M: " + M +
                        "   real S: " + realS + "   expected S: " + S +
                        "   linear Deviation/real S: " + linearDeviation / realS +
                        "\niteration time: " + time + "microsec" +
                        "   sampling accuracy: " + dFunc.StoredValuesCount + "(stored values/arguments range)");

            float x = j / 100f;
            TimeGraph.Update(new Vector3(x, time / 100f));
            QuadraticDeviationErrorGraph.Update(new Vector3(x, realS / S));
            LinearDeviationErrorGraph.Update(new Vector3(x, linearDeviation / realS));

            yield return null;
        }
    }
}
