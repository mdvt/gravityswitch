﻿Shader "Unlit/LinearGradient"
{
     Properties
	 {
         _LeftColor("LeftColor", Color) = (1,1,1,1)
         _RightColor("RightColor", Color) = (1,1,1,1)
         _MainTex ("Main Texture", 2D) = "white" {}
     }
         SubShader
     {        
        Pass
        {
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
 
             #include "UnityCG.cginc"
 
             sampler2D _MainTex;
             float4 _MainTex_ST;
 
             fixed4 _LeftColor;
             fixed4 _RightColor;
             half _Value;
 
             struct v2f {
                 float4 position : SV_POSITION;
                 fixed4 color : COLOR;
                 float2 uv : TEXCOORD0;
             };
 
             v2f vert (appdata_full v)
             {
                 v2f o;
                 o.position = UnityObjectToClipPos(v.vertex);
                 o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                 o.color = lerp(_LeftColor,_RightColor, v.texcoord.x);
                 return o;
             }
 
             fixed4 frag(v2f i) : SV_Target
             {
                 float4 color;
                 color.rgb = i.color.rgb;
                 return color;
             }
             ENDCG
        }
     }
}