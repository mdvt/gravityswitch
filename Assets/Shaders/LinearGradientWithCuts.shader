﻿Shader "Unlit/LinearGradientWithCuts"
{
	Properties
	{
		 _LeftTopPointX("LeftTopPointX", Range(0,1)) = 0
		 _LeftTopPointY("LeftTopPointY", Range(0,1)) = 1
		 _LeftBottomPointX("LeftBottomPointX", Range(0,1)) = 0
		 _LeftBottomPointY("LeftBottomPointY", Range(0,1)) = 0
		 _RightTopPointX("RightTopPointX", Range(0,1)) = 1
		 _RightTopPointY("RightTopPointY", Range(0,1)) = 1
		 _RightBottomPointX("RightBottomPointX", Range(0,1)) = 1
		 _RightBottomPointY("RightBottomPointY", Range(0,1)) = 0
         _LeftColor("LeftColor", Color) = (1,1,1,1)
         _RightColor("RightColor", Color) = (1,1,1,1)
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

            fixed4 _LeftColor;
            fixed4 _RightColor;

			float _LeftTopPointX;
			float _LeftTopPointY;
			float _LeftBottomPointX;
			float _LeftBottomPointY;
			float _RightTopPointX;
			float _RightTopPointY;
			float _RightBottomPointX;
			float _RightBottomPointY;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float leftBorder = lerp(_LeftBottomPointX, _LeftTopPointX, clamp((i.uv.y - _LeftBottomPointY) / (_LeftTopPointY - _LeftBottomPointY), 0, 1));
				float rightBorder = lerp(_RightBottomPointX, _RightTopPointX, clamp((i.uv.y - _RightBottomPointY) / (_RightTopPointY - _RightBottomPointY), 0, 1));
				float topBorder = lerp(_LeftTopPointY, _RightTopPointY, clamp((i.uv.x - _LeftTopPointX) / (_RightTopPointX - _LeftTopPointX), 0, 1));
				float bottomBorder = lerp(_LeftBottomPointY, _RightBottomPointY, clamp((i.uv.x - _LeftBottomPointX) / (_RightBottomPointX - _LeftBottomPointX), 0, 1));

                fixed4 col = lerp(_LeftColor,_RightColor, clamp((i.uv.x - leftBorder) / (rightBorder - leftBorder), 0, 1));

				clip(-(i.uv.x < leftBorder || i.uv.x > rightBorder || i.uv.y > topBorder || i.uv.y < bottomBorder));
				return col;
			}
			ENDCG
		}
	}
}